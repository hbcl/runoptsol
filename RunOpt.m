clc;   rng('shuffle');   clearvars;   close all;   ProbSet= SetDirectory;
%%=======================================================================%%
% This code runs optimizations for locomotion models including "PMR" and  %
% "srinR", referred to as "Actuated Spring-mass" and "Actuator-only" in   %
% the manuscript: "Elastic energy savings and active energy cost in a     %
% simple model of running", PLoS ONE, Comp Bio, 2021.                     %
% Authors: Ryan T. Schroeder & Arthur D. Kuo                              %
% Code by: Ryan T. Schroeder (last updated: May 6, 2021)                  %
% Faculty of Kinesiology, University of Calgary, Calgary, Alberta, Canada %
%%=======================================================================%%
%%=======================================================================%%
%===========================START: USER INPUT=============================%
%%==========================vvvvvvvvvvvvvvvvv============================%%
%%=============================vvvvvvvvvvv===============================%%
%%================================vvvvv==================================%%
%%==================================v====================================%%
%%=======================================================================%%



%% ------------------------------------------Select View Settings, (0 or 1)
View.PlotSummary= 1; %-plot basic outputs (trajectory, ground reaction forces, etc.) (# figures = length(Model.SPmat))
View.Cost=        0; %-plot cost constituents versus SPmat (# figure = 1)
View.DutyFactor=  0; %-plot duty factor versus SPmat       (# figure = 1)



%% --------------------------------------------------------Model Parameters
%-parameters
ProbSet.Save=            'yes'; %-Save OptSol?, default='yes' (input: 'yes' or 'no')
Model.Name=            'srinR'; %-Model name code, default='srinR' (input: e.g. 'PMR' or 'srinR)
Model.ScaleWork=         'yes'; %-Scale work cost by 25%,-120%?  (input: 'yes' or 'no')
Model.ForceRateCoef=      5e-4; %-Force-rate cost coefficient, default= 5e-4 (input: #>=0)
Model.DampingRatio=       0.10; %-Tendon damping ratio, default= 0.1 (input: #>=0)
Model.CollisionFraction=  0.03; %-Fraction of momentum lost in collision, default= 0.03 (input: #>=0)
Model.SpringConstant=    35582; %-Spring constant, default= 35582 N/m (input: #>0)
Model.GroundSlope=        0.00; %-Ground slope as fraction dy/dx, default= 0, flat  (input: #),
Model.RelativeGravity=    1.00; %-Gravity realtive to Earth, default= 1= 9.81 m/s2  (input: #),
Model.Speed=            [3.50]; %-Running speed, default= 3 m/s (input: #>0)
Model.StepLength=        'G06'; %-Step length, default= 'G06' (input: #>0 m or 'G06' for what humans do: Gutmann et al. 2006, JEB)
Model.BodyMass=             70; %-Body mass, default= 70 kg (input: #>0 kg)
Model.MaxLegLength=       0.90; %-Maximum allowable leg length, default= 0.90 m (input: #>0 m)
Model.SaveName=     'Test_001'; %-Name you would like to save optimization output as (input: character array), default= fnID
Model.Preset=               ''; %-Define problem parameters from presets (see ReadMe for options)
%-constraints
Model.MinMax_Force=     inf*[-1; 1]; %-Constraint on actuator force,                       default= [min=-inf; max=inf], (input: #)
Model.MinMax_ForceRate= inf*[-1; 1]; %-Constraint on actuator force rate,                  default= [min=-inf; max=inf], (input: #)
Model.MinMax_MechPower= inf*[-1; 1]; %-Constraint on actuator power,                       default= [min=-inf; max=inf], (input: #)
Model.MinMax_MetPower=  inf*[-1; 1]; %-Constraint on 'metabolic power' (i.e. energy rate), default= [min=-inf; max=inf], (input: #)
%-sweeping parameter
Model.SPmat= Model.Speed; %-For multiple optimizations, define SPmat with desired field in structure Model



%% ---------------------Parameters for Opt Phase I - Random Initial Guesses
%--Begin by selecting optimization settings for random guesses phase. The
%--process tests for global optimality by optimizing multiple iterations
%--of random initial guesses. Each optimal solution can therefore, be
%--thought of as a local minimum, whereby the lowest-cost solution is
%--considered the global optimum (although it is impossible to know if it
%--is indeed the true global optimum).
ProbSet.RG.MeshTolerance=     1e-4; %-Mesh tolerance (default: 1e-4)
ProbSet.RG.SNOPTTolerance=    1e-6; %-SNOPT (or chosen optimizer) tolerance (default: 1e-6)
ProbSet.RG.StartIteration=       1; %-Starting iteration number (default: 1)
ProbSet.RG.TotalIteration=      15; %-How many random guess iterations? (default: 15)
ProbSet.RG.DownsampleRate=    0.01; %-Downsamples after 1st mesh iteration (default: 0.01 sec)
ProbSet.RG.MaxMeshIteration=   [2]; %-Maximum # of mesh iterations (default: 2)
ProbSet.RG.MaxSNOPTIteration= 4000; %-Maximum # of mesh iterations (default: 4000)



%% ----------------------------Parameters for Opt Phase II - Perturb P-seed
%--Next, set optimization settings for perturbation phase. The process
%--tunes the "global" optimum by peturbing the solution multiple iterations
%--until the solution cannot be improved any further.
ProbSet.PP.MeshTolerance=     1e-4; %-Mesh tolerance (default: 1e-4)
ProbSet.PP.SNOPTTolerance=    1e-6; %-SNOPT (or chosen optimizer) tolerance (default: 1e-6)
ProbSet.PP.StartIteration=       1; %-Starting iteration number (default: 1)
ProbSet.PP.TotalIteration=      15; %-How many random guess iterations? (default: 15)
ProbSet.PP.PeturbMagnitude=    1/8; %-Perturbation magnitude (default: 1/8 * state/control range)
ProbSet.PP.DownsampleRate=    0.01; %-Downsamples after 1st mesh iteration (default: 0.01 sec)
ProbSet.PP.MaxMeshIteration=   [2]; %-Maximum # of mesh iterations (default: 2)
ProbSet.PP.MaxSNOPTIteration= 4000; %-Maximum # of mesh iterations (default: 4000)



%% ======================================================================%%
%%=======================================================================%%
%%==================================^====================================%%
%%================================^^^^^==================================%%
%%=============================^^^^^^^^^^^===============================%%
%%==========================^^^^^^^^^^^^^^^^^============================%%
%============================END: USER INPUT==============================%
%%=======================================================================%%
%%=======================================================================%%










%% ===============================================Prep Params/Monitor Files
%-check data types
% [Model, ProbSet]= CheckDataTypes(Model, ProbSet);

%-fill in default parameter values
if isempty(Model.Name);   Model.Name= 'srinR';   end
[Model, ProbSet]= feval([Model.Name '_DefaultParams'],Model, ProbSet);

%-delete pre-existing monitor files
DeleteMonitorFiles(ProbSet);


ProbSet.OptTime=[];   ProbSet.OptWhen=datestr(now);
%% ============================================Iterate for Multiple OptSols
for iSP=1:length(Model.SPmat)
fprintf('SPmat(%1.0f)= %1.2f\n',iSP,Model.SPmat(iSP));
ProbSet.RG.StartIteration=1;   ProbSet.PP.StartIteration=1;   ProbSet.iSP=iSP; ProbSet.SPmat= Model.SPmat;

%-compile model ID
[Model, ProbSet, auxdata]= CompileModelID(Model, ProbSet);

%-Replicate Archived OptSol (RAOS)?
if strcmpi(Model.Preset,'RAOS')
[Model, ProbSet, auxdata]= SearchArchivedOptSol(Model, ProbSet, auxdata);
end

%-check for preexisting OptSol
if ~exist('Response','var'); Response=[]; end
[Model, Response]= OverwriteCheck(Model, ProbSet, Response);
if Response==3; continue; elseif Response==4; return; end

%-define problem parameters for optimization
[Model, auxdata]= CalcParameters(Model, ProbSet, auxdata);

%-check for partial solution set to continue optimizations
[output, ProbSet]= CheckPartialSolution(Model, auxdata, ProbSet);


%% ==============Opt Phase I - Random Initial Guesses for Global Optimality
[output, ProbSet, auxdata, Pseed]= FindGlobalOpt(Model, ProbSet, auxdata, output);


%% ===========================================Opt Phase II - Perturb P-seed
[OptSol(iSP), output, ProbSet, auxdata, Pseed]= TuneGlobalOpt(Model, ProbSet, auxdata, output, Pseed);


%% ==========================================Save GPOPS OptSol at every iSP
SaveOptSol(Model, ProbSet, OptSol(iSP));


%% =================================================Output OptSol Variables
OPvars(iSP)= feval([Model.Name '_ProcessOptSol'],OptSol(iSP), Model);


%% ==================================================Plot Optimal Solutions
feval([Model.Name '_PlotOptSol'],OPvars(iSP), Model, ProbSet, OptSol(iSP), View);


end %-end SPmat(iSP)
if ispc; cd([ProbSet.Home '\OptSols']); else; cd([ProbSet.Home '/OptSols']); end


%% ============================================Plot Sweeping Parameter Vars
if Response~=3;   PlotSPmat(OPvars, Model, View);   end









