# Summary
This document contains information regarding an optimization workflow using the MATLAB software GPOPS-II [1] and SNOPT7 [2], as well as custom scripts from Ryan T. Schroeder (University of Calgary in Alberta, Canada; contact: *ryan.schroeder@ucalgary.ca*). The optimization scripts have been used to produce data and figures for peer-reviewed manuscripts [3]. This document includes information about the Setup and licenses needed to operate the software, short descriptions about the Main Programs users can interact with, and a summary of user inputs/outputs (e.g. *Model* Parameters, *View* Settings, *ProbSet* Parameters, *OPvars* output variables). All code was developed with MATLAB R2020a on a Windows 10 PC.  

## Setup
(1) Download [GPOPS2](https://www.gpops2.com/) and move all contents to the ”back_end\GPOPS2” folder in this repository. This includes folders like “examples”, “gpopsUtilities”, “lib”, “license”, “nlp”, etc.
(2) Open the subfolder “back_end\GPOPS2\nlp” from the repository and create a new folder called "snopt". Next download [SNOPT](https://ccom.ucsd.edu/~optimizers/downloads/) and add all contents to the new folder you just created. The added content should include folders like “snopt7”, etc.
(3) Run *PathSetup.m* to set up MATLAB path in same directory where *RunOpt.m* is. If the directory is changed in the future, the user should rerun *PathSetup.m* to re-establish the correct MATLAB path. The program *PathSetup.m* should always be in the same folder as *RunOpt.m*.


## Main Programs
(i)  ***RunFig.m*** is used to plot figures from saved optimization outputs, determined either by parameters indicated, saved name, or by presets (see below for details). The user can choose parameter values for the structures *View* and *Model*. If the user is unsure what parameter values they would like to use, default values are preprogrammed. Default parameters will also be used if any field variable is left empty (e.g., [] for numeric variable, ‘’ for character array variable). Furthermore, presets allow the user to view plots from published manuscripts [3] (see below for details).
(ii) ***RunOpt.m*** is used to run optimizations for predetermined models (see *Model.Name* below for more details) with chosen parameters described below. The user can choose parameter values for the structures *Model*, *View*, *ProbSet.RG* and *ProbSet.PP*. However, if unsure of what parameter values to use, default values are preprogrammed. Default parameters will also be used if any field variable is left empty (e.g., [] for numeric variable, ‘’ for character array variable).


## Structure Variables
Relevant structure variables are listed below, with input options, default values and comments (bullets). 

### Structure Variable *View* (input parameters used in RunOpt.m & RunFig.m)
The structure variable *View* contains fields that allow the user to choose plot displays as desired.  

*View.Preset*=  
Only found in RunFig.m
‘S21’ --> This displays plots from the published manuscript [3]
Produces 9 plots.
WARNING: non-empty presets override all other user inputs in RunFig.m. Use empty character array (i.e. ‘’) to plot saved optimization outputs associated with prescribed parameter values and View settings in Model and View structures, repsectively.
More presets to come in future updates


*View.PlotSummary*=  
This plot displays basic mechanical variables associated with the optimal solution (e.g. center of mass trajectory, ground reaction forces, mechanical power, etc.).
Input: # --> 1 to display plot, 0 to skip plot
WARNING: must input empty character array in View.Preset (i.e. ‘’) and set View settings to 1 in order to view them.
Produces number of plots equal to length(SPmat); see Model.SPmat for more details.


*View.Cost*=  
This plot displays cost contributions from work/force-rate, etc, versus the variable set in Model.SPmat (see description of this field for more details)
Input: # --> 1 to display plot, 0 to skip plot
WARNING: must input empty character array in View.Preset (i.e. ‘’) and set View settings to 1 in order to view them.
Produces 1 plot.


*View.DutyFactor*=  
This plot displays duty factor (defined as time of ground contact multiplied by step frequency) versus the variable set in Model.SPmat (see description of this field for more details)
Input: # --> 1 to display plot, 0 to skip plot
WARNING: must input empty character array in View.Preset (i.e. ‘’) and set View settings to 1 in order to view them.
Produces 1 plot.


### Structure Variable *Model* (input parameters used in RunOpt.m & RunFig.m)
The structure variable *Model* contains fields that allow the user to prescribe model parameters and presets as desired.  

*Model.Save*=
‘yes’ --> save optimization output as .mat file in folder “OptSols”
‘no’  --> do not save optimization output
Default = ‘yes’


*Model.Name*=  
‘PMR’   --> model with point-mass body, spring-damper in series with extending actuator, allows for simple model of collision loss and can be used with work/force-rate costs. Equivalent to Actuated-spring-mass Model [3]
‘srinR’ --> model with point-mass body and extending actuator (no spring or damper), can be used with work/force-rate costs (comparable to previous models [4,5]). Equivalent to Actuated-mass Model [3]
Default = ‘srinR’


*Model.ScaleWork*=  
‘yes’ --> scales positive/negative work by inverse of 25% and -120%, respectively
‘no’  --> no scaling
Default = ‘yes’ for PMR and ‘no’ for srinR


*Model.ForceRateCoef*=  
num --> weighting coefficient in front of force-rate cost: int (dFm/dt)^2 dt
Default = 5e-4


*Model.DampingRatio*=  
num --> damping ratio of damper in parallel with spring (=inf for srinR)
Default = 0.10 for PMR


*Model.CollisionFraction*=  
num --> collision fraction describes fraction of momentum lost in collision (≥ foot mass)
Default = 0.03 for PMR and 0 for srinR


*Model.SpringConstant*=  
num --> spring constant in PMR
Default = 35582 kN/m for PMR and inf for srinR


*Model.GroundSlope*=  
num --> ground slope, i.e. dy/dx
Default = 0


*Model.RelativeGravity*=  
num --> gravitational acceleration relative to Earth (x9.81 m/s^2)
Default = 1.00


*Model.Speed*=  
num --> running speed
Default = 3.00 m/s


*Model.StepLength*=  
num --> step length (i.e. distance travelled across the ground by body)
‘G06’ standard human relationship for step length vs. speed [6]
Default = ‘G06’


*Model.BodyMass*=  
num --> body mass
Default = 70 kg


*Model.MaxLegLength*=  
num --> maximum allowable leg length
Default = 0.90 m


*Model.SaveName*=  
‘Test_001’ --> file name of saved OptSol
Default = fnID (default ID that encodes parameter values)


*Model.Preset*=  
‘RAOS’ --> Replicate Archives Optimal Solution (OptSol)
Uses initial guesses from an OptSol in Archives folder instead of random values
More presets to come in future updates
Default = ‘’ (empty character array)


*Model.MinMax_Force*=
num (2x1 array) --> for constraints on actuator force: [min constraint; max constraint]
Use inf for no constraint
Default = [-inf; inf]


*Model.MinMax_ForceRate*=
num (2x1 array) --> for constraints on first derivative of actuator force: [min constraint; max constraint]  
Use inf for no constraint
Default = [-inf; inf]


*Model.MinMax_MechPower*=  
num (2x1 array) --> for constraints on mechanical power of actuator: [min constraint; max constraint]
Use inf for no constraint
Default = [-inf; inf]


*Model.MinMax_MechPower*=  
num (2x1 array) --> for constraints on ‘metabolic’ power, i.e. model cost per time: [min constraint; max constraint]
Use inf for no constraint
Default = [-inf; inf]


*Model.SPmat*=  
Var --> variable used for sweeping parameter study
Default = Model.Speed (i.e. optimize multiple speeds)

	
### Structure Variable *ProbSet.RG(PP)* (input parameters used in RunOpt.m only)
The structure variable *ProbSet.RG(PP)* contains fields that allow the user to prescribe model parameters and presets as desired. The field RG for “Random Guesses” refers to parameters associated with optimizations that help determine a global optimum with random initial guesses. The field PP for Peturb P-seed” refers to parameters associated with optimizations that help to fine-tune the global optimum by adding small-magnitude noise to the lowest-cost solution in RG.  

*ProbSet.RG(PP).MeshTolerance*=  
num --> mesh tolerance for optimization problem
Default = 1e-4


*ProbSet.RG(PP).SNOPTTolerance*=  
num --> optimization tolerance for solution
Default = 1e-6


*ProbSet.RG(PP).StartIteration*=  
num --> start iteration for RG(PP)
Default = 1
Note: set start iteration higher than total iteration to skip PP optimization


*ProbSet.RG(PP).TotalIteration*=  
num --> total number of iterations for RG(PP)
Default = 15


*ProbSet.RG(PP).DownsampleRate*=  
num --> when iterating meshes, nodes are down sampled to avoid excessive nodes in GPOPS2
Default = 0.01


*ProbSet.RG(PP).MaxMeshIteration*=  
num --> maximum number of mesh iterations
Default = 2


*ProbSet.RG(PP).MaxSNOPTIteration*=  
num --> maximum number of iterations until SNOPT prematurely terminates
Default = 4000


*ProbSet.PP.PeturbMagnitude*=  
num --> magnitude of noise scaled to state/control/parameter range
Default = 0.125


### Structure Variable *OPvars* (output variables from optimizations, used in RunOpt.m & RunFig.m)
The structure variable *OPvars* contains fields with various output variables of the optimization.  

*OPvars.name*=  
Model.Name


*OPvars.cost*=  
Includes model costs such as those from work, force rate and total cost.
Non-dimensional units.


*OPvars.energetics*=  
Includes work (per step) and power (per time) of the leg's elements (e.g. actuator, spring, etc.)
For work: [1x2] array for both negative and positive work of each element.
Units of J or W.


*OPvars.kinematics*=  
Includes positions, velocities and accelerations in the model.
Columns are for x/y/z and rows are for time.
Units of m, m/s, m/s^2 or deg, deg/s, deg/s^2 as appropriate.


*OPvars.kinetics*=  
Includes force and force derivatives.
Columns are for x/y/z and rows are for time.
Units of N or N/s as appropriate.


*OPvars.time*=  
Includes time vector (s), duty factor, contact time (s) and flight time (s).


*OPvars.units*=  
List of all units in OPvars.


*OPvars.Model*=  
Includes all input parameters of the optimization (= Model structue) for reference.


# References
[1] Patterson, M. A. & Rao, A. V. GPOPS-II: A MATLAB Software for Solving Multiple-Phase Optimal Control Problems Using hp-Adaptive Gaussian Quadrature Collocation Methods and Sparse Nonlinear Programming. ACM Trans. Math. Softw. 41, 1–37 (2014).  
[2] Gill, P. E., Murray, W. & Saunders, M. A. SNOPT: An SQP Algorithm for Large-Scale Constrained Optimization. SIAM Rev. 47, 99–131 (2005).  
[3] Schroeder, R. T. & Kuo, A. D. Elastic energy savings and active energy cost in a simple model of running. (submitted) PLoS Comp Bio (2021).  
[4] Rebula, J. R. & Kuo, A. D. The Cost of Leg Forces in Bipedal Locomotion: A Simple Optimization Study. PLOS ONE 10, e0117384 (2015).  
[5] Srinivasan, M. & Ruina, A. Computer optimization of a minimal biped model discovers walking and running. Nature 439, 72–75 (2006).  
[6] Gutmann, A. K. Constrained optimization in human running. Journal of Experimental Biology 209, 622–632 (2006).  

# Contact Information
Ryan T. Schroeder  
Postdoctoral Scholar, Faculty of Kinesiology  
University of Calgary in Alberta, Canada  
*ryan.schroeder@ucalgary.ca*
