function [OptSol output ProbSet auxdata Pseed]= TuneGlobalOpt(Model, ProbSet, auxdata, output, Pseed)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> structure with problem parameters
%  output  --> structure with optimal solutions from every iteration in field "RG"
%  Pseed   --> lowest cost solution in output.RG ("global" optimum)
%===================================================================Outputs
%  OptSol  --> optimal solution
%  output  --> structure with optimal solutions from every iteration in field "PP"
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> input auxdata updated with random initial guesses
%  Pseed   --> previously lowest cost solution to peturb
%==========================================================================
%  This function perturbs Pseed to tune the "global" optimum

iSP=    ProbSet.iSP;
fnID=   ProbSet.fnID;
Home=   ProbSet.Home;
SPmat=  ProbSet.SPmat;
SaveOP= ProbSet.SaveOP;
TempFiles=  ProbSet.TempFiles;
TempFolder= ProbSet.TempFolder;

iRG= ProbSet.RG.iteration;        nRG= ProbSet.RG.TotalIteration;
iPP= ProbSet.PP.StartIteration;   nPP= ProbSet.PP.TotalIteration;
if isfield(ProbSet.PP,  'CoT');         CoT_PP= ProbSet.PP.CoT;   end
if isfield(ProbSet.PP,   'EF');          EF_PP= ProbSet.PP.EF;    end
if isfield(ProbSet.PP,'seeds'); OptSolSeeds.PP= ProbSet.PP.seeds; end
if isfield(ProbSet.PP, 'tave'); OptTimeAve= ProbSet.tave; else OptTimeAve=[]; end

OptTime= ProbSet.OptTime;
OptWhen= ProbSet.OptWhen;
auxdata.MeshTol   = ProbSet.PP.MeshTolerance;
auxdata.SNOPTtol  = ProbSet.PP.SNOPTTolerance;
auxdata.MaxSNOPTi = ProbSet.PP.MaxSNOPTIteration;
MaxMeshiMat       = ProbSet.PP.MaxMeshIteration;
resTH = ProbSet.PP.DownsampleRate;
ErrMag= ProbSet.PP.PeturbMagnitude;

PflowerCost=-inf;
while Pseed.result.objective>PflowerCost
    if PflowerCost>0
        if isfield(output.PP(MinLoc_PP),'warnings')==0
            output.PP(MinLoc_PP).warnings=cell(1);
        end
        Pseed=output.PP(MinLoc_PP); iPP=1;
    end
while iPP<=nPP
    iMesh=1;
    while iMesh<=length(MaxMeshiMat)
    rng('shuffle');
%     clc;
%     fprintf('sweeping parameter number'); iSP
%     fprintf('\n\n');
%     fprintf('perturb p-seed number'); iPP
%     fprintf('\n\n');
%     fprintf('max mesh iterations'); MaxMeshiMat(iMesh)
%     fprintf('\n\n\n');
    auxdata.MaxMeshi = MaxMeshiMat(iMesh); % mesh max iterations


% ==============================================================Setup GPOPS
if iMesh==1
%  --------------------------------------------------Define Initial Guesses
%  -------------with perturbation scaled by range*ErrMag (default: +-12.5%)
for i=length(Pseed.result.solution.phase)
%  --------------------------------------------------------------------time
auxdata.guess.phase(i).time= Pseed.result.solution.phase(i).time;
%  -------------------------------------------------------------------state
StateErrMag= range(Pseed.result.solution.phase(i).state)*ErrMag;
StateErrPrt= (2*rand(size(Pseed.result.solution.phase(i).state))-1).*...
            repmat(StateErrMag,size(Pseed.result.solution.phase(i).time,1),1);
auxdata.guess.phase(i).state= Pseed.result.solution.phase(i).state+StateErrPrt;
%  -----------------------------------------------------------------control
ControlErrMag= range(Pseed.result.solution.phase(i).control)*ErrMag;
ControlErrPrt= (2*rand(size(Pseed.result.solution.phase(i).control))-1).*...
            repmat(ControlErrMag,size(Pseed.result.solution.phase(i).time,1),1);
auxdata.guess.phase(i).control= Pseed.result.solution.phase(i).control+ControlErrPrt;
%  ----------------------------------------------------------------integral
IntegralErrMag= Pseed.result.solution.phase(i).integral*ErrMag;
IntegralErrPrt= (2*rand(size(Pseed.result.solution.phase(i).integral))-1).*...
                                                            IntegralErrMag;
auxdata.guess.phase(i).integral= Pseed.result.solution.phase(i).integral;
%  ---------------------------------------------------------------parameter
if isfield(Pseed.result.solution,'parameter')==1
ParameterErrMag= Pseed.result.solution.parameter*ErrMag;
ParameterErrPrt= (2*rand(size(Pseed.result.solution.parameter))-1).*...
                                                           ParameterErrMag;
auxdata.guess.parameter= Pseed.result.solution.parameter;
end
end

elseif iMesh>1
% T = output.RG(iRG,iMesh-1).result.solution.phase.time(end);
if isfield(Pseed.result.solution,'parameter')==1
auxdata.guess.parameter  = output.PP(iPP,iMesh-1).result.solution.parameter;
end

for i=length(auxdata.guess.phase)
tf  = output.PP(iPP,iMesh-1).result.solution.phase(i).time(end) - ...
      output.PP(iPP,iMesh-1).result.solution.phase(i).time(  1);
nact= length(output.PP(iPP,iMesh-1).result.solution.phase(i).time);
nthr= round(tf/resTH);
if nact-nthr>2
DSlogic = ones(size(output.PP(iPP,iMesh-1).result.solution.phase(i).time))==1;
ndel= nact-nthr;
nskp= round((nact-2)/(ndel-1));
DSlogic(2:nskp:end-1)= false;
auxdata.guess.phase(i).time     = output.PP(iPP,iMesh-1).result.solution.phase(i).time(DSlogic);
auxdata.guess.phase(i).state    = output.PP(iPP,iMesh-1).result.solution.phase(i).state(DSlogic,:);
auxdata.guess.phase(i).control  = output.PP(iPP,iMesh-1).result.solution.phase(i).control(DSlogic,:);
auxdata.guess.phase(i).integral = output.PP(iPP,iMesh-1).result.solution.phase(i).integral;
else
auxdata.guess.phase(i).time     = output.PP(iPP,iMesh-1).result.solution.phase(i).time;
auxdata.guess.phase(i).state    = output.PP(iPP,iMesh-1).result.solution.phase(i).state;
auxdata.guess.phase(i).control  = output.PP(iPP,iMesh-1).result.solution.phase(i).control;
auxdata.guess.phase(i).integral = output.PP(iPP,iMesh-1).result.solution.phase(i).integral;
end
end
end


%-overwrite initial guesses with seeds field in 'RAOS' mode
if strcmpi(Model.Preset,'RAOS') && isfield(ProbSet.PP,'Seeds')
auxdata.guess= ProbSet.PP.Seeds(iRG,iMesh);
end


% =============================================================Run GPOPS-II
ProbSet.ttic =        tic; % beginning time
ProbSet.tave = OptTimeAve; % Ave. time for Opt Sol

LatestOutput = feval(Model.Name,auxdata); %-call model in GPOPS-II

        if isfield(LatestOutput,'warnings')==0
            LatestOutput.warnings=cell(1);
        end
output.PP(iPP,iMesh)=LatestOutput;   clear LatestOutput;
OptSolSeeds.PP(iPP,iMesh)= output.PP(iPP,iMesh).result.setup.auxdata.guess;
OptTime = [OptTime toc(ProbSet.ttic)];
OptWhen = [OptWhen;datestr(now)];
OptWhenCur = OptWhen(end,:);

if SaveOP==1
   if isempty(Model.SaveName)
   fsave= [Home '/OptSols/working' fnID(findstr(fnID,'OptSol')+6:end) '.mat'];
   else
   if strcmpi(Model.SaveName(end-3:end),'.mat')
   fsave= [Home '/OptSols/working_' Model.SaveName];    
   else
   fsave= [Home '/OptSols/working_' Model.SaveName '.mat'];
   end
   end
   
   if ispc; fsave= strrep(fsave,'/','\'); end
   save(fsave,'output')
end

% ---------------------------------------------------Cost of Opt Sol Output
CoT_PP(iPP,iMesh,iSP)= output.PP(iPP,iMesh).result.objective;
 EF_PP(iPP,iMesh,iSP)= output.PP(iPP,iMesh).result.nlpinfo;
              EF_cPP = EF_PP(iPP,iMesh,iSP);

    if EF_PP(iPP,iMesh,iSP)==1 || EF_PP(iPP,iMesh,iSP)==2
          if toc(ProbSet.ttic)<=4*ProbSet.tave
          iMesh=iMesh+1;
          else
          iMesh=1;
          end
    else
          iMesh=1;
    end
        OptTimeAve=   mean(OptTime);
        OptTimeNum= length(OptTime);
    
    AllSol = output;
    fileN = TempFiles{2};
    fileNaux = 'MonAux.mat';
    if ispc; s='\'; else s='/'; end
    if exist([Home TempFolder s fileN])==0
    save([Home TempFolder s fileN],'AllSol');
    save([Home TempFolder s fileNaux],'SPmat','iSP','iRG','iPP','iMesh','OptTimeAve','OptTimeNum','OptWhenCur','EF_cPP','fnID');
    else
    save([Home TempFolder s fileN],'AllSol','-append');
    save([Home TempFolder s fileNaux],'SPmat','iSP','iRG','iPP','iMesh','OptTimeAve','OptTimeNum','OptWhenCur','EF_cPP','fnID','-append');
    end
    clear AllSol;
    OptWhen(end,:)='';
    end
iMesh=length(MaxMeshiMat);

    if EF_PP(iPP,iMesh,iSP)==1 || EF_PP(iPP,iMesh,iSP)==2
          iPP=iPP+1;
    end
end
iPP=nPP;

ProbSet.PP.iteration= iPP;
ProbSet.PP.CoT= CoT_PP;
ProbSet.PP.EF =  EF_PP;
ProbSet.PP.seeds= OptSolSeeds.PP;
ProbSet.OptTime= OptTime;
ProbSet.OptWhen= OptWhen;


% -------------------------------------------------------------------------
% Define final optimal solution as lowest cost output from Perturbed P-seed
MinCoT_PP   = min( min(CoT_PP(:,:,iSP)));
MinLoc_PP   = min(find(CoT_PP(:,:,iSP)==MinCoT_PP));
PflowerCost = output.PP(MinLoc_PP).result.objective;
if contains(fnID,'_FreeBnds')
PflowerCost = inf; 
end

end


if contains(fnID,'_FreeBnds')
OptSol= output.PP(MinLoc_PP);
else
if nRG>=1
OptSol= Pseed;
elseif nRG<1
OptSol= output.PP(MinLoc_PP);
end
end

OptSol.result.setup.seeds= OptSolSeeds;







end