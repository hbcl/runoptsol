function [SPps, View, Model]= SetPresetSPmat(View, Model, ProbSet)
%====================================================================Inputs
%  View    --> structure with figure plot instructions
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%  SPps    --> structure with model parameters from all OptSols in preset
%  View    --> structure with figure plot instructions
%  Model   --> structure with updated SPmat
%==========================================================================
%  This function overwrites SPmat for preset





%-which preset?
if strcmpi(View.Preset,'S21') %-OptSols from Schroeder & Kuo, 2021
%-Fig 1: (x1)
%- spring-mass --> kr=0.45, CF=0, DR=0, cR=0, v=3.9 m/s, ds=1.3 m, f=3.0 Hz
%-Fig 2: (x0)
%- no results in Fig. 2
%-Fig 3: (x8)
%-(top x4) spring-mass         -->  kr= 0.18 0.45 1.33 4.45 (v=3.5 m/s, f~3.1 Hz)
%-(bot x4)    act-mass (Ew+Er) --> eps= 10.^(-[5:-1:2]) (v=3.5 m/s, f~3.1 Hz)
%-Fig 4: (x2)
%-Act-mass (Ew) --> kr= 0.5 4.0 (v=3.0 m/s, f=2.94 Hz)
%-Fig 5: (4x3=12)
%-Act-mass (Ew) --> kr= 0.5 1 2 4.0 (v=2.5 3.0 3.5 m/s, f=2.81 2.94 3.07 Hz)
%-Fig 6: (x10)
%-(top x4 ) Act-mass (Ew+Er) --> eps= 0.0002 0.0008 0.003 0.01 (v=3.0 m/s, f=2.94 Hz)
%-(bot x10) Act-mass (Ew+Er) --> eps= [1 2 5 8 1 2 3 5 1 2].*10.^(-[4 4 4 4 3 3 3 3 2 2]) (v=3.0 m/s, f=2.94 Hz)
%-Fig 7: (x1)
%-Act-mass (Ew+Er) --> all nominal values (v=3.9 m/s ds=1.3 m)
%-Fig 8: (x37)
%-(left   x7) Act-mass (Ew+Er) --> all nom, v= 2 2.22 2.5 3 3.5 3.89 4 m/s (f= 'G06')
%-(rght 3x10) Act-mass (Ew+Er) --> all nom, v= 2.5 3.0 3.5 m/s (f= 'G06'), kr= [0.50 0.70 1.0 1.3 1.7 2.0 2.5 3.0 3.5 4.0] (per speed)
%-Fig 9: (x158)
%-(a,b x13) Act-mass (Ew+Er) --> all nom, v= 3 m/s (f='G06'), Slope= [-0.25:0.05:0.25] & [-0.08 -0.07]
%-(  c x33) Act-mass (Ew+Er) --> all nom, v= 3 m/s (f='G06'), Slope= [-0.25:0.05:0.25] per kr, where kr= [0.5 2.0 inf]
%-(  d x35) Act-mass (Ew+Er) --> all nom, v= 3 m/s (''), Slope= [-0.25:0.05:0.25] add [-0.08 -0.07] for Eps=0, Eps= [0 5e-4 2e-3]
%-(  e x39) Act-mass (Ew+Er) --> all nom, v= 3 m/s (''), Slope= [-0.25:0.05:0.25] add [-0.03 -0.01] for  DR=0, add [-0.08 -0.07] for DR=0.10, add [-0.13 -0.08] for DR=0.20,  DR= [0 0.10 0.20]
%-(  f x38) Act-mass (Ew+Er) --> all nom, v= 3 m/s (''), Slope= [-0.25:0.05:0.25] add [-0.03      ] for  CF=0, add [-0.08 -0.07] for CF=0.03, add [-0.08 -0.07] for CF=0.06,  CF= [0 0.03 0.06]
k_ref= 27370.46501693961;
S21.Name=              [repelem({'PMR'},1,5) repelem({'srinR'},1,4) repelem({'PMR'},1,97) repelem({'srinR'},1,11) repelem({'PMR'},1,112)]; %-Model name code
S21.ScaleWork=         [repelem({'yes'},1,229)]; %-Scale actuator work
S21.ForceRateCoef=     [zeros(1,5) 10.^(-[2:5]) zeros(1,2) zeros(1,12) [1 2 5 8 1 2 3 5 1 2].*10.^(-[4 4 4 4 3 3 3 3 2 2]) 5e-4*ones(1,84) zeros(1,13) 5e-4*ones(1,11) 2e-3*ones(1,11) 5e-4*ones(1,77)]; %-Force-rate cost coefficient
S21.DampingRatio=      [zeros(1,9) 0.10*ones(1,97) zeros(1,11) 0.10*ones(1,35) zeros(1,13) 0.10*ones(1,13) 0.20*ones(1,13) 0.10*ones(1,38)]; %-Tendon damping ratio
S21.CollisionFraction= [zeros(1,9) 0.03*ones(1,97) zeros(1,11) 0.03*ones(1,74) zeros(1,12) 0.03*ones(1,13) 0.06*ones(1,13)]; %-Fraction of CoM momentum lost in collision
S21.SpringConstant=    [0.45 0.18 0.45 1.33 4.45 inf(1,4) 0.5 4 repmat([0.5 1 2 4],1,3) 1.3*ones(1,18) repmat([0.50 0.70 1.0 1.3 1.7 2.0 2.5 3.0 3.5 4.0],1,3) 1.3*ones(1,13) repelem([0.5 2 inf],1,11) 1.3*ones(1,112)]*k_ref; %-Spring constant
S21.GroundSlope=       [zeros(1,71) sort([[-0.25:0.05:0.25] [-0.08 -0.07]],'ascend') repmat([-0.25:0.05:0.25],1,3) sort([[-0.25:0.05:0.25] [-0.08 -0.07]],'ascend') repmat([-0.25:0.05:0.25],1,2) sort([[-0.25:0.05:0.25] [-0.03 -0.01]],'ascend') sort([[-0.25:0.05:0.25] [-0.08 -0.07]],'ascend') sort([[-0.25:0.05:0.25] [-0.13 -0.08]],'ascend') sort([[-0.25:0.05:0.25] [-0.03]],'ascend') repmat(sort([[-0.25:0.05:0.25] [-0.08 -0.07]],'ascend'),1,2)]; %-Ground slope
S21.RelativeGravity=   [ones(1,229)]; %-Gravity realtive to Earth
S21.Speed=             [3.9 3.5*ones(1,8) 3*ones(1,2) repelem([2.5 3.0 3.5],1,4) 3*ones(1,10) 3.9 2 2.22 2.5 3 3.5 3.89 4 repelem([2.5 3.0 3.5],1,10) 3*ones(1,158)]; %-Running speed
S21.StepLength=        [1.3 repelem({'G06'},1,32)  1.30 repelem({'G06'},1,195)]; %-Step length
S21.BodyMass=          [75.30 repelem(   70.0,1,32) 75.30 repelem(   70.0,1,195)]; %-Body mass
S21.MaxLegLength=      [ 0.79 repelem(    0.9,1,32)  0.79 repelem(    0.9,1,195)]; %-Maximum allowable leg length
% S21.SaveName=     'Test_001'; %-Name you would like to save optimization output as (input: character array), default= fnID
% S21.Preset=               ''; %-Define problem parameters from presets (see ReadMe for options)
ProbSet.SPmat= 1:229; %-For multiple optimizations, define SPmat with desired field in structure Model
  Model.SPmat= 1:229; %-For multiple optimizations, define SPmat with desired field in structure Model

SPps= S21;


%-turn off other plots
View.PlotSummary= 0;
View.Simulation=  0;
View.Cost=        0;
View.DutyFactor=  0;


elseif isempty(View.Preset) %-no preset
SPps= [];


else %-unknown preset
error(['Unknown Model.Preset: ' View.Preset]);

end




end %-end function