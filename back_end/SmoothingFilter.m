function [yf,w] = SmoothingFilter(x,y,DAQr,w)
%% This function attempts to remove noise from a given signal by calculating
%  the mean value for successive chunks of data centered around points
%  (x,y) and bounded by the width (w). Only 2D data currently works. The 
%  number of data points in a given chunk is equal to 2*w+1.

%  ***NOTE*** ESSENTIALLY, THIS IS A MOVING AVERAGE FILTER
%  ***NOTE*** If there is no "w" input, the function will allow the user to
%             explore w values.
       

if length(x)>ceil(8*DAQr)
    e = ceil(8*DAQr);
else
    e = length(x);
end

if nargin==3
figure
huf = plot(x(1:e),y(1:e),'b'); grid on; hold on; set(huf,'LineWidth',2);
xlabel('X','FontSize',14); ylabel('Signal','FontSize',14);
title('Smoothing Filter','FontSize',28);
hstop = uicontrol('Style', 'PushButton', 'String', 'Finished', ...
                  'Callback', 'delete(gcbo)');
hslid = uicontrol('style','slider','units','pixel','position',[200 10 300 20],'Min',1,'Max',10*DAQr,'Value',1,'SliderStep',[0.001 0.01]);
drawnow;
elseif nargin==4
hstop=plot(nan); delete(hstop);
end


hf = plot(nan); ht = plot(nan);
while ishandle(hstop)
% Check to make sure there is enough data supported by width (w)
  w=floor(get(hslid,'Value')); wa = 2*w+1;

if length(x)<2*w
    fprintf('!!!ERROR!!! \n Your width (w) is less than twice the number of data points in x and y.');
else
for i=1:length(y)
    if i-w<1 && i+w>length(y)
        yf(i) = mean(y(1:length(y)));
    elseif i-w<1 && i+w<=length(y)
        yf(i) = mean(y(1:i+w));
    elseif i-w>=1 && i+w>length(y)
        yf(i) = mean(y(i-w:length(y)));
    elseif i-w>=1 && i+w<=length(y)
        yf(i) = mean(y(i-w:i+w));
    end
end
end
     
  disp(datestr(now, 0));
  delete(hf);
  hf = plot(x(1:e),yf(1:e),'r');
  txtw = sprintf('w = %1.0f',wa);
  delete(ht);
  ht = uicontrol('Style', 'text',...
                 'String', txtw,... %replace something with the text you want
                 'Units','normalized',...
                 'FontSize',14,...
                 'Position', [0.8 0.005 0.05 0.05]); 
  set(hf,'LineWidth',4);
  pause(0.25);
  clc;
legend('Unfiltered Signal','Filtered Signal');
end
clc; close;

if nargin==3
wt = wa/DAQr;
endfilter = sprintf('You have stopped on a width value of w = %1.0f = %1.2fs\n\n\n\n\n\n\n\n\n\n',wa,wt);
fprintf(endfilter);
elseif nargin==4
    % Check to make sure there is enough data supported by width (w)
      wa = 2*w+1;

if length(x)<2*w
    fprintf('!!!ERROR!!! \n Your width (w) is less than twice the number of data points in x and y.');
else
for i=1:length(y)
    if i-w<1 && i+w>length(y)
        yf(i) = mean(y(1:length(y)));
    elseif i-w<1 && i+w<=length(y)
        yf(i) = mean(y(1:i+w));
    elseif i-w>=1 && i+w>length(y)
        yf(i) = mean(y(i-w:length(y)));
    elseif i-w>=1 && i+w<=length(y)
        yf(i) = mean(y(i-w:i+w));
    end
end
end

    figure;
    huf = plot(x(1:e),y(1:e),'b'); grid on; hold on; set(huf,'LineWidth',2);
    xlabel('X','FontSize',14); ylabel('Signal','FontSize',14);
    title('Smoothing Filter','FontSize',28);
    hf = plot(x(1:e),yf(1:e),'r');
    set(hf,'LineWidth',4);
    legend('Unfiltered Signal','Filtered Signal');
end

% pause(0.1);
close;