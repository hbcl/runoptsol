function [Model, auxdata]= CalcParameters(Model, ProbSet, auxdata)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> structure with problem parameters
%  iSP     --> Sweeping parameter iteration
%===================================================================Outputs
%  Model   --> structure with model parameters
%  auxdata --> input auxdata updated with random initial guesses
%==========================================================================
%  This function calculates parameter values for optimization

iSP= ProbSet.iSP;

Model_cell= struct2cell(Model); %-convert Model struct to cell array
fnames= fieldnames(Model);      %-field names in Model struct
iAP= [cellfun(@isnumeric,Model_cell).*cellfun('size',Model_cell,2)];
iAP(find(iAP>1))= iSP;



i= strcmp(fnames,'Speed');                Sp= Model_cell{i}(:,iAP(i)); %-speed
i= strcmp(fnames,'BodyMass');             M = Model_cell{i}(:,iAP(i)); %-body mass
% i= strcmp(fnames,'DutyFactor');        DF= Model_cell{i}(:,iAP(i)); %-duty factor
DF= nan;
i= strcmp(fnames,'GroundSlope');          S = Model_cell{i}(:,iAP(i)); %-ground slope
i= strcmp(fnames,'DampingRatio');         DR= Model_cell{i}(:,iAP(i)); %-damping ratio
i= strcmp(fnames,'MaxLegLength');         hb= Model_cell{i}(:,iAP(i)); %-max allowable leg length
i= strcmp(fnames,'ForceRateCoef');        cR= Model_cell{i}(:,iAP(i)); %-force rate coefficient
i= strcmp(fnames,'SpringConstant');       k = Model_cell{i}(:,iAP(i)); %-leg spring constant
i= strcmp(fnames,'RelativeGravity');      gC= Model_cell{i}(:,iAP(i)); %-relative gravity
i= strcmp(fnames,'CollisionFraction');    CF= Model_cell{i}(:,iAP(i)); %-collision fraction
i= strcmp(fnames,'MinMax_Force');       mmFm= Model_cell{i}(:,iAP(i)); %-constraint on actautor force
i= strcmp(fnames,'MinMax_ForceRate');   mmFR= Model_cell{i}(:,iAP(i)); %-constraint on actautor force rate
i= strcmp(fnames,'MinMax_MechPower');   mmPm= Model_cell{i}(:,iAP(i)); %-constraint on actautor mechanical power
i= strcmp(fnames,'MinMax_MetPower');    mmER= Model_cell{i}(:,iAP(i)); %-constraint on actautor energy cost rate



i= strcmp(fnames,'StepLength');
if ischar(Model_cell{i})
if strcmpi(Model_cell{i},'G06')
fs= 0.2592*Sp+2.1651; %[Hz] step frequency: relationship based on Gutman et al., 2006
ds= Sp./fs; %[m] step length
else
error(['Unknown code for Model.StepLength: ' Model_cell{i}]);
end
elseif isnumeric(Model_cell{i})
ds= Model_cell{i}(:,iAP(i)); %[m] step length
fs= Sp./ds; %[Hz] step frequency
end


fnID= auxdata.fnID;
gE  =  9.81; %[m/s2] Earth's gravitational acceleration
 

if strcmpi(Model.Preset,'Polet2018')
%(parameter values taken from red grv running study: Polet et al., 2018)
gC   = [0.15  0.25  0.35  0.50  1.00]';
UC   = [0.50  1.00  2.00  3.06  4.17];
fsMat= [1.31  1.52  1.85  2.09  2.46
        1.67  1.88  2.03  2.29  2.65
        1.76  1.88  2.13  2.37  2.76
        1.95  2.05  2.26  2.54  2.88
        2.34  2.45  2.69  2.94  3.10];

hb=  0.92; %[ m] max leg length
M = 67.28; %[kg] body mass

% if Model.SPmat==Model.RelativeGravity
% g = SPmat(iSP)*gE; %[m/s2] gravitational acceleration
% Sp= Model.Speed;   %[m/s ] treadmill speed
% elseif Model.SPmat==Model.Speed
% g = Model.RelativeGravity*gE; %[m/s] gravitational acceleration
% Sp= SPmat(iSP);               %[m/s ] treadmill speed
% end
fs = fsMat(find(g/gE==gC),find(Sp==UC)); %[Hz] step frequency
ds= Sp/f; %[m] step length

Model.RelativeGravity= gC;
Model.Speed= UC;


end
Model.MaxLegLength= hb;
Model.BodyMass=     M;
Model.StepLength=   ds;

Ts= 1/fs; %[s] step time period for running
alpha= atan2d(S,1); %[deg] angle of gravity vector (reorients gravity depending on ground slope)
g= gC*gE*[sind(alpha) cosd(alpha)]; %[m/s2] gravitational acceleration

hm= 0.5*hb; %[m] max muscle length (arbitrary #) - gives muscle room to make length changes [default: 0.2 or 0.5?]
if contains(fnID,'_noCol') && contains(fnID,'_noDmp') && contains(fnID,'_noFRC')
hm= 0;
end
dh_mb= hb-hm; %[ m] max spring-damper length
m= CF*M;      %[kg] collision mass (m/M represents momentum lost in collision)


cc= 2*sqrt(k*M); %[Ns/m] critical damping
if DR==0; DR= nan; end
c= [1 1]*cc*DR; %[Ns/m] leg damping coefficient
k= [1 1]*k;

%-cost scaling coefficients
epsC = [1e-9 cR 1e+5]; % [#] eps constants for scaling cost terms default:[1e-9 1e-4] eps(2) seems to be pretty close to balanced


%% ==========================================================Define Auxdata
auxdata.dhmb=  dh_mb; %[   m] hb - hm
auxdata.h__b=     hb; %[   m] max leg    height during straight standing
auxdata.h__m=     hm; %[   m] max muscle length during straight standing
auxdata.stpL=     ds; %[   m] step length during running
auxdata.mcom=      M; %[  kg] body mass
auxdata.mcol=      m; %[  kg] collision mass (m/M represents momentum loss due to collision)
auxdata.grav=      g; %[m/s2] gravitational acceleration
auxdata.mfrq=     fs; %[  Hz] hopping frequency
auxdata.DFac=     DF; %[   #] duty factor
auxdata.sprC=      k; %[ N/m] leg spring constant
auxdata.dmpC=      c; %[Ns/m] leg damping coefficient
% auxdata.fref=  f_ref; %[  Hz] reference frequency (fn)
auxdata.epsC=   epsC; % cost scaling coefficients
auxdata.cnstr.mmFm=  mmFm; %[nd] max actuator force
auxdata.cnstr.mmFR=  mmFR; %[nd] max actuator force rate
auxdata.cnstr.mmPm=  mmPm; %[nd] max actuator power
auxdata.cnstr.mmER=  mmER; %[nd] max actuator energy cost rate





end

