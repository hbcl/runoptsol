function [auxdata ProbSet]= FreeActuatorJerkBounds(Model, ProbSet)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%  auxdata --> structure with problem parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%==========================================================================
%  This function adjusts some optimization parameters to help with certain 
%  optimization problems, namely where actuator jerk has no bounds


iSP= ProbSet.iSP;

Model_cell= struct2cell(Model); %-convert Model struct to cell array
fnames= fieldnames(Model);%-field names in Model struct
iAP= [cellfun(@isnumeric,Model_cell).*cellfun('length',Model_cell)];
iAP(find(iAP>1))= iSP;


if round(Model.DampingRatio,2)~=0 | round(Model.CollisionFraction,2)~=0 |...
           Model.ForceRateCoef~=0 | round(Model.GroundSlope,2)~=0
   ProbSet.PP.TotalIteration=    30; %double perterbuation N since no reiteration of seed
   ProbSet.PP.PeturbMagnitude= 1/20; %use smaller perturbation magnitude
   
   if round(Model.CollisionFraction,2)>0 & round(Model.DampingRatio,2)==0
   auxdata.jrkB= 8e2; %-bounds on actuator jerk
   else
   i= strcmp(fnames,'SpringConstant'); k= Model_cell{i}(iAP(i));
   k_ref= 27370.46501693961;
   k_rel= k/k_ref;
   if k_rel<2
   auxdata.jrkB= 2.0e2;
   else
%    auxdata.jrkB= 4.0e3;
   auxdata.jrkB= 2.0e3; %-bounds on actuator jerk
   end
   end
end






end




