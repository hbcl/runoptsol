function ProbSet= SetDirectory
%===================================================================Outputs
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%==========================================================================
%  This function sets the directory




ProbSet.Home= startupFileGPOPSII;
if ispc; s='\'; else s='/'; end
ProbSet.TempFolder= [s 'Mon1'];
ProbSet.TempFiles = [{'MonRG.mat'} {'MonPP.mat'}];
cd([ProbSet.Home ProbSet.TempFolder]);

end