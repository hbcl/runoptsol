function [cycSP cycPTSint HS PO freq pat] = BasicGaitAnalysis(sig,gait,meas,mode,workflow,DAQr,pat,View)
%% This program will perform basic gait analysis including stride frequency,
%  pushoff and heelstrike. This function assumes a steady state locomotion.

%-------------------------------OUTPUTS------------------------------------
%  cycSP     ==> starting point for each cycle
%  cycPTSint ==> interpolated values for each cycle
%  HS        ==> heelstrike locations
%  PO        ==> pushoff locations (PO=cycSP for a walk measured at foot)

%-------------------------------INPUTS-------------------- ----------------
%  sig       ==> the signal wanting analysis
%  gait      ==> the gait type (e.g. walk or run) ***For n/a, enter run
%  meas      ==> where is the sensor located? (e.g. foot or CoM) ***For n/a, enter CoM
%  mode      ==> what do you choose as a starting point? (e.g. max or min)
%  workflow  ==> choose either manual or auto for automation
%  DAQr      ==> data acquisition frequency
%  View      ==> view frequency measurement? (0 or 1)

% clc;clear HS PO;close all;
zth=0.5;pth=1;itr=1;
% figure; plot(sig(1:200),'b');hold on;
% % % % if isnan(pat)==1
% % % % if strcmpi(mode,'max')==1 && strcmpi(gait,'walk')==1 &&...
% % % %                      strcmpi(meas,'foot')==1 && strcmpi(workflow,'auto')==1
% % % % while median(abs(sig(itr:itr+10)))>zth
% % % %     itr=itr+1;
% % % % %     cur=plot(itr,sig(itr),'ro');pause(0.1);
% % % % %     delete(cur);
% % % % end
% % % % while median(abs(sig(itr:itr+5)))<pth
% % % %     itr=itr+1;
% % % % %         cur=plot(itr,sig(itr),'go');pause(0.1);
% % % % %     delete(cur);
% % % % end
% % % % b4=itr;af=b4+20;
% % % % pat(1)=find(max(sig(b4:af))==sig); pat(2)=0; PO(1)=pat(1);
% % % % %     cur=plot(itr,sig(itr),'ro');pause(0.1);
% % % % %     delete(cur);
% % % % %     pat1=plot(pat(1),sig(pat(1)),'gx');pause();
% % % % while median(abs(sig(itr:itr+10)))>zth
% % % %     itr=itr+1;
% % % % %     cur=plot(itr,sig(itr),'ro');pause(0.1);
% % % % %     delete(cur);
% % % % end
% % % % 
% % % % HS(1)=find(max(sig(itr-20:itr))==sig);
% % % % itr=itr+10;
% % % % while median(abs(sig(itr:itr+5)))<pth
% % % %     itr=itr+1;
% % % % %     cur=plot(itr,sig(itr),'ro');pause(0.1);
% % % % %     delete(cur);
% % % % end
% % % % 
% % % % b4=itr;af=b4+20;
% % % % pat(3)=find(max(sig(b4:af))==sig)-pat(1); pat(4)=0;
% % % % %     pat3=plot(pat(1)+pat(3),sig(pat(1)+pat(3)),'gx');pause();
% % % % 
% % % % else
% % % % figure; tfi=8;
% % % % if length(sig)<tfi*DAQr
% % % % tfi=length(sig)/DAQr;
% % % % end
% % % % SamPat=plot(1:DAQr*tfi,sig(1:DAQr*tfi));
% % % % xlim([1 DAQr*8]);
% % % % ylim([min(sig(1:DAQr*tfi))-0.08*range(sig(1:DAQr*tfi))...
% % % %     max(sig(1:DAQr*tfi))+0.08*range(sig(1:DAQr*tfi))]);
% % % % title('Please designate a pattern to be sequenced by dragging a box with the cursor.');
% % % % xlabel('Time [pts]'); ylabel('Sample Signal');
% % % % pat = getrect();close;     % Frame one good cycle (for every subject in steady state gait)
% % % % end
% % % % end
tfi=8;
if length(sig)<tfi*DAQr
tfi=length(sig)/DAQr;
end
if mean(isnan(pat))==1
if mean(isnan(pat))==1 && strcmpi(mode,'max')==1 && strcmpi(gait,'walk')==1 &&...
                                                    strcmpi(meas,'foot')==1
%% =================================================Approximate First Cycle
% figure;
chW=round(0.5*DAQr);j=1;
while chW<=round(1.5*DAQr)
trySP = 1:chW:length(sig);
for i=1:length(trySP)-1
chunk(:,i) = sig(trySP(i):trySP(i+1)-1);
if i>1
   SimCH(i)=mean(log(abs(chunk(:,i-1)-chunk(:,i))+1));
end
end
% plot(chunk);
SimCHm(j)=mean(SimCH);
j=j+1;
% pause()
clear SimCH chunk; chW=chW+1;
end
% figure;plot(SimCHm,'bd');grid on;
FC = sig(1:0.5*DAQr+find(min(SimCHm)==SimCHm)); %---------------First Cycle

%% ================================Define Peak Domains and Determine 1st PO
dw=5;
for i=1:length(FC)
    if i-dw<1
        Var(i) = var(FC(1:i+dw));
    elseif i+dw>length(FC)
        Var(i) = var(FC(i-dw:length(FC)));
    else
        Var(i) = var(FC(i-dw:i+dw));
    end
end
Sep1 = find(min(Var)==Var);  %-----------------------------Mid Stance Point
Sep2 = find(min(FC)==FC);    %------------------------------------Leg Swing
if Sep1<Sep2
        pk1 = Sep1+find(max(FC(Sep1:Sep2))==FC(Sep1:Sep2))-1;
elseif Sep2<Sep1
    if max(FC(1:Sep2))<max(FC(Sep1:length(FC)))
        pk1 = Sep1+find(max(FC(Sep1:length(FC)))==FC(Sep1:length(FC)))-1;    
    elseif max(FC(1:Sep2))>max(FC(Sep1:length(FC)))
        pk1 = find(max(FC(1:Sep2))==FC(1:Sep2));
    end
end
pat(1)=pk1; pat(2)=0; pat(3)=length(FC); pat(4)=0;

elseif mean(isnan(pat))==1 && strcmpi(gait,'walk')~=1 || strcmpi(meas,'foot')~=1
figure; tfi=8;
if length(sig)<tfi*DAQr
tfi=length(sig)/DAQr;
end
SamPat=plot(1:DAQr*tfi,sig(1:DAQr*tfi));
xlim([1 DAQr*8]);
ylim([min(sig(1:DAQr*tfi))-0.08*range(sig(1:DAQr*tfi))...
    max(sig(1:DAQr*tfi))+0.08*range(sig(1:DAQr*tfi))]);
title('Please designate a pattern to be sequenced by dragging a box with the cursor.');
xlabel('Time [pts]'); ylabel('Sample Signal');
pat = getrect();close;     % Frame one good cycle (for every subject in steady state gait)
end

pkT=5;
%% ============================================Fine Tune Peaks in 1st Cycle
if strcmpi(mode,'max')==1
    if pat(1)-pkT<1
pat(1)=find(max(sig(1:round(pat(1))+pkT))==sig);        
    else
pat(1)=find(max(sig(round(pat(1))-pkT:round(pat(1))+pkT))==sig);        % Chosen push off peak defines start point of pattern
pat(3)=find(max(sig(round(pat(1)+pat(3))-pkT:round(pat(1)+pat(3))+pkT))==sig)-1-pat(1);      % Wavelength of pattern chosen
    end
elseif strcmpi(mode,'min')==1
pat(1)=find(min(sig(round(pat(1))-5:round(pat(1))+5))==sig);        % Chosen push off peak defines start point of pattern
pat(3)=find(max(sig(round(pat(1)+pat(3))-5:round(pat(1)+pat(3))+5))==sig)-1-pat(1);
elseif strcmpi(mode,'zero')==1
pat(1)=find(sig(round(pat(1))-5:round(pat(1))+5)<0,1,'last')+1;        % Chosen push off peak defines start point of pattern
pat(3)=find(sig(round(pat(1)+pat(3))-5:round(pat(1)+pat(3))+5)<0,1,'last')-pat(1);
end
end

cycSP(1) = pat(1);           % Starting point of first cycle
cycSP(2) = pat(1)+pat(3)+1;  % Starting point of second cycle
 patPTS  = sig(pat(1):pat(1)+pat(3)); % Points defining the first cycle
 patPTSint = spline(cycSP(1):cycSP(1)+pat(3),patPTS,linspace(cycSP(1),cycSP(1)+pat(3),100));  % Spline interpolation by 100 pts
cycPTSint(:,1,1) = linspace(cycSP(1),cycSP(1)+pat(3),100);
cycPTSint(:,1,2) = patPTSint;
 

n=3;
% figure;
while cycSP(n-1)+pat(3)+round(pat(3)/4)<=length(sig)
%     figure;
for i=-round(pat(3)/2):round(pat(3)/4)
      potPTS    = sig(cycSP(n-1):cycSP(n-1)+pat(3)+i);
      potPTSint = spline(cycSP(n-1):cycSP(n-1)+pat(3)+i,potPTS,linspace(cycSP(n-1),cycSP(n-1)+pat(3)+i,100));
      V(i+round(pat(3)/2)+1)=var(patPTSint-potPTSint);
%       subplot(2,1,1);hold on;
% h(1)=plot(patPTSint,'b');h(2)=plot(potPTSint,'r');
% subplot(2,1,2);
% h(3)=plot(V,'g'); 
% % pause(0.05)
% delete(h(2));
end
% delete(h);

CHpk=10;
Vmin=find(min(V)==V)-round(pat(3)/2)-1;
if strcmpi(mode,'max')==1
for i=1:CHpk
    if length(sig)<cycSP(n-1)+pat(3)+Vmin+1+CHpk/2
    cycSP(n)=find(max(sig(cycSP(n-1)+pat(3)+Vmin+1-CHpk/2:length(sig)))==sig);
    else
    cycSP(n)=find(max(sig(cycSP(n-1)+pat(3)+Vmin+1-CHpk/2:cycSP(n-1)+pat(3)+Vmin+1+CHpk/2))==sig);
    end
end
elseif strcmpi(mode,'min')==1
for i=1:CHpk
    cycSP(n)=find(min(sig(cycSP(n-1)+pat(3)+Vmin+1-CHpk/2:cycSP(n-1)+pat(3)+Vmin+1+CHpk/2))==sig);
end
elseif strcmpi(mode,'zero')==1
for i=1:CHpk
    cycSP(n)=find(sig(cycSP(n-1)+pat(3)+Vmin+1-CHpk/2:cycSP(n-1)+pat(3)+Vmin+1+CHpk/2)<0,1,'last')+1;
end
end

newPTS    = sig(cycSP(n-1):cycSP(n)-1);
newPTSint = spline(cycSP(n-1):cycSP(n)-1,newPTS,linspace(cycSP(n-1),cycSP(n)-1,100));
cycPTSint(:,n-1,1) = linspace(cycSP(n-1),cycSP(n)-1,100);
cycPTSint(:,n-1,2) = newPTSint;

% subplot(2,1,1);
% hold on;
% h(1)=plot(patPTSint,'b');
% h(2)=plot(newPTSint,'r');
% subplot(2,1,2);
% h(3)=plot(V,'g'); 
% pause()
% 
% Detect HS and label PO
% % % if strcmpi(mode,'max')==1 && strcmpi(gait,'walk')==1 && strcmpi(meas,'foot')==1
% % % i=cycSP(n)-10;iT=1;
% % % while median(sig(i-10:i))>zth
% % % %     h(3)=plot(i,sig(i),'bo');hold on;pause(0.3)
% % % i=i-1;
% % % % delete(h(3));
% % % end
% % % while median(sig(i-5:i))<pth
% % % %         h(3)=plot(i,sig(i),'bo');pause(0.3)
% % % i=i-1;
% % % % delete(h(3));
% % % end
% % % while sum(sig(i-4:i))>0
% % % %     check=iT
% % %     if iT==1
% % %         Aft=i;
% % %     end
% % % %         h(3)=plot(i,sig(i),'bo');
% % % Pctr(iT)=sum(sig(i-2:i));
% % % % pause()
% % % iT=iT+1;i=i-1;
% % % % delete(h(3));
% % % end
% % % Bfr=i;
% % % % pause()
% % % HS(n-1)=find(max(sig(Bfr:Aft))==sig);
% % % PO(n-1)=cycSP(n-1);
% % % end
% % % if strcmpi(gait,'walk')==0 || strcmpi(meas,'foot')==0
    HS=nan;PO=nan;
% % % end


% delete(h);
clear V;n=n+1;
% close;
end
freq = [(cycSP(1:length(cycSP)-1)./DAQr); DAQr./diff(cycSP)];

if View==1
figure;plot(cycSP(1:length(cycSP)-1)./DAQr,DAQr./diff(cycSP),'bd');grid on;
title('Stride Frequency vs. Time'); ylim([0 max(freq(2,:))+3*range(freq(2,:))]);
xlabel('Time [s]'); ylabel('Stride Frequency [Hz]');
txt=sprintf('Ave. Freq. = %1.3f',mean(freq(2,:)));
text(mean(freq(1,:)),mean(freq(2,:))-3*range(freq(2,:)),txt);
end

% if strcmpi(mode,'max')==1 && strcmpi(gait,'walk')==1 && strcmpi(meas,'foot')==1
% figure;plot(sig(1:DAQr*tfi),'b','LineWidth',4);hold on;
% for i=2:n-2
%     if PO(i)<=DAQr*tfi && HS(i)<=DAQr*tfi
%    plot([PO(i) PO(i)],[0 sig(PO(i))],'r','LineWidth',2);plot(nan,'k--','LineWidth',2);
%    plot([HS(i) HS(i)],[0 sig(HS(i))],'r','LineWidth',2);
%     else
%         break
%     end
% end
% hr=rectangle('Position',[pat(1) min(sig(pat(1):pat(1)+pat(3)))...
%                              pat(3) max(sig(pat(1):pat(1)+pat(3)))+...
%                                      abs(min(sig(pat(1):pat(1)+pat(3))))]);
% set(hr,'EdgeColor','k','LineWidth',2,'LineStyle','--'); grid on;
% title('Signal with Peaks Detected'); xlabel('Time [s]'); ylabel('Signal');
% legend('Signal','Peaks Detected','User Designated Pattern');
% end
HS=nan;PO=nan;


end