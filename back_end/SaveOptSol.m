function SaveOptSol(Model, ProbSet, OptSol)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  OptSol  --> Optimal solution to be saved
%==========================================================================
%  This function saves the optimal solution to desired save location


fnID=   ProbSet.fnID;
Home=   ProbSet.Home;
SaveOP= ProbSet.SaveOP;


if SaveOP==1
   if ispc; s='\'; else s='/'; end
   if isempty(Model.SaveName)
   fsave=    [Home s 'OptSols' s fnID '.mat'];
   WorkFile= [Home s 'OptSols' s 'working' fnID(findstr(fnID,'OptSol')+6:end) '.mat'];
   else
   if strcmpi(Model.SaveName(end-3:end),'.mat')
   fsave=    [Home s 'OptSols' s Model.SaveName];   
   WorkFile= [Home s 'OptSols' s 'working_' Model.SaveName];
   else
   fsave=    [Home s 'OptSols' s Model.SaveName '.mat'];
   WorkFile= [Home s 'OptSols' s 'working_' Model.SaveName '.mat'];
   end
   end
   save(fsave,'OptSol')

   
   if exist(WorkFile,'file')==2
   delete(WorkFile);
   end
end





end