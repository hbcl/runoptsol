function CheckParametersMatch(Model)
%-This function checks that parameter fields match as expected and sends an
%-error if there is not a match.

Check.Name=              [];%-(01)
Check.ScaleWork=         [];%-(02)
Check.ForceRateCoef=     [];%-(03)
Check.DampingRatio=      [];%-(04)
Check.CollisionFraction= [];%-(05)
Check.SpringConstant=    [];%-(06)
Check.GroundSlope=       [];%-(07)
Check.RelativeGravity=   [];%-(08)
Check.Speed=             [];%-(09)
Check.StepLength=        [];%-(10)
Check.BodyMass=          [];%-(11)
Check.MaxLegLength=      [];%-(12)
Check.SaveName=          [];%-(13)
Check.Preset=            [];%-(14)
Check.SPmat=             [];%-(15)


if length(fieldnames(rmfield(Model,intersect(fieldnames(Model),fieldnames(Check)))))~=0 |...
   length(fieldnames(rmfield(Check,intersect(fieldnames(Model),fieldnames(Check)))))~=0
%-Since parameters are defined by Model fields in alphabetical order,
%-changes to the struct must be reflected in this function as well.
%-Total list of parameters shown in Check struct above.
msg= sprintf(['Check parameter definitions in "CalcParameters()".\n',...
              'Parameter structure fields do not match.\n']);
error(msg);
end





end