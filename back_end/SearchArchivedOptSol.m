function [Model, ProbSet, auxdata]= SearchArchivedOptSol(Model, ProbSet, auxdata)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> structure with model parameters
%===================================================================Outputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> auxdata updated with fnID, etc.
%==========================================================================
%  This function searches for an OptSol in "OptSol\Archives\..." and
%  updates optimization initial guesses and parameters as neccessary


if ispc; s='\'; else; s='/'; end
fn= [ProbSet.Home s 'OptSols' s 'Archives' s Model.Name s ProbSet.fnID '.mat'];


%-if file exists in archive
if exist(fn,'file')==2
load(fn);


%-if file doesn't exist in archive
elseif exist(fn,'file')~=2
fprintf(['File does not exist in Archives by the name of: \n\n' ProbSet.fnID '.mat\n\n',...
         'Please select an existing OptSol to replicate from Archives.\n']);
[fpic dir]= uigetfile([ProbSet.Home s 'OptSols' s 'Archives' s '*.*']);clc;
fn= [dir fpic];
load(fn);

ProbSet.fnID= OptSolCur.result.setup.auxdata.fnID;
auxdata.fnID= OptSolCur.result.setup.auxdata.fnID;

Model.Name= OptSolCur.name;

if length(strfind(fn,'_DiffWorkEff'))
Model.ScaleWork= 'yes';
else
Model.ScaleWork= 'no';
end

if length(strfind(fn,'_noFRC'))
Model.ForceRateCoef= 0;
else length(strfind(fn,'_wFRC'))
if isfield(OptSolCur.result.setup.auxdata,'epsC')
Model.ForceRateCoef= OptSolCur.result.setup.auxdata.epsC(2);
end
end

if length(strfind(fn,'_noDmp'))
Model.DampingRatio= 0;
elseif length(strfind(fn,'_DmpRat'))
if isfield(OptSolCur.result.setup.auxdata,'dmpC')
Model.DampingRatio= OptSolCur.result.setup.auxdata.dmpC(1);
end
end

if isfield(OptSolCur.result.setup.auxdata,'sprC')
Model.SpringConstant= OptSolCur.result.setup.auxdata.sprC(1);
end

if isfield(OptSolCur.result.setup.auxdata,'mcom')
Model.BodyMass= OptSolCur.result.setup.auxdata.mcom;
end

if isfield(OptSolCur.result.setup.auxdata,'h__b')
Model.MaxLegLength= OptSolCur.result.setup.auxdata.h__b;
end

if length(strfind(fn,'_wCol'))==0
Model.CollisionFraction= 0;
elseif length(strfind(fn,'_wCol'))
if isfield(OptSolCur.result.setup.auxdata,'mcol')
Model.CollisionFraction= OptSolCur.result.setup.auxdata.mcol;
end
end

if length(strfind(fn,'_Slope'))==0
Model.GroundSlope= 0;
elseif length(strfind(fn,'_Slope'))
ID= fn(strfind(fn,'_Slope')+6:strfind(fn,'_Slope')+10); ID(find(ID=='p'))='.';
Model.GroundSlope= str2num(ID); clear ID;
end

Model.RelativeGravity= sqrt(sum(OptSolCur.result.setup.auxdata.grav.^2));
Model.StepLength= OptSolCur.result.setup.auxdata.stpL;

if isfield(OptSolCur.result.setup.auxdata,'mfrq')
Model.Speed= Model.StepLength*OptSolCur.result.setup.auxdata.mfrq;
end

Model.SaveName= fpic;
end %-end if exist(fn)






%-if seeds field exists
if isfield(OptSolCur.result.setup,'seeds')
ProbSet.RG.Seeds=             OptSolCur.result.setup.seeds.RG;
ProbSet.RG.MeshTolerance=     OptSolCur.result.setup.mesh.tolerance;
ProbSet.RG.SNOPTTolerance=    OptSolCur.result.setup.nlp.snoptoptions.tolerance;
ProbSet.RG.TotalIteration=    length(OptSolCur.result.setup.seeds.RG);
ProbSet.RG.MaxMeshIteration=  OptSolCur.result.setup.mesh.maxiterations;
ProbSet.RG.MaxSNOPTIteration= OptSolCur.result.setup.nlp.snoptoptions.maxiterations;
ProbSet.PP.Seeds=             OptSolCur.result.setup.seeds.PP;
ProbSet.PP.MeshTolerance=     OptSolCur.result.setup.mesh.tolerance;
ProbSet.PP.SNOPTTolerance=    OptSolCur.result.setup.nlp.snoptoptions.tolerance;
ProbSet.PP.TotalIteration=    length(OptSolCur.result.setup.seeds.PP);
ProbSet.PP.MaxMeshIteration=  OptSolCur.result.setup.mesh.maxiterations;
ProbSet.PP.MaxSNOPTIteration= OptSolCur.result.setup.nlp.snoptoptions.maxiterations;


%-no seeds field exists
else
error('Seeds do not exist in this file.');
%-the file does not have a field labeled "seeds". Cannot compare output to 
%-that of archived file.
end %-end if isfield('seeds')









end %-end function