function [output ProbSet]= CheckPartialSolution(Model, auxdata, ProbSet)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  auxdata --> structure with problem parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%  output  --> structure with optimal solutions from every iteration in field "RG"
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%==========================================================================
%  This function checks for saved partial solutions so optimization can 
%  continue and finish

iSP = ProbSet.iSP;
Home= ProbSet.Home;
fnID= auxdata.fnID;

if ispc; s='\'; else s='/'; end
if isempty(Model.SaveName)
fsave= [Home 'OptSols' s 'working_' fnID(findstr(fnID,'OptSol')+6:end) '.mat'];
else
if strcmpi(Model.SaveName(end-3:end),'.mat')
fsave= [Home 'OptSols' s 'working_' Model.SaveName];    
else
fsave= [Home 'OptSols' s 'working_' Model.SaveName '.mat'];
end
end

MaxMeshiMatRG= ProbSet.RG.MaxMeshIteration;
MaxMeshiMatPP= ProbSet.PP.MaxMeshIteration;
% if iSP==2
%     STP=1;
% end
if exist(fsave,'file')==2
load(fsave);
iRG= size(output.RG,1)+1;
    for i1=1:iRG-1
    for i2=1:length(MaxMeshiMatRG)
        CoT_RG(i1,i2,1)= output.RG(i1,i2).result.objective;
         EF_RG(i1,i2,1)= output.RG(i1,i2).result.nlpinfo;
OptSolSeeds.RG(i1,i2  )= output.RG(i1,i2).result.setup.auxdata.guess;
    end
    end
ProbSet.RG.CoT= CoT_RG;
ProbSet.RG.EF =  EF_RG;
ProbSet.RG.seeds= OptSolSeeds.RG;
ProbSet.RG.StartIteration= iRG;

if isfield(output,'PP')
iPP= size(output.PP,1)+1;
    for i1=1:iPP-1
    for i2=1:length(MaxMeshiMatPP)
        CoT_PP(i1,i2,1)= output.PP(i1,i2).result.objective;
         EF_PP(i1,i2,1)= output.PP(i1,i2).result.nlpinfo;
OptSolSeeds.PP(i1,i2  )= output.PP(i1,i2).result.setup.auxdata.guess;
    end
    end
ProbSet.PP.CoT= CoT_PP;
ProbSet.PP.EF =  EF_PP;
ProbSet.PP.seeds= OptSolSeeds.PP;
ProbSet.PP.StartIteration= iPP;
end

else
output= [];
end



end