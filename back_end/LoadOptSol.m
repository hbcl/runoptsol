function [OptSol, Model, ProbSet, auxdata]= LoadOptSol(Model, ProbSet, auxdata)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> structure with model parameters
%===================================================================Outputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> auxdata updated with fnID, etc.
%==========================================================================
%  This function searches for an OptSol in "OptSol\Archives\..." and
%  outputs it.


if ispc; s='\'; else; s='/'; end

if isempty(Model.SaveName); fnID= ProbSet.fnID; else fnID= Model.SaveName; end
fn1= [ProbSet.Home s 'OptSols' s fnID '.mat'];
fn2= [ProbSet.Home s 'OptSols' s 'Archives' s Model.Name s fnID '.mat'];


%-does OptSol exist?
if exist(fn1,'file')==2 %-in OptSol folder?
load(fn1);

elseif exist(fn2,'file')==2 %-in Archives folder?
load(fn2);


%-if file doesn't exist
elseif exist(fn2,'file')~=2 %-so user picks then...
fprintf(['File does not exist in OptSols by the name of: \n\n' fnID '.mat\n\n',...
         'Please select an existing OptSol to to plot figures.\n']);
[fpic dir]= uigetfile([ProbSet.Home s 'OptSols' s '*.*']);clc;
fn= [dir fpic];
load(fn);

ProbSet.fnID= OptSolCur.result.setup.auxdata.fnID;
auxdata.fnID= OptSolCur.result.setup.auxdata.fnID;

Model.Name= OptSolCur.name;

if length(strfind(fn,'_DiffWorkEff'))
Model.ScaleWork= 'yes';
else
Model.ScaleWork= 'no';
end

if length(strfind(fn,'_noFRC'))
Model.ForceRateCoef= 0;
else length(strfind(fn,'_wFRC'))
if isfield(OptSolCur.result.setup.auxdata,'epsC')
Model.ForceRateCoef= OptSolCur.result.setup.auxdata.epsC(2);
end
end

if length(strfind(fn,'_noDmp'))
Model.DampingRatio= 0;
elseif length(strfind(fn,'_DmpRat'))
if isfield(OptSolCur.result.setup.auxdata,'dmpC')
Model.DampingRatio= OptSolCur.result.setup.auxdata.dmpC(1);
end
end

if isfield(OptSolCur.result.setup.auxdata,'sprC')
Model.SpringConstant= OptSolCur.result.setup.auxdata.sprC(1);
end

if isfield(OptSolCur.result.setup.auxdata,'mcom')
Model.BodyMass= OptSolCur.result.setup.auxdata.mcom;
end

if isfield(OptSolCur.result.setup.auxdata,'h__b')
Model.MaxLegLength= OptSolCur.result.setup.auxdata.h__b;
end

if length(strfind(fn,'_wCol'))==0
Model.CollisionFraction= 0;
elseif length(strfind(fn,'_wCol'))
if isfield(OptSolCur.result.setup.auxdata,'mcol')
Model.CollisionFraction= OptSolCur.result.setup.auxdata.mcol;
end
end

if isempty(strfind(fn,'_Slope'))==0
Model.GroundSlope= 0;
else
ID= fn(strfind(fn,'_Slope')+6:strfind(fn,'_Slope')+10); ID(find(ID=='p'))='.';
Model.GroundSlope= str2num(ID); clear ID;
end

Model.RelativeGravity= sqrt(sum(OptSolCur.result.setup.auxdata.grav.^2));
Model.StepLength= OptSolCur.result.setup.auxdata.stpL;

if isfield(OptSolCur.result.setup.auxdata,'mfrq')
Model.Speed= Model.StepLength*OptSolCur.result.setup.auxdata.mfrq;
end

Model.SaveName= fpic;
end %-end if exist(fn)




if exist('OptSol','var')~=1;   OptSol= OptSolCur;   end








end %-end function