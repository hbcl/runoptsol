function PlotPresetFigures(OPvars, OptSol, Model, View, SPps)
%====================================================================Inputs
%  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
%  OptSol --> optimal solution
%  Model  --> structure with SPmat
%  View   --> structure indicating what plots to show
%  SPps   --> structure with model parameters from all OptSols in preset
%==========================================================================
%  This function plots figueres from various presets


if strcmpi(View.Preset,'S21') %-if S21
clc; close all;
PlotS21(OPvars, OptSol, Model, View, SPps);

elseif isempty(View.Preset) %-no preset


else %-unknown preset code
error(['Unknown View.Preset: ' View.Preset]);

end %-preset?











end %-end function