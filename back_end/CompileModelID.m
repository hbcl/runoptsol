function [Model, ProbSet, auxdata]= CompileModelID(Model, ProbSet)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> auxdata updated with fnID
%==========================================================================
%  This function compiles a model identifier string with information about
%  the desired optimization



%-change to srinR model if: k=inf or c=inf
if strcmpi(Model.Name,'PMR') &...
  (isinf(Model.SpringConstant) | isinf(Model.DampingRatio))
Model.Name= 'srinR';
Model.SpringConstant= inf; %-rigid tendon/spring
Model.DampingRatio=   inf;
end




%-model IDs
if strcmpi(Model.Name,'PMR') %-Model PMR
fnID= PMR_IDs(Model, ProbSet);

elseif strcmpi(Model.Name,'srinR') %-Model srinR
fnID= srinR_IDs(Model, ProbSet);

else
error(['Unrecognized model name: ' Model.Name '\n',...
       'See ReadMe for available models and names.\n'],class('')); %-Model ???
end


ProbSet.fnID= fnID;
auxdata.fnID= ProbSet.fnID;


if isfield(ProbSet,'Save')
if strcmpi(ProbSet.Save,'yes')
    ProbSet.SaveOP=1;
elseif strcmpi(ProbSet.Save,'no')
    ProbSet.SaveOP=0;
end
end




end %-end function

