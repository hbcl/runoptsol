function [Model, ProbSet]= CheckDataTypes(Model, ProbSet)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%  Model   --> Model   updated with defaults
%  ProbSet --> ProbSet updated with defaults
%==========================================================================
%  This function checks data types of Model and ProbSet vars and sends an
%  error if it finds something unexpected.



% if ~isstr(Model.Name);              error('Model.Name should be a string');              end
% if ~isstr(Model.ScaleWork);         error('Model.ScaleWork should be a string');         end
% if ~isstr(Model.CollisionFraction); error('Model.CollisionFraction should be a string'); end





if isempty(Model.Name);   Model.Name='srinR';   end


%-model PMR
if strcmpi(Model.Name,'PMR')
%-Model parameters
if isempty(Model.ScaleWork);         Model.ScaleWork=       'yes';  end
if isempty(Model.ForceRateCoef);     Model.ForceRateCoef=    5e-4;  end
if isempty(Model.DampingRatio);      Model.DampingRatio=     0.10;  end
if isempty(Model.SpringConstant);    Model.SpringConstant=  35582;  end
if isempty(Model.CollisionFraction); Model.CollisionFraction=0.03;  end
if isempty(Model.GroundSlope);       Model.GroundSlope=      0.00;  end
if isempty(Model.RelativeGravity);   Model.RelativeGravity=  1.00;  end
if isempty(Model.Speed);             Model.Speed=            3.00;  end
if isempty(Model.StepLength);        Model.StepLength=      'G06';  end
if isempty(Model.BodyMass);          Model.BodyMass=         80.0;  end
if isempty(Model.MaxLegLength);      Model.MaxLegLength=     0.90;  end
if isempty(Model.SPmat);             Model.SPmat=     Model.Speed;  end
%-Model actuator constraints
if isempty(Model.MinMax_Force);      Model.MinMax_Force=     [-inf;inf]; end
if isempty(Model.MinMax_ForceRate);  Model.MinMax_ForceRate= [-inf;inf]; end
if isempty(Model.MinMax_MechPower);  Model.MinMax_MechPower= [-inf;inf]; end
if isempty(Model.MinMax_MetPower);   Model.MinMax_MetPower=  [-inf;inf]; end
%-ProbSet
if exist('ProbSet','var')==1
if isempty(ProbSet.Save);                  ProbSet.Save=               'yes'; end
if isempty(ProbSet.RG.MeshTolerance);      ProbSet.RG.MeshTolerance=    1e-4; end
if isempty(ProbSet.RG.SNOPTTolerance);     ProbSet.RG.SNOPTTolerance=   1e-6; end
if isempty(ProbSet.RG.StartIteration);     ProbSet.RG.StartIteration=      1; end
if isempty(ProbSet.RG.TotalIteration);     ProbSet.RG.TotalIteration=     15; end
if isempty(ProbSet.RG.DownsampleRate);     ProbSet.RG.DownsampleRate=   0.01; end
if isempty(ProbSet.RG.MaxMeshIteration);   ProbSet.RG.MaxMeshIteration=  [2]; end
if isempty(ProbSet.RG.MaxSNOPTIteration);  ProbSet.RG.MaxSNOPTIteration=4000; end
if isempty(ProbSet.PP.MeshTolerance);      ProbSet.PP.MeshTolerance=    1e-4; end
if isempty(ProbSet.PP.SNOPTTolerance);     ProbSet.PP.SNOPTTolerance=   1e-6; end
if isempty(ProbSet.PP.StartIteration);     ProbSet.PP.StartIteration=      1; end
if isempty(ProbSet.PP.TotalIteration);     ProbSet.PP.TotalIteration=     15; end
if isempty(ProbSet.PP.PeturbMagnitude);    ProbSet.PP.PeturbMagnitude=   1/8; end
if isempty(ProbSet.PP.DownsampleRate);     ProbSet.PP.DownsampleRate=   0.01; end
if isempty(ProbSet.PP.MaxMeshIteration);   ProbSet.PP.MaxMeshIteration=  [2]; end
if isempty(ProbSet.PP.MaxSNOPTIteration);  ProbSet.PP.MaxSNOPTIteration=4000; end
end



%-model srinR
elseif strcmpi(Model.Name,'srinR')
%-Model Parameters
if isempty(Model.ScaleWork);         Model.ScaleWork=        'no';  end
if isempty(Model.ForceRateCoef);     Model.ForceRateCoef=    5e-4;  end
if isempty(Model.DampingRatio);      Model.DampingRatio=      inf;  end
if isempty(Model.SpringConstant);    Model.SpringConstant=    inf;  end
if isempty(Model.CollisionFraction); Model.CollisionFraction=0.00;  end
if isempty(Model.GroundSlope);       Model.GroundSlope=      0.00;  end
if isempty(Model.RelativeGravity);   Model.RelativeGravity=  1.00;  end
if isempty(Model.Speed);             Model.Speed=            3.00;  end
if isempty(Model.StepLength);        Model.StepLength=      'G06';  end
if isempty(Model.BodyMass);          Model.BodyMass=         80.0;  end
if isempty(Model.MaxLegLength);      Model.MaxLegLength=     0.90;  end
if isempty(Model.SPmat);             Model.SPmat=     Model.Speed;  end
%-Model actuator constraints
if isempty(Model.MinMax_Force);      Model.MinMax_Force=     [-inf;inf]; end
if isempty(Model.MinMax_ForceRate);  Model.MinMax_ForceRate= [-inf;inf]; end
if isempty(Model.MinMax_MechPower);  Model.MinMax_MechPower= [-inf;inf]; end
if isempty(Model.MinMax_MetPower);   Model.MinMax_MetPower=  [-inf;inf]; end
%-ProbSet
if exist('ProbSet','var')==1
if isempty(ProbSet.Save);                  ProbSet.Save=               'yes'; end
if isempty(ProbSet.RG.MeshTolerance);      ProbSet.RG.MeshTolerance=    1e-4; end
if isempty(ProbSet.RG.SNOPTTolerance);     ProbSet.RG.SNOPTTolerance=   1e-6; end
if isempty(ProbSet.RG.StartIteration);     ProbSet.RG.StartIteration=      1; end
if isempty(ProbSet.RG.TotalIteration);     ProbSet.RG.TotalIteration=     15; end
if isempty(ProbSet.RG.DownsampleRate);     ProbSet.RG.DownsampleRate=   0.01; end
if isempty(ProbSet.RG.MaxMeshIteration);   ProbSet.RG.MaxMeshIteration=  [2]; end
if isempty(ProbSet.RG.MaxSNOPTIteration);  ProbSet.RG.MaxSNOPTIteration=4000; end
if isempty(ProbSet.PP.MeshTolerance);      ProbSet.PP.MeshTolerance=    1e-4; end
if isempty(ProbSet.PP.SNOPTTolerance);     ProbSet.PP.SNOPTTolerance=   1e-6; end
if isempty(ProbSet.PP.StartIteration);     ProbSet.PP.StartIteration=      1; end
if isempty(ProbSet.PP.TotalIteration);     ProbSet.PP.TotalIteration=     15; end
if isempty(ProbSet.PP.PeturbMagnitude);    ProbSet.PP.PeturbMagnitude=   1/8; end
if isempty(ProbSet.PP.DownsampleRate);     ProbSet.PP.DownsampleRate=   0.01; end
if isempty(ProbSet.PP.MaxMeshIteration);   ProbSet.PP.MaxMeshIteration=  [2]; end
if isempty(ProbSet.PP.MaxSNOPTIteration);  ProbSet.PP.MaxSNOPTIteration=4000; end
end


else %-unknown
error(['Unknown Model.Name: ' Model.Name]);
 
end %-which model?


end %-end function