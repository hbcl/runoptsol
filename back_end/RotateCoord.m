function qg= RotateCoord(q,alpha)

%-use rotation matrix to transform local body coordinate into global
Rmat= [cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)];

for i=1:size(q,1)
%-transform coords
qg(i,:)= (Rmat*[q(i,1); q(i,2)])';
end

end
