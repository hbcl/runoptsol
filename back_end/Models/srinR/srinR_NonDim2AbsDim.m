function [t q u p C] = srinR_NonDim2AbsDim(OptSol)
%====================================================================Inputs
%  OptSol  --> optimal solution
%===================================================================Outputs
%  t --> time    var
%  q --> state   vars
%  u --> control vars
%  p --> output parameters from optimization
%  C --> cost terms
%==========================================================================
%  This function dimensionalizes OptSol vars with combinations of M,L and g
%  for model srinR.

% non-dim variables
fnID= OptSol.result.setup.auxdata.fnID; % File ID for saving and Opt desired
   M= OptSol.result.setup.auxdata.mcom; %[  kg] body mass
   g= OptSol.result.setup.auxdata.grav; %[m/s2] gravitational acceleration
  hb= OptSol.result.setup.auxdata.h__b; %[   m] standing height
  gM= sqrt(sum(g.^2));
 tau= sqrt(hb/gM);                      %[   s] time non-dimensionalization factor


iP=1;
p(iP)= OptSol.result.interpsolution.parameter(iP)*hb; %xf - foot position


%-Define opt model outputs
% t= OptSol.result.interpsolution.phase.time;
% q= OptSol.result.interpsolution.phase.state;
% u= OptSol.result.interpsolution.phase.control;
t= OptSol.result.solution.phase.time;
q= OptSol.result.solution.phase.state;
u= OptSol.result.solution.phase.control;
C= OptSol.result.solution.phase.integral;

%-Dimensionalize non-dim model outputs
t= t*tau;

%-states
q(:,1)= q(:,1)*hb;
q(:,2)= q(:,2)*hb/tau;
q(:,3)= q(:,3)*hb;
q(:,4)= q(:,4)*hb/tau;
q(:,5)= q(:,5)*M*gM;
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
q(:,6)= q(:,6)*M*gM/tau;
end

%-controls
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
u(:,1)= u(:,1)*M*gM/tau^2;
else
u(:,1)= u(:,1)*M*gM/tau;
end
u(:,2)= u(:,2)*M*gM*hb/tau;
u(:,3)= u(:,3)*M*gM*hb/tau;
if contains(fnID,'_cFmdd')
u(:,4)= u(:,4)*M*gM/tau^2;
u(:,5)= u(:,5)*M*gM/tau^2;
end
if contains(fnID,'_cAbsFmd')
u(:,4)= u(:,4)*M*gM/tau;
u(:,5)= u(:,5)*M*gM/tau;
end
% C([1 5  9]) = C([1 5  9])*M*gM*h;
% C([2 6 10]) = C([2 6 10])*(M*gM)^2/tau;
C= C;

if exist('p','var')==0
    p=nan;
end



end




   
   
   
   
   
   
   