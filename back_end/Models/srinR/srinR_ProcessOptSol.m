function [OPvars]= srinR_ProcessOptSol(OptSol, Model)
%====================================================================Inputs
%  OptSol  --> optimal solution
%  Model   --> structure with input parameters
%===================================================================Outputs
%  OPvars --> structure with vars from OptSol
%==========================================================================
%  This function takes outputs from OptSol (states, controls, etc.) and 
%  compiles them into a more accesible structure OPvars



%% =============================================================Load OptSol
 fnID= OptSol.result.setup.auxdata.fnID; % File ID for saving and Opt desired
   ds= OptSol.result.setup.auxdata.stpL; %[    m] step length while running
   hb= OptSol.result.setup.auxdata.h__b; %[    m] step length while running
    M= OptSol.result.setup.auxdata.mcom; %[   kg] body mass
    g= OptSol.result.setup.auxdata.grav; %[m/s^2] gravitational acceleration
    f= OptSol.result.setup.auxdata.mfrq; %[   Hz] hopping frequency

   gM= sqrt(sum(g.^2));
  tau= sqrt(hb/gM); %[    s] time non-dimensionalization factor
    w= 2*pi*f;      %[rad/s] step frequency
    T= 1/f;         %[    s] time period of hopping cycle
 
 
    t =OptSol.result.interpsolution.phase.time;
    q =OptSol.result.interpsolution.phase.state;
    u =OptSol.result.interpsolution.phase.control;
    if isfield(OptSol.result.interpsolution,'parameter')==1
    p =OptSol.result.interpsolution.parameter;
    end
    Obj =OptSol.result.objective;




%---Transform variables from non-dimensional to dimensional, if appropriate
[t q u p CT]= srinR_NonDim2AbsDim(OptSol); pTO= length(t);
Tc= t(end); %[s] ground contact time
Tf= T-Tc;   %[s] flight time

DF= Tc/T;

cWrk= CT(1)/0.25 + CT(2)/1.20; %-work cost (srinR does not scale by eff since net work = 0)
cFRS= CT(3); %-force rate cost
cTot= cWrk+cFRS; %-total cost

alpha= atan2d(g(1),g(2)); %ground angle
S= tand(alpha); %ground slope
% ---Transform local body coordinates into global coordinates (if slope~=0)
q(:,[1 3])= RotateCoord(q(:,[1 3]),alpha); %-positions
q(:,[2 4])= RotateCoord(q(:,[2 4]),alpha); %-velocities
xf= p(1);   yf= 0;
pf= RotateCoord([xf yf],alpha); %-foot position
xf= pf(1);  yf= pf(2);


% ---------------------Extrapolate running solution to include flight phase
[t q u]= srinR_ExtrapolateFlightPhase(OptSol,t,q,u);
to= t(1);  %plot from t=to
tf=    T;  %plot from t=tf (typically either Tc or T)



mp=round(length(t)/2);
%----------states+controls
xb= q(:,1);   xbd= q(:,2);
yb= q(:,3);   ybd= q(:,4);
Fm= q(:,5);
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
Fmd= q(:,6);
end
ybr = range(yb);


%-leg length/velocity
Ll = sqrt( (yb-yf).^2 + (xb-xf).^2 );   %leg length
Lld= ((yb-yf).*ybd + (xb-xf).*xbd)./Ll; %leg velocity
Lt= nan; Ltd= nan; Fs= nan;%-no spring
Fd= nan; %-no damper
Lm=  Ll; Lmd= Lld; %-muscle = leg

%-leg angle
sTh= (yb-yf)./Ll; %sin(theta), where theta is the leg angle, CCW from axis: + -->
cTh= (xb-xf)./Ll; %cos(theta), where theta is the leg angle, CCW from axis: + --> 

Th    = atan2d(sTh,cTh);
Th_gnd= Th-alpha;
Thd= (ybd.*(xb-xf)-(yb-yf).*xbd) ./ ((yb-yf).^2 + (xb-xf).^2); %[rad/s] leg ang vel
%-------------------------------------------------------------Accelerations
% CqV= [1 (1/M) 1 (1/M) 1  1]; %coeff vector
xbdd= (cTh.*Fm - M*g(1))/M; %xbdd
ybdd= (sTh.*Fm - M*g(2))/M; %ybdd
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
Fmdd= u(:,1);  %Fmdd
else
Fmd = u(:,1);  %Fmd
Fmdd= nan;
end
pPm = u(:,2);  %pPm
qPm = u(:,3);  %qPm
if contains(fnID,'_cFmdd')
pFmdd= u(:,4); %pos 2nd muscle force rate
qFmdd= u(:,5); %neg 2nd muscle force rate
elseif contains(fnID,'_cAbsFmd')
pFmd=  u(:,4); %pos 1st muscle force rate
qFmd=  u(:,5); %neg 1st muscle force rate
end
%---------------------------------------------------------Leg accelerations
%implement quotient rule for d(Lld)/dt
N   = Lld.*Ll; %numerator
Nd  = ybd.^2 + (yb-yf).*ybdd + xbd.^2 + (xb-xf).*xbdd; %numerator derivative
D   = Ll;      %denomenator
Dd  = Lld;     %denomenator derivative
D2  = Ll.^2;   %denomenator squared
Lldd= (D.*Nd - N.*Dd)./D2; %leg acceleration
Llddd= nan; %-leg jerk not important for this model
Lmdd= Lldd; Lmddd= Llddd; %-muscle = leg
%------------------------------------------cartesian ground reaction forces
Rx= cTh.*Fm; %[N] hor
Ry= sTh.*Fm; %[N] vrt
%---------------------------------------------------------------------Power
Pm= Fm.*Lld; %[W] muscle power
Ps= nan;
Pd= nan;
%--------------------------------------------------------------------Energy
E_grv= M*gM*yb;                %gravitational potential energy
E_kin= 0.5*M*(xbd.^2+ybd.^2);  %body kinetic energy
%---------------------------------------------------------------Muscle Work
Wm_ac= cumtrapz(t,Pm); %accumulating muscle work
Wm   = Wm_ac(end);     %net muscle work
Wm_nd= Wm/(M*gM*hb);   %^^^non-dim
PmPos=Pm;   PmPos(find(PmPos<0))=0;   WmPos= trapz(t,PmPos); %-pos work
PmNeg=Pm;   PmNeg(find(PmNeg>0))=0;   WmNeg= trapz(t,PmNeg); %-neg work
WsPos= nan; WsNeg= nan;   WdPos= nan; WdNeg= nan;
%------------------------------------------------------Absolute Muscle Work
absWm_ac= cumtrapz(t,abs(Pm)); %accumulating absolute value of muscle work
absWm   = absWm_ac(end);       %total accumulated absolute value of muscle work
absWm_nd= absWm/(M*gM*hb);     %^^^ non-dim
%------------------------------------------------------------Energy balance
E_sys= E_grv + E_kin - Wm_ac; %system energy (should be constant)


%--------------------------------------------------------------------------
%check outputs to make sure everything looks kosher
%--------------------------------------------------------------------------
Lld_ch= (Ll(2:end)-Ll(1:end-1)) ./ (t(2:end)-t(1:end-1));
Lld_ch= [Lld_ch(1); Lld_ch];

Pm_ch= Fm.*Lld_ch;
Accum_ch= cumtrapz(t,Pm_ch);
Wm_ch= Accum_ch(end);

Accum_ch_abs= cumtrapz(t,abs(Pm_ch));
Wm_ch_abs= Accum_ch_abs(end);
Wm_ch_nd_abs= Wm_ch_abs/(M*gM*hb);



%% ==================================Define output vars in structure OPvars
%-time
OPvars.time.time= t;
OPvars.time.events= [1 pTO length(t); 0 t(pTO) t(end)]; %-heelstrike, takeoff, next heelstrike
OPvars.time.dutyFactor=  DF; %-duty factor
OPvars.time.contactTime= t(pTO); %-ground contact time
OPvars.time.flightTime=  t(end)-t(pTO); %-flight time


%-kinematics
OPvars.kinematics.pos.legAngle=  Th; %-leg angle, CCW from +--> (global)
OPvars.kinematics.pos.legLength= Ll; %-leg length
if strcmpi(OptSol.name,'PMR')
OPvars.kinematics.pos.springLength=   Lt;
OPvars.kinematics.pos.damperLength=   Lt;
OPvars.kinematics.pos.actuatorLength= Lm;
end
OPvars.kinematics.pos.body= [xb  yb]; %-body kinematics
if strcmpi(OptSol.name,'PMR')
OPvars.kinematics.pos.actuatorEnd= [xm  ym]; %-actuator end away from ground
end
OPvars.kinematics.pos.foot= [xf  yf ]; %-foot
OPvars.kinematics.vel.legAngle=  Thd;  %-leg angular velocity, CCW +
OPvars.kinematics.vel.legLength= Lld;  %-leg velocity
OPvars.kinematics.vel.body= [xbd ybd]; %-body
if strcmpi(OptSol.name,'PMR')
OPvars.kinematics.vel.springLength=   Ltd;
OPvars.kinematics.vel.damperLength=   Ltd;
OPvars.kinematics.vel.actuatorLength= Lmd;
end
OPvars.kinematics.acc.leglength=  Lldd;
OPvars.kinematics.acc.body= [xbdd ybdd];
if strcmpi(OptSol.name,'PMR'); OPvars.kinematics.acc.actuatorLength=  Lmdd; end
if ~isnan(Llddd);              OPvars.kinematics.jrk.legLength=      Llddd; end
if strcmpi(OptSol.name,'PMR'); OPvars.kinematics.jrk.actuatorLength= Lmddd; end


%-kinetics
OPvars.kinetics.groundRxnForce= [Rx  Ry]; %-GRF
OPvars.kinetics.actuatorForce=        Fm; %-Actuator force
OPvars.kinetics.actuatorForce_ddt1=   Fmd;
if ~isnan(Fmdd); OPvars.kinetics.actuatorForce_ddt2= Fmdd; end
if strcmpi(OptSol.name,'PMR')
OPvars.kinetics.springForce= Fs; %-Spring force
OPvars.kinetics.damperForce= Fd; %-Damper force
end


%-energetics
OPvars.energetics.work.actuator= [WmNeg WmPos]; %-actuator work (neg, pos)
if strcmpi(OptSol.name,'PMR')
OPvars.energetics.work.spring=   [WsNeg WsPos]; %-spring...
OPvars.energetics.work.damper=   [WdNeg WdPos]; %-damper...
end
OPvars.energetics.power.actuator= Pm; %-actuator power
if strcmpi(OptSol.name,'PMR')
OPvars.energetics.power.spring=   Ps; %-spring...
OPvars.energetics.power.damper=   Pd; %-damper...
end


%-cost
OPvars.cost.total=     cTot;
OPvars.cost.work=      cWrk;
OPvars.cost.forceRate= cFRS;


%-units
OPvars.units.time=       [{'s'}]; %-units
OPvars.units.events=     [{'pt'} {'s'}];
OPvars.units.kinematics= [{'m'} {'m/s'} {'m/s2'} {'m/s3'} {'deg'} {'deg/s'} {'deg/s2'}];
OPvars.units.kinetics=   [{'N'} {'N/s'} {'N/s2'}];
OPvars.units.energetics= [{'J'} {'W'}];
OPvars.units.cost=       [{'non-dimensional'}];


%-order fields
OPvars= orderfields(OPvars);
OPvars.units= orderfields(OPvars.units);
OPvars.name= OptSol.name;
OPvars= orderfields(OPvars,[length(fieldnames(OPvars)) 1:length(fieldnames(OPvars))-1]); %-place name at top
%-include input parameters for references
OPvars.Model= Model;




end %-end function