function fnID= srinR_IDs(Model, ProbSet)
%====================================================================Inputs
%  Model --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%  fnID  --> model identifier string
%==========================================================================
%  This function compiles a model identifier string with information about
%  the desired optimization for srinR model. Equivalent to PMR where k=inf
%  or c=inf. Based on Srinivasan & Ruina, 2006 (Nature)  but with force-rate cost.
%  Also based on Rebula & Kuo, 2015 (PLoS one).



iSP= ProbSet.iSP;

Model_cell= struct2cell(Model); %-convert Model struct to cell array
fnames= fieldnames(Model); %-field names in Model struct
iAP= [cellfun(@isnumeric,Model_cell).*cellfun('length',Model_cell)];
iAP(find(iAP>1))= iSP;


fnID='OptSol_';
fnID= [fnID Model.Name];

%-Scale work?
if strcmpi(Model.ScaleWork,'yes')
fnID= [fnID '']; %-all solutions have scaled work
end

%-Ground slope
i= strcmp(fnames,'GroundSlope'); S= Model_cell{i}(iAP(i));
if round(S,2)~=0
ID= num2str(round(S,2),'%1.2f'); ID(find(ID=='.'))='p';
if S<=0
fnID= [fnID '_Slope'  ID];
elseif S>0
fnID= [fnID '_Slope+' ID];
end
end

%-Collision?
i= strcmp(fnames,'CollisionFraction');
% CF= Model_cell{i}(iAP(i));
CF= 0; %-model doesn't currently have functionality for collisions
if round(CF,2)>0
ID= num2str(round(CF,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID '_wCol' ID];
end

%-Force-rate cost?
i= strcmp(fnames,'ForceRateCoef'); cR= Model_cell{i}(iAP(i));
if cR==0
fnID= [fnID '']; %-all solutions have force-rate cost (otherwise, Fm=inf)
elseif cR>0
ID= [num2str(round(cR/(10^(floor(log10(cR)))),1),'%1.1f'),...
     'e' num2str(floor(log10(cR)),'%1.0f')]; ID(find(ID=='.'))='p';
fnID= [fnID '_eps' ID];
end

%-Damping?
i= strcmp(fnames,'DampingRatio');
% DR= Model_cell{i}(iAP(i));
DR= 0; %-tendon is rigid, by definition of the model
if round(DR,2)==0
fnID= [fnID '']; %-all solutions no damping since tendon is rigid
elseif round(DR,2)>0
ID= num2str(round(DR,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID '_slackFd_DmpRat' ID];
end

%-Spring constant
i= strcmp(fnames,'SpringConstant');
% k= Model_cell{i}(iAP(i));
fnID= [fnID '']; %-all solutions have rigid tendon

%-Step length & step frequency
if isnumeric(Model.StepLength)==1
i= strcmp(fnames,'StepLength'); ds= Model_cell{i}(iAP(i));
ID= num2str(round(ds,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID '_ds' ID];
i= strcmp(fnames,'Speed');      Sp= Model_cell{i}(iAP(i));
fs= Sp./ds;
ID= num2str(round(fs,2),'%1.2f');  ID(find(ID=='.'))='p';
fnID= [fnID '_f' ID];
elseif ischar(Model.StepLength)==1 & strcmpi(Model.StepLength,'G06')==1
i= strcmp(fnames,'Speed'); Sp= Model_cell{i}(iAP(i));
fs= 0.2592*Sp+2.1651; %-relationship from (Gutman et al, 2006, JEB)
ds= Sp./fs;
ID= num2str(round(ds,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID '_Vopt' '_ds' ID];
ID= num2str(round(fs,2),'%1.2f');  ID(find(ID=='.'))='p';
fnID= [fnID '_f' ID];
end

%-Gravity level
i= strcmp(fnames,'RelativeGravity'); gC= Model_cell{i}(iAP(i));
if gC~=1
ID= num2str(round(gC,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID,'_gRel' ID]; 
end














end