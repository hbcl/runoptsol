function [t q u]= srinR_ExtrapolateFlightPhase(OptSol,t,q,u)
%====================================================================Inputs
%  OptSol --> optimal solution
%===================================================================Outputs
%  t --> time    var
%  q --> state   vars
%  u --> control vars
%==========================================================================
%  This function extrapolates vars for subsequent flight phase in running
%  models such as "PMR"

    
    
%-params
 fnID= OptSol.result.setup.auxdata.fnID; %       file ID
   hb= OptSol.result.setup.auxdata.h__b; %[   m] standing height
    M= OptSol.result.setup.auxdata.mcom; %[  kg] body mass
    g= OptSol.result.setup.auxdata.grav; %[m/s2] gravitational acceleration
    f= OptSol.result.setup.auxdata.mfrq; %[  Hz] hopping frequency
   gM= sqrt(sum(g.^2));
    T= 1/f;    %[s] hopping        time period
   Tc= t(end); %[s] ground contact time period
   Tf= T-Tc;   %[s] flight         time period (i.e. no ground contact)
  
%-time+states+controls
   xb= q(:,1);   xbd= q(:,2);
   yb= q(:,3);   ybd= q(:,4);
   Fm= q(:,5);
   if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
   Fmd= q(:,6);
   end

   dt= 0.001; %[s] time resolution
    N= floor(Tf/dt);   No= length(t);
    t= [t; t(end)+dt*(1:N)'];
   for i=(1:N)+No
      xb(i)= xb(i-1)+xbd(i-1)*dt;
     xbd(i)= xbd(i-1);
      yb(i)= yb(i-1) + ybd(i-1)*dt - 0.5*gM*dt^2;
     ybd(i)= ybd(i-1) - gM*dt;
      Fm(i)= 0;
   if contains(fnID,'_cFmdd') || contains(fnID,'_cAbsFmd')
     Fmd(i)= 0;
      pD(i)= 0;
      qD(i)= 0;
     u(i,:)= [0 0 0 0 0];
   else
     u(i,:)= [0 0 0];
   end
   end
   
   if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
   q= [xb xbd yb ybd Fm Fmd];
   else
   q= [xb xbd yb ybd Fm];
   end
    
    





end %-end function


















