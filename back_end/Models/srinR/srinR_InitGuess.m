function auxdata= srinR_InitGuess(ProbSet, auxdata)
%====================================================================Inputs
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> structure with problem parameters
%===================================================================Outputs
%  auxdata --> input auxdata updated with random initial guesses
%==========================================================================
%  This function randomly assigns initial guesses for model srinR



fnID= ProbSet.fnID;
hb  = auxdata.h__b;
g   = auxdata.grav;
gM  = sqrt(sum(g.^2));
tau = sqrt(hb/gM); %[s] time non-dimensionalization factor


Rnd= rand;

gM= sqrt(sum(g.^2));
tau= sqrt(hb/gM); %[s] time non-dimensionalization factor
auxdata.guess.phase.time     = [0; Rnd];
auxdata.guess.phase.state    = rand(2,5);
auxdata.guess.phase.control  = rand(2,3);
auxdata.guess.phase.integral = [rand(1,3) 0];

if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
auxdata.guess.phase.state    = [auxdata.guess.phase.state rand(2,1)];
end
if contains(fnID,'_cFmdd') || contains(fnID,'_cAbsFmd')
auxdata.guess.phase.control  =  [auxdata.guess.phase.control   rand(2,2)];
auxdata.guess.phase.integral =  [auxdata.guess.phase.integral zeros(1,1)];
end
auxdata.guess.parameter= [rand];












end