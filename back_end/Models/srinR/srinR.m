function output= srinR(auxdata)
%-------------------------------------------------------------------------%
%-----------------------------------------------Point Mass Runner (srinR)-%
%-This models simple running locomotion with a point mass body and a------%
%-massless leg made up of a lengthening muscle. The point of this model---%
%-to demonstrate a Srinivasa/Ruina (Nature, 2006) type model for running--%
%-but with force rate cost to make forces less impulsive, more realistic.-%
%-Outputs of this model will be compared with srinR (muscle in series w/ -%
%-spring-damper mechanism). This model is equivalent to PMR where k=inf---%
%-or c=inf.---------------------------------------------------------------%
%-------------------------------------------------------------------------%

%% ===============================================Define Problem Parameters
%-------------------------------------------------------------------------%
% Load Auxdata------------------------------------------------------------%
%-------------------------------------------------------------------------%
 fnID= auxdata.fnID;    % File ID for saving and Opt desired
%   Fmm= auxdata.maxF;    %[    N] max muscle force
   hb= auxdata.h__b;    %[    m] max leg length during grnd contact
   ds= auxdata.stpL;    %[    m] step length
    M= auxdata.mcom;    %[   kg] body mass
    g= auxdata.grav;    %[m/s^2] gravitational acceleration
    f= auxdata.mfrq;    %[   Hz] hopping frequency
   DF= auxdata.DFac;    %[    #] duty factor


 MaxMeshi= auxdata.MaxMeshi;  % mesh max iterations
  MeshTol= auxdata.MeshTol;   % mesh tolerance
MaxSNOPTi= auxdata.MaxSNOPTi; % SNOPT max iterations
 SNOPTtol= auxdata.SNOPTtol;  % SNOPT optimal solution tolerance
 
% ------------------------------------------------Non-dimensionalize params
gM = sqrt(sum(g.^2));
tau= sqrt(hb/gM); %[s] time non-dimensionalization factor

w= 2*pi*f;   w= w*tau; %step frequency
T= 1/f;      T= T/tau; %hopping time period

%convert abs --> dim
ds= ds/hb;


%-------------------------------------------------------------------------%
% Setup for Problem Bounds------------------------------------------------%
%-------------------------------------------------------------------------%

%---------------------------------------------------------------Time Bounds
bounds.phase.initialtime.lower=    0;
bounds.phase.initialtime.upper=    0;
if contains(fnID,'_DF')
bounds.phase.finaltime.lower=   DF*T;
bounds.phase.finaltime.upper=   DF*T;
else
bounds.phase.finaltime.lower=   1e-6;
bounds.phase.finaltime.upper=    inf;
end
%--------------------------------------------------------------State Bounds
%bounds.phase.states.variables  = [ xb   xbd   yb   ybd    Fm ]
bounds.phase.initialstate.lower = [  0     0    0  -inf     0 ];
bounds.phase.initialstate.upper = [  0   inf    1     0     0 ];
bounds.phase.state.lower        = [  0  -inf    0  -inf     0 ];
bounds.phase.state.upper        = [ ds   inf    1   inf   inf ];
bounds.phase.finalstate.lower   = [  0     0    0     0     0 ];
bounds.phase.finalstate.upper   = [ ds   inf    1   inf     0 ];
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
bounds.phase.initialstate.lower = [bounds.phase.initialstate.lower -inf]; %[... Fmdd]
bounds.phase.initialstate.upper = [bounds.phase.initialstate.upper  inf];
bounds.phase.state.lower        = [bounds.phase.state.lower        -inf];
bounds.phase.state.upper        = [bounds.phase.state.upper         inf];
bounds.phase.finalstate.lower   = [bounds.phase.finalstate.lower   -inf];
bounds.phase.finalstate.upper   = [bounds.phase.finalstate.upper    inf];
end
%------------------------------------------------------------Control Bounds
%bounds.phase.control.vars= [ Fmdd  pPm  qPm ]
bounds.phase.control.lower= [ -inf    0    0 ];
bounds.phase.control.upper= [  inf  inf  inf ];
if contains(fnID,'_cFmdd') || contains(fnID,'_cAbsFmd')
bounds.phase.control.lower= [bounds.phase.control.lower    0    0]; %[... pFmd qFmd] or [.. pFmdd qFmdd]
bounds.phase.control.upper= [bounds.phase.control.upper  inf  inf];
end
%-----------------------------------------------------------Integral Bounds
%bounds.phase.integral.vars= [ W+   W-  FRS  pPm*qPm]
bounds.phase.integral.lower= [  0    0    0        0];
bounds.phase.integral.upper= [inf  inf  inf      inf];
if contains(fnID,'_cFmdd') || contains(fnID,'_cAbsFmd')
bounds.phase.integral.lower= [bounds.phase.integral.lower    0 ]; %[... pFmd*qFmd] or [... pFmdd*qFmdd]
bounds.phase.integral.upper= [bounds.phase.integral.upper  inf ];
end
%----------------------------------------------------------Parameter Bounds
bounds.parameter.lower=  0; %xf - foot position
bounds.parameter.upper= ds;
%-----------------------------------------------------------EndPoint Bounds
I= 4; %-No. of endpoint constraints
for i=1:I
bounds.eventgroup(i).lower= 0; %e.g. cyclical Solutions, etc.
bounds.eventgroup(i).upper= 0;
end
%---------------------------------------------------------------Path Bounds
bounds.phase.path.lower= [ 0  0 ]; %[Pm-pPm+qPm  Ll]
bounds.phase.path.upper= [ 0  1 ];
if contains(fnID,'_cFmdd') || contains(fnID,'_cAbsFmd')
bounds.phase.path.lower= [bounds.phase.path.lower  0  ]; %[... Fmd-pFmd+qFmd] or [... Fmdd-pFmdd+qFmdd]
bounds.phase.path.upper= [bounds.phase.path.upper  0  ];
end

%-------------------------------------------------------------------------%
% Provide Guess of Solution-----------------------------------------------%
%-------------------------------------------------------------------------%
guess= auxdata.guess;


%-------------------------------------------------------------------------%
% Provide Mesh Refinement Method and Initial Mesh-------------------------%
%-------------------------------------------------------------------------%
% mesh.method        = 'hp-LiuRao-Legendre';
mesh.method        = 'hp-DarbyRao';
mesh.tolerance     =  MeshTol;
mesh.maxiterations = MaxMeshi;
mesh.colpointsmin  =  6;
mesh.colpointsmax  = 20;


%-------------------------------------------------------------------------%
% Assemble Info into Problem Structure------------------------------------%
%-------------------------------------------------------------------------%
setup.name                 = 'srinR';
setup.functions.continuous = @srinR_cont;
setup.functions.endpoint   = @srinR_endp;
setup.displaylevel         = 0;
setup.bounds               = bounds;
setup.guess                = guess;
setup.mesh                 = mesh;
setup.nlp.solver           = 'snopt';
setup.auxdata              = auxdata;

setup.nlp.snoptoptions.tolerance     =  SNOPTtol;
setup.nlp.snoptoptions.maxiterations = MaxSNOPTi;
% setup.nlp.SNOPToptions.linear_solver = 'ma57';

% setup.derivatives.supplier           = 'adigator';
setup.derivatives.derivativelevel    = 'first';
setup.method                         = 'RPM-Differentiation';

%-------------------------------------------------------------------------%
% Solve Problem Using GPOPS2----------------------------------------------%
%-------------------------------------------------------------------------%
tic
output= gpops2(setup);
toc
end

%-------------------------------------------------------------------------%
% BEGIN: cont.m-----------------------------------------------------------%
%-------------------------------------------------------------------------%
function phaseout= srinR_cont(input)
%--------------------------------------------------------------Load Auxdata
 fnID= input.auxdata.fnID; %        File ID for saving and Opt desired
%   Fmm= input.auxdata.maxF; %[    N] max muscle force
   hb= input.auxdata.h__b; %[    m] max CoM height during grnd contact
   ds= input.auxdata.stpL; %[    m] step length
    M= input.auxdata.mcom; %[   kg] body mass
    g= input.auxdata.grav; %[m/s^2] gravitational acceleration [gx gy]
    f= input.auxdata.mfrq; %[   Hz] hopping frequency
 epsC= input.auxdata.epsC; %        cost coefficiencts
 eps1= epsC(1);
 eps2= epsC(2);
 eps3= epsC(3);

% ------------------------------------------------Non-dimensionalize params
gM =sqrt(sum(g.^2));
tau= sqrt(hb/gM); %[s] time non-dimensionalization factor

w= 2*pi*f;   w= w*tau; %step frequency
T= 1/f;      T= T/tau; %hopping time period

ds= ds/hb;


iP=1;
P= input.phase.parameter;
xf= P(1,iP);


%--------------------------------------------------------Define Opt Vectors
t= input.phase.time;    %time
q= input.phase.state;   %states
u= input.phase.control; %controls

%-states
xb = q(:,1); %hor body pos
xbd= q(:,2); %hor body vel
yb = q(:,3); %vrt body pos
ybd= q(:,4); %vrt body vel
Fm = q(:,5); %vrt muscle force
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
Fmd= q(:,6); %vrt muscle force rate
end


%-controls
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
Fmdd = u(:,1); %muscle force rate (2nd)
else
Fmd  = u(:,1); %muscle force rate (1st)
end
pPm  = u(:,2); %pos muscle power
qPm  = u(:,3); %neg muscle power
if contains(fnID,'_cFmdd')
pFmdd= u(:,4); %pos 2nd force rate
qFmdd= u(:,5); %neg 2nd force rate
end
if contains(fnID,'_cAbsFmd')
pFmd= u(:,4); %pos 1st force rate
qFmd= u(:,5); %neg 1st force rate
end


%-leg length/velocity
Ll = sqrt( (yb).^2 + (xb-xf).^2 ); %leg length
Lld= (yb.*ybd + (xb-xf).*xbd)./Ll; %leg velocity

%-dynamics
cTh= (xb-xf)./Ll;
sTh= (yb   )./Ll;
CqV= [1 (gM*tau^2/hb) 1 (gM*tau^2/hb) 1 1 ]; %coeff vector
ii=1;
qdot(:,ii)= CqV(1)*(xbd);               ii=ii+1;%xbd
qdot(:,ii)= CqV(2)*(cTh.*Fm - g(1)/gM); ii=ii+1;%xbdd
qdot(:,ii)= CqV(3)*(ybd);               ii=ii+1;%ybd
qdot(:,ii)= CqV(4)*(sTh.*Fm - g(2)/gM); ii=ii+1;%ybdd
qdot(:,ii)= CqV(5)*(Fmd);               ii=ii+1;%Fmd
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
qdot(:,ii)= CqV(6)*(Fmdd);              ii=ii+1;%Fmdd
end
phaseout.dynamics= qdot;


xbdd= qdot(:,2);   ybdd= qdot(:,4);
%-Leg accelerations
%-implement quotient rule for d(Lld)/dt
N   = Lld.*Ll; %numerator
Nd  = ybd.^2 + yb.*ybdd + xbd.^2 + (xb-xf).*xbdd; %numerator derivative
D   = Ll;      %denomenator
Dd  = Lld;     %denomenator derivative
D2  = Ll.^2;   %denomenator squared
Lldd= (D.*Nd - N.*Lld)./(Ll.^2); %leg acceleration


Pm= Fm.*Lld; %muscle power

%-path constraints
phaseout.path= [Pm-pPm+qPm  Ll ]; %path constraints
if contains(fnID,'_cFmdd')
phaseout.path= [phaseout.path  Fmdd-pFmdd+qFmdd ]; %path constraints
end
if contains(fnID,'_cAbsFmd')
phaseout.path= [phaseout.path  Fmd-pFmd+qFmd    ]; %path constraints
end



if length(strfind(fnID,'_DiffWorkEff'))==0
Cp= 1;
Cq= 1;
elseif length(strfind(fnID,'_DiffWorkEff'))==1
Cp= 1/0.25;
Cq= 1/1.20;
end
Ca= 0.5*(Cp+Cq);
if length(strfind(fnID,'_ZeroNegWrkCost'))==1
Cq=  0;
Ca= Cp;
end

if contains(fnID,'_cFmdd')
   cForceRate= pFmdd+qFmdd;
elseif contains(fnID,'_cFmddSq')
   cForceRate= Fmdd.^2;
elseif contains(fnID,'_cAbsFmd')
   cForceRate= pFmd+qFmd;
else
   cForceRate= Fmd.^2;
end

%-cost function
phaseout.integrand= [Cp*pPm  Cq*qPm  Ca*eps2*cForceRate  Ca*eps1*pPm.*qPm]; %Cost Term Integrands
if contains(fnID,'_cFmdd')
phaseout.integrand= [phaseout.integrand  Ca*eps3*pFmdd.*qFmdd];
end
if contains(fnID,'_cAbsFmd')
phaseout.integrand= [phaseout.integrand  Ca*eps3*pFmd.*qFmd];
end
end
%-------------------------------------------------------------------------%
% END: cont.m-------------------------------------------------------------%
%-------------------------------------------------------------------------%


%-------------------------------------------------------------------------%
% BEGIN: endp.m-----------------------------------------------------------%
%-------------------------------------------------------------------------%
function output= srinR_endp(input)
%--------------------------------------------------------------Load Auxdata

%-input parameters
fnID= input.auxdata.fnID; % File ID
hb  = input.auxdata.h__b; %[    m] max CoM height during grnd contact
ds  = input.auxdata.stpL; %[    m] step length
g   = input.auxdata.grav; %[m/s^2] gravitational acceleration
f   = input.auxdata.mfrq; %[   Hz] hopping frequency
DF  = input.auxdata.DFac; %[    #] duty factor

%-non-dimensionalize input parameters
gM= sqrt(sum(g.^2));
tau= sqrt(hb/gM);   %[s] time non-dimensionalization factor
ds = ds/hb;
gM = gM*tau^2/hb;

%-------------------initial states
xbi = input.phase.initialstate(1);
xbdi= input.phase.initialstate(2);
ybi = input.phase.initialstate(3);
ybdi= input.phase.initialstate(4);
Fmi = input.phase.initialstate(5);
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
Fmdi= input.phase.initialstate(6);
end
%---------------------final states
xbf = input.phase.finalstate(1);
xbdf= input.phase.finalstate(2);
ybf = input.phase.finalstate(3);
ybdf= input.phase.finalstate(4);
Fmf = input.phase.finalstate(5);
if contains(fnID,'_cFmdd') || contains(fnID,'_cFmddSq')
Fmdf= input.phase.finalstate(6);
end
%-------------initial/final time
to= input.phase.initialtime;
tf= input.phase.finaltime;


%-parameters
iP=1;
P= input.parameter;
xf= P(1,iP);

%-leg length/velocity
Lli = sqrt( (ybi).^2 + (xbi-xf).^2 );    %initial leg length
Llf = sqrt( (ybf).^2 + (xbf-xf).^2 );    %final   leg length
Lldi= (ybi.*ybdi + (xbi-xf).*xbdi)./Lli; %initial leg velocity
Lldf= (ybf.*ybdf + (xbf-xf).*xbdf)./Llf; %final   leg velocity


%-Time periods
T = 1/f;           %[   s] step time period
T = T/tau;         %[nonD] step time period
if contains(fnID,'_DF')
Tc= DF*T;          %[   #] duty factor
Tf= T-Tc;          %[nonD] flight time period
else
Tc= tf-to;         %[nonD] time of ground phase
Tf=  T-Tc;         %[nonD] flight time period
end


alpha= atan2d(g(1),g(2)); %ground angle
%-use rotation matrix to transform local body coordinate into global
Rmat= [cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)];
%-transform initial positions
pgi= Rmat*[xbi; ybi];
xgi= pgi(1); ygi= pgi(2);
%-transform final positions
pgf= Rmat*[xbf; ybf];
xgf= pgf(1); ygf= pgf(2);
%-transform initial velocities
vgi = Rmat*[xbdi; ybdi];
xgdi= vgi(1); ygdi= vgi(2);
%-transform final velocities
vgf = Rmat*[xbdf; ybdf];
xgdf= vgf(1); ygdf= vgf(2);


dPE= gM*(ygf-ygi); %change in potential energy during flight
dKE= 0.5*(ygdf^2 + xgdf^2 - ygdi^2 - xgdi^2); %change in kinetic energy during flight
dxg= xgf-xgi;

% -----------------------------------------------------Endpoint Constraints
ii=1;
output.eventgroup(ii).event= dxg+xgdf*Tf-ds*cosd(alpha); ii=ii+1; %enforce step length
output.eventgroup(ii).event= xgdf-xgdi;                  ii=ii+1; %periodic gait
output.eventgroup(ii).event= ygdi-ygdf+gM*Tf;            ii=ii+1; %periodic gait
output.eventgroup(ii).event= dPE+dKE-ds*sind(alpha);     ii=ii+1; %periodic gait

q= input.phase.integral;
output.objective= sum(q);
end
%-------------------------------------------------------------------------%
% END: endp.m-------------------------------------------------------------%
%-------------------------------------------------------------------------%













