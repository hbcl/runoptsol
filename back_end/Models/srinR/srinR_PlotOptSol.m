function srinR_PlotOptSol(OPvars, Model, ProbSet, OptSol, View)
%====================================================================Inputs
%  OPvars  --> basic vars from OptSol
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  OptSol  --> optimal solution
%  View    --> structure indicating what plots to show
%==========================================================================
%  This function plots summary of vars in OPvars


if isfield(View,'Preset'); if ~isempty(View.Preset); return; end; end %-bypass if preset


iSP= ProbSet.iSP;
%% ===========================================================Define Params
Model_cell= struct2cell(Model); %-convert Model struct to cell array
fnames= fieldnames(Model);      %-field names in Model struct
iAP= [cellfun(@isnumeric,Model_cell).*cellfun('length',Model_cell)];
iAP(find(iAP>1))= iSP;

i= strcmp(fnames,'Speed');             Sp= Model_cell{i}(iAP(i)); %-speed
i= strcmp(fnames,'BodyMass');          M = Model_cell{i}(iAP(i)); %-body mass
% i= strcmp(fnames,'DutyFactor');      DF= Model_cell{i}(iAP(i)); %-duty factor
DF= nan;
i= strcmp(fnames,'GroundSlope');       S = Model_cell{i}(iAP(i)); %-ground slope
i= strcmp(fnames,'DampingRatio');      DR= Model_cell{i}(iAP(i)); %-damping ratio
i= strcmp(fnames,'MaxLegLength');      hb= Model_cell{i}(iAP(i)); %-max allowable leg length
i= strcmp(fnames,'ForceRateCoef');     cR= Model_cell{i}(iAP(i)); %-force rate coefficient
i= strcmp(fnames,'SpringConstant');    k = Model_cell{i}(iAP(i)); %-leg spring constant
i= strcmp(fnames,'RelativeGravity');   gC= Model_cell{i}(iAP(i)); %-relative gravity
i= strcmp(fnames,'CollisionFraction'); CF= Model_cell{i}(iAP(i)); %-collision fraction


i= strcmp(fnames,'StepLength');
if ischar(Model_cell{i})
if strcmpi(Model_cell{i},'G06')
fs= 0.2592*Sp+2.1651; %[Hz] step frequency: relationship based on Gutman et al., 2006
ds= Sp./fs; %[m] step length
else
error(['Unknown code for Model.StepLength: ' Model_cell{i}]);
end
elseif isnumeric(Model_cell{i})
ds= Model_cell{i}(iAP(i)); %[m] step length
fs= Sp./ds; %[Hz] step frequency
end
gM= 9.81; %[m/s2] Earth's gravitational acceleration

if isfield(OptSol.result.setup.auxdata,'h__m')
hm= OptSol.result.setup.auxdata.h__m;
else
hm= nan;
end
dh_mb= hb-hm;


%% ==================================Define output vars in structure OPvars

%-time
t  = OPvars.time.time; %-time
T  = t(end); %-step  time
to = t(1);   %-start time
tf = t(end); %-end   time
DF = OPvars.time.dutyFactor;  %-duty factor
Tc = OPvars.time.contactTime; %-ground contact time
Tf = OPvars.time.flightTime;  %-flight time
pTO= OPvars.time.events(1,2); %-point of take off

%-kinematics
Th= OPvars.kinematics.pos.legAngle;  %-leg angle, CCW from +--> (global)
Ll= OPvars.kinematics.pos.legLength; %-leg length
xb= OPvars.kinematics.pos.body(:,1); %-body hor pos
yb= OPvars.kinematics.pos.body(:,2); %-body vrt pos
xf= OPvars.kinematics.pos.foot(:,1);  %-foot hor pos
yf= OPvars.kinematics.pos.foot(:,2);  %-foot vrt pos

Thd= OPvars.kinematics.vel.legAngle;  %-leg angular velocity, CCW +
Lld= OPvars.kinematics.vel.legLength; %-leg velocity
xbd= OPvars.kinematics.vel.body(:,1); %-body hor pos
ybd= OPvars.kinematics.vel.body(:,2); %-body vrt pos

Lldd= OPvars.kinematics.acc.leglength; %-leg acc
xbdd= OPvars.kinematics.acc.body(:,1); %-hor body acc
ybdd= OPvars.kinematics.acc.body(:,2); %-vrt body acc
Lmdd= nan;
if isfield(OPvars.kinetics,'jrk'); Llddd= OPvars.kinematics.jrk.legLength;       else; Llddd=nan; end
Lmddd=nan;


%-kinetics
Rx=  OPvars.kinetics.groundRxnForce(:,1); %-hor GRF
Ry=  OPvars.kinetics.groundRxnForce(:,2); %-vrt GRF
Fm=  OPvars.kinetics.actuatorForce;  %-Actuator force
Fmd= OPvars.kinetics.actuatorForce_ddt1; %-d/dt actuator force
if isfield(OPvars.kinetics,'actuatorForce_ddt2'); Fmdd= OPvars.kinetics.actuatorForce_ddt2; else; Fmdd=nan; end %-d2/dt2 actuator force
if isfield(OPvars.kinetics,'springForce');        Fs  = OPvars.kinetics.springForce;        else; Fs  =nan; end %-Spring force
if isfield(OPvars.kinetics,'damperForce');        Fd  = OPvars.kinetics.springForce;        else; Fd  =nan; end %-Damper force


%-energetics
negMscWrk= OPvars.energetics.work.actuator(1); %-neg actuator work
posMscWrk= OPvars.energetics.work.actuator(2); %-pos actuator work
absMscWrk= posMscWrk-negMscWrk; %-abs actuator work
netMscWrk= posMscWrk+negMscWrk; %-net actuator work

Pm= OPvars.energetics.power.actuator; %-actuator power


%-cost
cTot= OPvars.cost.total;
cWrk= OPvars.cost.work;
cFRS= OPvars.cost.forceRate;



%% --------------------------------------------------------------Plot srinR
%--------------------------------------------------------------------Energy
E_grv= M*gM*yb;                %gravitational potential energy
E_kin= 0.5*M*(xbd.^2+ybd.^2);  %body kinetic energy
%---------------------------------------------------------------Muscle Work
Wm_ac= cumtrapz(t,Pm); %accumulating muscle work
Wm   = Wm_ac(end);     %net muscle work
Wm_nd= Wm/(M*gM*hb);   %^^^non-dim
PmPos=Pm;   PmPos(find(PmPos<0))=0;   WmPos= trapz(t,PmPos); %-pos work
PmNeg=Pm;   PmNeg(find(PmNeg>0))=0;   WmNeg= trapz(t,PmNeg); %-neg work
WsPos= nan; WsNeg= nan;   WdPos= nan; WdNeg= nan;
%------------------------------------------------------Absolute Muscle Work
absWm_ac= cumtrapz(t,abs(Pm)); %accumulating absolute value of muscle work
absWm   = absWm_ac(end);       %total accumulated absolute value of muscle work
absWm_nd= absWm/(M*gM*hb);     %^^^ non-dim
%------------------------------------------------------------Energy balance
E_sys= E_grv + E_kin - Wm_ac; %system energy (should be constant)


%--------------------------------------------------------------------------
%check outputs to make sure everything looks kosher
%--------------------------------------------------------------------------
Lld_ch= (Ll(2:end)-Ll(1:end-1)) ./ (t(2:end)-t(1:end-1));
Lld_ch= [Lld_ch(1); Lld_ch];

Pm_ch= Fm.*Lld_ch;
Accum_ch= cumtrapz(t,Pm_ch);
Wm_ch= Accum_ch(end);

Accum_ch_abs= cumtrapz(t,abs(Pm_ch));
Wm_ch_abs= Accum_ch_abs(end);
Wm_ch_nd_abs= Wm_ch_abs/(M*gM*hb);
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------


if View.PlotSummary==1
% =============================================================Plot Results
FigTxt= ['Summary Plot: SPmat(' num2str(iSP) ') = ' num2str(Model.SPmat(iSP),'%g')];
figure('Name',FigTxt,'NumberTitle','Off','Color','w',...
                      'units','normalized','Position',[0.05 0.08 0.5 0.8]);
LW=2;
%-----------------------------------------------------------------Positions
subplot(3,2,1);ih=1;
% title('Position');
plot([0 xb(end)],[0 S*xb(end)],'k','LineWidth',3);hold on;
h(ih)= plot(xb(  1:pTO),yb(  1:pTO),'b  ','LineWidth',LW);ih=ih+1; hold on;
h(ih)= plot(xb(pTO:end),yb(pTO:end),'b--','LineWidth',LW);ih=ih+1;
h(ih)= plot([xf xb(  1)],[yf yb(  1)],'k','LineWidth',LW);ih=ih+1;
h(ih)= plot([xf xb(pTO)],[yf yb(pTO)],'k','LineWidth',LW);ih=ih+1;
h(ih)= plot(xf,yf,'mx','LineWidth',LW);ih=ih+1;
xlim([0 ds]); %ylim([min(yb)-0.2 max(yb)+0.2]);
axis equal;
xlabel('Hor Pos (m)','FontSize',16,'FontWeight','Bold');
ylabel('Vrt Pos (m)','FontSize',16,'FontWeight','Bold');
% legend(h([1 3]),'y_b');
% set(gca,'FontSize',16,'FontWeight','Bold');
grid on;
%----------------------------------------------------------------Velocities
subplot(3,2,3);ih=1;
% title('Velocity');
h(ih)= plot(t(  1:pTO),xbd(  1:pTO),'r  ','LineWidth',LW);ih=ih+1; hold on;
h(ih)= plot(t(pTO:end),xbd(pTO:end),'r--','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(  1:pTO),ybd(  1:pTO),'b  ','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(pTO:end),ybd(pTO:end),'b--','LineWidth',LW);ih=ih+1;
xlim([to tf]);
xlabel( 'Time (s)','FontSize',16,'FontWeight','Bold');
ylabel('Vel (m/s)','FontSize',16,'FontWeight','Bold');
grid on;
%--------------------------------------------------------------------Forces
subplot(3,2,5);ih=1; grid on;
h(ih)= plot(t(  1:pTO),Rx(  1:pTO)/(M*gM),'b  ','LineWidth',LW+1);ih=ih+1; hold on;
h(ih)= plot(t(pTO:end),Rx(pTO:end)/(M*gM),'b--','LineWidth',LW+1);ih=ih+1;
h(ih)= plot(t(  1:pTO),Ry(  1:pTO)/(M*gM),'r  ','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(pTO:end),Ry(pTO:end)/(M*gM),'r--','LineWidth',LW);ih=ih+1;
legend(h([1:2:3]),'R_x','R_y');
xlim([to tf]);% ylim([0 5]);
ylabel('GRF (BW)','FontSize',16,'FontWeight','Bold');
xlabel('Time (s)','FontSize',16,'FontWeight','Bold');
grid on;
%------------------------------------------------------------Energy Balance
subplot(3,2,2);ih=1; grid on;
h(ih)= plot(t(  1:pTO), E_grv(  1:pTO),'b  ','LineWidth',LW);ih=ih+1; hold on;
h(ih)= plot(t(pTO:end), E_grv(pTO:end),'b--','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(  1:pTO), E_kin(  1:pTO),'r  ','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(pTO:end), E_kin(pTO:end),'r--','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(  1:pTO), Wm_ac(  1:pTO),'m  ','LineWidth',LW  );ih=ih+1;
h(ih)= plot(t(pTO:end), Wm_ac(pTO:end),'m--','LineWidth',LW  );ih=ih+1;
h(ih)= plot(t(  1:pTO), E_sys(  1:pTO),'k  ','LineWidth',LW+1);ih=ih+1;
h(ih)= plot(t(pTO:end), E_sys(pTO:end),'k--','LineWidth',LW+1);ih=ih+1;
legend(h([1:2:7]),'PE (grv)','KE','W (act)','PE+KE-W');
xlim([to tf]);% ylim([0 5]);
ylabel('Energy (J)','FontSize',16,'FontWeight','Bold');
title(['E_{sys}= ' num2str(E_sys(end),'%1.2f') ' J']);
grid on;
%--------------------------------------------------------------Muscle Power
subplot(3,2,4);ih=1; grid on;
h(ih)= plot(t(  1:pTO), Fm(  1:pTO)/max(Fm),   'k  ','LineWidth',LW);ih=ih+1; hold on;
h(ih)= plot(t(pTO:end), Fm(pTO:end)/max(Fm),   'k--','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(  1:pTO),Lld(  1:pTO)/range(Lld),'r  ','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(pTO:end),Lld(pTO:end)/range(Lld),'r--','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(  1:pTO), Pm(  1:pTO)/range(Pm), 'm  ','LineWidth',LW);ih=ih+1;
h(ih)= plot(t(pTO:end), Pm(pTO:end)/range(Pm), 'm--','LineWidth',LW);ih=ih+1;
legend(h([1:2:5]),'F_m','Ld_{l}','P_m');
xlim([to tf]);% ylim([0 5]);
ylabel('Muscle','FontSize',16,'FontWeight','Bold');
% title(['|W_m|= ' num2str((CT(1)+CT(2))*M*g*hb,'%1.2f') ' J']);
grid on;
%-----------------------------------------------------------------All Power
subplot(3,2,6);ih=1; grid on;
h(ih)= plot(t(  1:pTO),Pm(  1:pTO),'b  ','LineWidth',LW  );ih=ih+1; hold on;
h(ih)= plot(t(pTO:end),Pm(pTO:end),'b--','LineWidth',LW  );ih=ih+1;
% legend(h([1]),'P_m');
xlim([to tf]);% ylim([0 5]);
ylabel('Power (W)','FontSize',16,'FontWeight','Bold');
% title(['E_{sys}= ' num2str(E_sys(1),'%1.2f') ' J']);
grid on;


%--------------------------------------------------------------McMahon Plot
% figure('Color','w');
% plot((yb-max(yb(find(Fm>0.01*M*gM))))*100,ybdd/gM,'k','LineWidth',2);
% xlabel('Vrt. Dsp. (cm)');
% ylabel('Vrt. Acc. (/g)');
% title('McMahon Leg Stiffness Plot');
end

















end %-end function


