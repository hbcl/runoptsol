function [Model, ProbSet]= PMR_DefaultParams(Model, ProbSet)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%  Model   --> Model   updated with defaults
%  ProbSet --> ProbSet updated with defaults
%==========================================================================
%  This function checks for empty fields and fills in default values.




%-Model parameters
if isfield(Model,'ScaleWork');         if isempty(Model.ScaleWork);         Model.ScaleWork=       'yes';  end; end
if isfield(Model,'ForceRateCoef');     if isempty(Model.ForceRateCoef);     Model.ForceRateCoef=    5e-4;  end; end
if isfield(Model,'DampingRatio');      if isempty(Model.DampingRatio);      Model.DampingRatio=     0.10;  end; end
if isfield(Model,'SpringConstant');    if isempty(Model.SpringConstant);    Model.SpringConstant=  35582;  end; end
if isfield(Model,'CollisionFraction'); if isempty(Model.CollisionFraction); Model.CollisionFraction=0.03;  end; end
if isfield(Model,'GroundSlope');       if isempty(Model.GroundSlope);       Model.GroundSlope=      0.00;  end; end
if isfield(Model,'RelativeGravity');   if isempty(Model.RelativeGravity);   Model.RelativeGravity=  1.00;  end; end
if isfield(Model,'Speed');             if isempty(Model.Speed);             Model.Speed=            3.00;  end; end
if isfield(Model,'StepLength');        if isempty(Model.StepLength);        Model.StepLength=      'G06';  end; end
if isfield(Model,'BodyMass');          if isempty(Model.BodyMass);          Model.BodyMass=         70.0;  end; end
if isfield(Model,'MaxLegLength');      if isempty(Model.MaxLegLength);      Model.MaxLegLength=     0.90;  end; end
if isfield(Model,'SPmat');             if isempty(Model.SPmat);             Model.SPmat=     Model.Speed;  end; end
%-Model actuator constraints
if isfield(Model,'MinMax_Force');     if isempty(Model.MinMax_Force);      Model.MinMax_Force=     [-inf;inf]; end; end
if isfield(Model,'MinMax_ForceRate'); if isempty(Model.MinMax_ForceRate);  Model.MinMax_ForceRate= [-inf;inf]; end; end
if isfield(Model,'MinMax_MechPower'); if isempty(Model.MinMax_MechPower);  Model.MinMax_MechPower= [-inf;inf]; end; end
if isfield(Model,'MinMax_MetPower');  if isempty(Model.MinMax_MetPower);   Model.MinMax_MetPower=  [-inf;inf]; end; end
%-ProbSet
if isfield('ProbSet','var')==1
if isfield(ProbSet,'Save');                 if isempty(ProbSet.Save);                  ProbSet.Save=               'yes'; end; end
if isfield(ProbSet,'RG')
if isfield(ProbSet.RG,'MeshTolerance');     if isempty(ProbSet.RG.MeshTolerance);      ProbSet.RG.MeshTolerance=    1e-4; end; end
if isfield(ProbSet.RG,'SNOPTTolerance');    if isempty(ProbSet.RG.SNOPTTolerance);     ProbSet.RG.SNOPTTolerance=   1e-6; end; end
if isfield(ProbSet.RG,'StartIteration');    if isempty(ProbSet.RG.StartIteration);     ProbSet.RG.StartIteration=      1; end; end
if isfield(ProbSet.RG,'TotalIteration');    if isempty(ProbSet.RG.TotalIteration);     ProbSet.RG.TotalIteration=     15; end; end
if isfield(ProbSet.RG,'DownsampleRate');    if isempty(ProbSet.RG.DownsampleRate);     ProbSet.RG.DownsampleRate=   0.01; end; end
if isfield(ProbSet.RG,'MaxMeshIteration');  if isempty(ProbSet.RG.MaxMeshIteration);   ProbSet.RG.MaxMeshIteration=  [2]; end; end
if isfield(ProbSet.RG,'MaxSNOPTIteration'); if isempty(ProbSet.RG.MaxSNOPTIteration);  ProbSet.RG.MaxSNOPTIteration=4000; end; end
end
if isfield(ProbSet,'PP')
if isfield(ProbSet.PP,'MeshTolerance');     if isempty(ProbSet.PP.MeshTolerance);      ProbSet.PP.MeshTolerance=    1e-4; end; end
if isfield(ProbSet.PP,'SNOPTTolerance');    if isempty(ProbSet.PP.SNOPTTolerance);     ProbSet.PP.SNOPTTolerance=   1e-6; end; end
if isfield(ProbSet.PP,'StartIteration');    if isempty(ProbSet.PP.StartIteration);     ProbSet.PP.StartIteration=      1; end; end
if isfield(ProbSet.PP,'TotalIteration');    if isempty(ProbSet.PP.TotalIteration);     ProbSet.PP.TotalIteration=     15; end; end
if isfield(ProbSet.PP,'PeturbMagnitude');   if isempty(ProbSet.PP.PeturbMagnitude);    ProbSet.PP.PeturbMagnitude=   1/8; end; end
if isfield(ProbSet.PP,'DownsampleRate');    if isempty(ProbSet.PP.DownsampleRate);     ProbSet.PP.DownsampleRate=   0.01; end; end
if isfield(ProbSet.PP,'MaxMeshIteration');  if isempty(ProbSet.PP.MaxMeshIteration);   ProbSet.PP.MaxMeshIteration=  [2]; end; end
if isfield(ProbSet.PP,'MaxSNOPTIteration'); if isempty(ProbSet.PP.MaxSNOPTIteration);  ProbSet.PP.MaxSNOPTIteration=4000; end; end
end
end





end %-end function