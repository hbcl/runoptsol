function [t q u]= PMR_ExtrapolateFlightPhase(OptSol,t,q,u)
%====================================================================Inputs
%  OptSol --> optimal solution
%===================================================================Outputs
%  t --> time    var
%  q --> state   vars
%  u --> control vars
%==========================================================================
%  This function extrapolates vars for subsequent flight phase in running
%  models such as "PMR"




%-params
% dh_bf= OptSol.result.setup.auxdata.dhbf; %[   m] hop height above standing height
 fnID= OptSol.result.setup.auxdata.fnID; %       file ID
   hb= OptSol.result.setup.auxdata.h__b; %[   m] standing height
    M= OptSol.result.setup.auxdata.mcom; %[  kg] body mass
    g= OptSol.result.setup.auxdata.grav; %[m/s2] gravitational acceleration
    f= OptSol.result.setup.auxdata.mfrq; %[  Hz] hopping frequency
    T= 1/f;    %[s] hopping        time period
   Tc= t(end); %[s] ground contact time period
   Tf= T-Tc;   %[s] flight         time period (i.e. no ground contact)
  
   gM= sqrt(sum(g.^2));
%-time+states+controls
     ii= 1;
     xb= q(:,ii); ii=ii+1;
    xbd= q(:,ii); ii=ii+1;
     yb= q(:,ii); ii=ii+1;
    ybd= q(:,ii); ii=ii+1;
     Lm= q(:,ii); ii=ii+1;
    Lmd= q(:,ii); ii=ii+1;
   Lmdd= q(:,ii); ii=ii+1;

   if contains(fnID,'_slackFd')==1 && contains(fnID,'_NoDmp')==0
    pLtd= q(:,ii); ii=ii+1;
    qLtd= q(:,ii); ii=ii+1;
   if contains(fnID,'_cFmdd')
   pLtdd= q(:,ii); ii=ii+1;
   qLtdd= q(:,ii); ii=ii+1;
   end
   end


   dt= 0.001; %[s] time resolution
    N= floor(Tf/dt);   No= length(t);
    t= [t; t(end)+dt*(1:N)'];
   for i=(1:N)+No
      xb(i)= xb(i-1)+xbd(i-1)*dt;
     xbd(i)= xbd(i-1);
      yb(i)= yb(i-1) + ybd(i-1)*dt - 0.5*gM*dt^2;
     ybd(i)= ybd(i-1) - gM*dt;
%       Lm(i)= Lm(i-1) + yb(i) - yb(i-1);
%      Lmd(i)= ybd(i);
      Lm(i)= nan;
     Lmd(i)= nan;
    Lmdd(i)= nan;
   if contains(fnID,'_slackFd')==1 && contains(fnID,'_NoDmp')==0
      pLtd(i)= 0;
      qLtd(i)= 0;
     pLtdd(i)= 0;
     qLtdd(i)= 0;
   end
     addu= [nan 0 0];
     if contains(fnID,'_cFmdd')
     addu= [addu nan nan];
     end
     if contains(fnID,'_slackFd')==1 && contains(fnID,'_NoDmp')==0
     addu= [addu nan nan];
     end
     u(i,:)= addu;
   end
   

   q= [xb xbd yb ybd Lm Lmd Lmdd];

   if contains(fnID,'_slackFd')==1 && contains(fnID,'_NoDmp')==0
   q= [q pLtd  qLtd ];
   if contains(fnID,'_cFmdd')
   q= [q pLtdd qLtdd]; 
   end
   end







end %-end function


















