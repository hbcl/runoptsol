function [OPvars]= PMR_ProcessOptSol(OptSol, Model)
%====================================================================Inputs
%  OptSol  --> optimal solution
%  Model   --> structure with input parameters
%===================================================================Outputs
%  OPvars --> structure with vars from OptSol
%==========================================================================
%  This function takes outputs from OptSol (states, controls, etc.) and 
%  compiles them into a more accesible structure OPvars



%% ====================================================Define GPOPS Outputs
 fnID= OptSol.result.setup.auxdata.fnID; % File ID for saving and Opt desired
   hb= OptSol.result.setup.auxdata.h__b; %[    m] max leg    length when standing straight
   hm= OptSol.result.setup.auxdata.h__m; %[    m] max muscle length when standing straight
   ds= OptSol.result.setup.auxdata.stpL; %[    m] step length while running
    M= OptSol.result.setup.auxdata.mcom; %[   kg] body mass
    g= OptSol.result.setup.auxdata.grav; %[m/s^2] gravitational acceleration
    f= OptSol.result.setup.auxdata.mfrq; %[   Hz] hopping frequency
    k= OptSol.result.setup.auxdata.sprC; %[  N/m] leg spring constant
    c= OptSol.result.setup.auxdata.dmpC; %[ Ns/m] leg damping coefficient

    nPh= length(OptSol.result.solution.phase); %-number of phases
    Ph{1}= 1:length(OptSol.result.solution.phase(1).time); %-points from 1st phase
    for iPh=2:nPh
    Ph{iPh}= (1:length(OptSol.result.solution.phase(iPh).time))+length(Ph{iPh-1}); %-points from each successive phase
    end

gM   = sqrt(sum(g.^2));
dh_mb= hb-hm;       %[    m] max spring-damper length
tau  = sqrt(hb/gM); %[    s] time non-dimensionalization factor
    
w= 2*pi*f;     %[rad/s] step frequency
T= 1/f;        %[    s] time period of hopping cycle
 

pC2E= length(OptSol.result.solution.phase(1).time);


% --Transform variables from non-dimensional to dimensional
[t q u p CT]= PMR_NonDim2AbsDim(OptSol); pTO= length(t);


Tc= t(end); %[s] ground contact time
Tf= T-Tc;   %[s] flight time

cWrk= sum(CT([1 2])); %-work cost
cFRS= CT(3); %-force rate cost
cTot= cWrk+cFRS; %-total cost


alpha= atan2d(g(1),g(2)); %ground angle
S= tand(alpha); %ground slope
% ---Transform local body coordinates into global coordinates (if slope~=0)
q(:,[1 3])= RotateCoord(q(:,[1 3]),alpha); %-positions
q(:,[2 4])= RotateCoord(q(:,[2 4]),alpha); %-velocities
xf= p(1);   yf= 0;
pf= RotateCoord([xf yf],alpha); %-foot position
xf= pf(1);  yf= pf(2);


% --------------Extrapolate solution to include flight phase if appropriate
[t q u]= PMR_ExtrapolateFlightPhase(OptSol,t,q,u);
nPh= nPh+1; %-add flight phase
Ph{nPh}= sum(cellfun('length',Ph)):length(t); %-points from extrapolated flight phase

to= t(1);  %plot from t=to
tf=    T;  %plot from t=tf (typically either Tc or T)
DF= t(pTO)/T;



iP=2;
if length(k)==2
if k(1)~=k(2) && isnan(k(1))==0
    iP=iP+1;
    k= p(iP);
elseif k(1)==k(2)
    k= k(1);
end
end
if length(c)==2
if c(1)~=c(2) && isnan(c(1))==0
    iP=iP+1;
    c= p(iP);
elseif c(1)==c(2)
    c= c(1);
end
end


mp=round(length(t)/2);
%----------states+controls
  ii= 1;
  xb= q(:,ii); ii=ii+1;
 xbd= q(:,ii); ii=ii+1;
  yb= q(:,ii); ii=ii+1;
 ybd= q(:,ii); ii=ii+1;
  Lm= q(:,ii); ii=ii+1;
 Lmd= q(:,ii); ii=ii+1;
Lmdd= q(:,ii); ii=ii+1;
if contains(fnID,'_slackFd')==1 && contains(fnID,'_noDmp')==0
  pLtd= q(:,ii); ii=ii+1;
  qLtd= q(:,ii); ii=ii+1;
 pLtdd= u(:, 4);
 qLtdd= u(:, 5);
if contains(fnID,'_cFmdd')
 qLtdd= q(:,ii); ii=ii+1;
 pLtdd= q(:,ii); ii=ii+1;
pLtddd= u(:, 4);
qLtddd= u(:, 5);
end
end
ybr = range(yb);

%-leg length/velocity
Ll  = sqrt( (yb-yf).^2 + (xb-xf).^2 );   %leg    length
Lt  = Ll -  Lm;                          %tendon length
Lld = ((yb-yf).*ybd + (xb-xf).*xbd)./Ll; %leg    velocity
Ltd = Lld - Lmd;                         %tendon velocity


%-leg angle
sTh= (yb-yf)./Ll; %sin(theta), where theta is the leg angle, CCW from axis: + -->
cTh= (xb-xf)./Ll; %cos(theta), where theta is the leg angle, CCW from axis: + -->
 Th= atan2d(sTh,cTh);
 
Th_gnd= Th-alpha;

Thd= (ybd.*(xb-xf)-(yb-yf).*xbd) ./ ((yb-yf).^2 + (xb-xf).^2); %[rad/s] leg ang vel


%-muscle state
 xm=  cTh.*Lm+xf; %[m] hor pos of muscle end
 ym=  sTh.*Lm+yf; %[m] vrt pos of muscle end
xmd= -sTh.*Thd.*Lm + cTh.*Lmd; %[m/s] hor vel of muscle end
ymd=  cTh.*Thd.*Lm + sTh.*Lmd; %[m/s] vrt vel of muscle end


%-----Spring/Damping Force
if isnan(k(1))==0
Fs= k*(dh_mb-Lt);   Fs(find(t>=Tc))= 0;
else
Fs= zeros(size(t));
end
if isnan(c(1))==0
if contains(fnID,'_slackFd')==1 && contains(fnID,'_noDmp')==0
Fd= -c*qLtd; %damping only during compression 
else
Fd= -c*Ltd;   Fd(find(t>=Tc))= 0;
end
else
Fd= zeros(size(t));
end
%-------------------------------------------------------------Accelerations
% CqV= [1 (1/M) 1 (1/M) 1 1];    %coeff vector
xbdd= (cTh.*Fs + cTh.*Fd - M*g(1))/M; %xbdd
ybdd= (sTh.*Fs + sTh.*Fd - M*g(2))/M; %ybdd
Lmddd= u(:,1);                        %muscle jerk
pPm  = u(:,2);                        %pos muscle power
qPm  = u(:,3);                        %neg muscle power
if contains(fnID,'_cFmdd')
pFmdd= u(:,4);
qFmdd= u(:,5);
end
%--------------------------------------------------------Leg accelerations
%implement quotient rule for d(Lld)/dt
  N1= Lld.*Ll; %numerator(1)
 N1d= ybd.^2 + (yb-yf).*ybdd + xbd.^2 + (xb-xf).*xbdd; %numerator(1) derivative
  D1= Ll;         %denominator(1)
 D1d= Lld;        %denominator(1) derivative
D1sq= Ll.^2;      %denominator(1) squared
Lldd= (D1.*N1d - N1.*D1d)./D1sq; %leg    acceleration
Ltdd= Lldd - Lmdd;                        %tendon acceleration
%----------------------------------------------Rate of Spring/damping Force
if isnan(k(1))==0
Fsd= -k*Ltd;   Fsd(find(t>=Tc))= 0;
else
Fsd= zeros(size(t));
end
if isnan(c(1))==0
if contains(fnID,'_slackFd')==1 && contains(fnID,'_noDmp')==0
Fdd= -c*qLtd; %damping only during compression 
else
Fdd= -c*Ltdd; Fdd(find(t>=Tc))= 0;
end
else
Fdd= zeros(size(t));
end
%--------------------------------------------------muscle force (and rates)
Fm = Fs   + Fd;
Fmd= Fsd  + Fdd;
%------------------------------------------cartesian ground reaction forces
Rx= cTh.*Fm; %[N] hor
Ry= sTh.*Fm; %[N] vrt
%------------------------------------------------------------------Leg jerk
%implement quotient rule again, for d(Lldd)/dt
xbddd= cTh.*Fsd + cTh.*Fdd; %hor body jerk
ybddd= sTh.*Fsd + sTh.*Fdd; %vrt body jerk
N1dd= 3*xbd.*xbdd + (xb-xf).*xbddd +...
      3*ybd.*ybdd + (yb-yf).*ybddd; %--numerator(1) 2nd derivative
D1dd= Lldd;                                  %denominator(1) 2nd derivative

   N2= D1.*N1d  - N1.*D1d;  %numerator(2)
  N2d= D1.*N1dd - N1.*D1dd; %numerator(2) derivative
   D2= D1.^2;               %denominator(2)
  D2d= 2*D1.*D1d;           %denominator(2) derivative
 D2sq= D2.^2;               %denominator(2) squared
Llddd= (D2.*N2d - N2.*D2d)./D2sq; %leg    jerk
Ltddd= Llddd - Lmddd;             %tendon jerk
%--------------------------------------------2nd Force Rate (spring/damper)
if isnan(k(1))==0
Fsdd= -k*Ltdd; %rate of spring force (2nd rate)
else
Fsdd= zeros(size(t)); %no spring
end
if isnan(c(1))==0
    if contains(fnID,'_slackFd')
%         Fddd= -c*qLtddd;  %rate of damping force (2nd rate, only compression)
        Fddd=nan;
    else
        Fddd= -c*Ltddd;   %rate of damping force (2nd rate)
    end
else
Fddd= zeros(size(t)); %no damper
end
Fmdd= Fsdd + Fddd; %muscle 2nd force rate
%---------------------------------------------------------------------Power
Lmd(find(isnan(Lmd)))=0;
Ltd(find(isnan(Ltd)))=0;
Pm= Fm.*Lmd;  %muscle power
Ps= Fs.*Ltd;  %spring power on body
Pd= Fd.*Ltd;  %damper power on body
Pn= Pm+Ps+Pd; %net power
%--------------------------------------------------------------------Energy
E_spr= (0.5/k)*Fs.^2; %spring        potential energy
E_grv= M*gM*yb;       %gravitational potential energy
E_kin= 0.5*M*(xbd.^2+ybd.^2);  %body kinetic energy
%---------------------------------------------------------------Spring Work
Ws_ac= cumtrapz(t,Ps); %accumulating spring work
Ws   = Ws_ac(end);     %net spring work
PsPos=Ps;   PsPos(find(PsPos<0))=0;   WsPos= trapz(t,PsPos); %-pos work
PsNeg=Ps;   PsNeg(find(PsNeg>0))=0;   WsNeg= trapz(t,PsNeg); %-neg work
%---------------------------------------------------------------Damper Work
Wd_ac= cumtrapz(t,Pd); %accumulating damper work
Wd   = Wd_ac(end);     %net damper work
PdPos=Pd;   PdPos(find(PdPos<0))=0;   WdPos= trapz(t,PdPos); %-pos work
PdNeg=Pd;   PdNeg(find(PdNeg>0))=0;   WdNeg= trapz(t,PdNeg); %-neg work
%---------------------------------------------------------------Muscle Work
Wm_ac= cumtrapz(t,Pm); %accumulating muscle work
Wm   = Wm_ac(end);     %net muscle work
Wm_nd= Wm/(M*gM*hb);   %^^^non-dim
PmPos=Pm;   PmPos(find(PmPos<0))=0;   WmPos= trapz(t,PmPos); %-pos work
PmNeg=Pm;   PmNeg(find(PmNeg>0))=0;   WmNeg= trapz(t,PmNeg); %-neg work
%------------------------------------------------------Absolute Muscle Work
absWm_ac= cumtrapz(t,abs(Pm)); %accumulating absolute value of muscle work
absWm   = absWm_ac(end);       %total accumulated absolute value of muscle work
absWm_nd= absWm/(M*gM*hb);     %^^^ non-dim
%------------------------------------------------------------Collision Work
V= sqrt(xbd.^2+ybd.^2);
WcPos= 0; %-zero generative work from collision
WcNeg= 0.5*M*(V(1)^2-V(end)^2); %-collision loss
%------------------------------------------------------------Energy balance
E_net= E_spr + E_grv + E_kin - Wm_ac - Wd_ac; %net energy balance (should be constant)
%-Note: Collision loss not included in E_net since it occurs at the boundary

%--------------------------------------------------------------------------
%check outputs to make sure everything looks kosher
%--------------------------------------------------------------------------
Lmd_ch= (Lm(2:end)-Lm(1:end-1)) ./ (t(2:end)-t(1:end-1));
Lmd_ch= [Lmd_ch(1); Lmd_ch];

Pm_ch= Fm.*Lmd_ch;
Accum_ch= cumtrapz(t,Pm_ch);
Wm_ch= Accum_ch(end);

Accum_ch_abs= cumtrapz(t,abs(Pm_ch));
Wm_ch_abs= Accum_ch_abs(end);
Wm_ch_nd_abs= Wm_ch_abs/(M*gM*hb);



%% ==================================Define output vars in structure OPvars
%-time
OPvars.time.time= t;
OPvars.time.events= [1 pTO length(t); 0 t(pTO) t(end)]; %-heelstrike, takeoff, next heelstrike
OPvars.time.dutyFactor=  DF; %-duty factor
OPvars.time.contactTime= t(pTO); %-ground contact time
OPvars.time.flightTime=  t(end)-t(pTO); %-flight time


%-kinematics
OPvars.kinematics.pos.legAngle=  Th; %-leg angle, CCW from +--> (global)
OPvars.kinematics.pos.legLength= Ll; %-leg length
if strcmpi(OptSol.name,'PMR')
OPvars.kinematics.pos.springLength=   Lt;
OPvars.kinematics.pos.damperLength=   Lt;
OPvars.kinematics.pos.actuatorLength= Lm;
end
OPvars.kinematics.pos.body= [xb  yb]; %-body kinematics
if strcmpi(OptSol.name,'PMR')
OPvars.kinematics.pos.actuatorEnd= [xm  ym]; %-actuator end away from ground
end
OPvars.kinematics.pos.foot= [xf  yf ]; %-foot
OPvars.kinematics.vel.legAngle=  Thd;  %-leg angular velocity, CCW +
OPvars.kinematics.vel.legLength= Lld;  %-leg velocity
OPvars.kinematics.vel.body= [xbd ybd]; %-body
if strcmpi(OptSol.name,'PMR')
OPvars.kinematics.vel.springLength=   Ltd;
OPvars.kinematics.vel.damperLength=   Ltd;
OPvars.kinematics.vel.actuatorLength= Lmd;
end
OPvars.kinematics.acc.leglength=  Lldd;
OPvars.kinematics.acc.body= [xbdd ybdd];
if strcmpi(OptSol.name,'PMR'); OPvars.kinematics.acc.actuatorLength=  Lmdd; end
if ~isnan(Llddd);              OPvars.kinematics.jrk.legLength=      Llddd; end
if strcmpi(OptSol.name,'PMR'); OPvars.kinematics.jrk.actuatorLength= Lmddd; end


%-kinetics
OPvars.kinetics.groundRxnForce= [Rx  Ry]; %-GRF
OPvars.kinetics.actuatorForce=        Fm; %-Actuator force
OPvars.kinetics.actuatorForce_ddt1=   Fmd;
if ~isnan(Fmdd); OPvars.kinetics.actuatorForce_ddt2= Fmdd; end
if strcmpi(OptSol.name,'PMR')
OPvars.kinetics.springForce= Fs; %-Spring force
OPvars.kinetics.damperForce= Fd; %-Damper force
end


%-energetics
OPvars.energetics.work.actuator= [WmNeg WmPos]; %-actuator work (neg, pos)
if strcmpi(OptSol.name,'PMR')
OPvars.energetics.work.spring=   [WsNeg WsPos]; %-spring...
OPvars.energetics.work.damper=   [WdNeg WdPos]; %-damper...
OPvars.energetics.work.collision=[WcNeg WcPos]; %-collision...
end
OPvars.energetics.power.actuator= Pm; %-actuator power
if strcmpi(OptSol.name,'PMR')
OPvars.energetics.power.spring=   Ps; %-spring...
OPvars.energetics.power.damper=   Pd; %-damper...
end


%-cost
OPvars.cost.total=     cTot;
OPvars.cost.work=      cWrk;
OPvars.cost.forceRate= cFRS;


%-units
OPvars.units.time=       [{'s'}]; %-units
OPvars.units.events=     [{'pt'} {'s'}];
OPvars.units.kinematics= [{'m'} {'m/s'} {'m/s2'} {'m/s3'} {'deg'} {'deg/s'} {'deg/s2'}];
OPvars.units.kinetics=   [{'N'} {'N/s'} {'N/s2'}];
OPvars.units.energetics= [{'J'} {'W'}];
OPvars.units.cost=       [{'non-dimensional'}];


%-order fields
OPvars= orderfields(OPvars);
OPvars.units= orderfields(OPvars.units);
OPvars.name= OptSol.name;
OPvars= orderfields(OPvars,[length(fieldnames(OPvars)) 1:length(fieldnames(OPvars))-1]); %-place name at top
%-include input parameters for references
OPvars.Model= Model;




end %-end function