function output= PMR(auxdata)
%-------------------------------------------------------------------------%
%-------------------------------------------------Point Mass Runner (PMR)-%
%-This models simple running locomotion with a point mass body and a------%
%-massless leg made up of a lengthening muscle in series with a spring-  -%
%-damper mechanism. The point of this model is to demonstrate the---------%
%-advantage of elasticity (but disadvantage of compliance) on the work----%
%-one by the muscle to manage a run at some step length/frequency---------%
%-------------------------------------------------------------------------%

%% ===============================================Define Problem Parameters
%-------------------------------------------------------------------------%
% Load Auxdata------------------------------------------------------------%
%-------------------------------------------------------------------------%
dh_mb= auxdata.dhmb;  %[    m] hb - hm
 fnID= auxdata.fnID;  % File ID for saving and Opt desired
   hb= auxdata.h__b;  %[    m] max leg length during grnd contact
   hm= auxdata.h__m;  %[    m] max muscle length during grnd contact
   ds= auxdata.stpL;  %[    m] step length
    M= auxdata.mcom;  %[   kg] body mass
    m= auxdata.mcol;  %[   kg] collision mass (m/M represents momentum loss due to collision)
    g= auxdata.grav;  %[m/s^2] gravitational acceleration
    f= auxdata.mfrq;  %[   Hz] hopping frequency
    k= auxdata.sprC;  %[  N/m] leg spring constant
    c= auxdata.dmpC;  %[ Ns/m] leg damping coefficient
 mmFm= auxdata.cnstr.mmFm;  %[nd] max actuator force
 mmFR= auxdata.cnstr.mmFR;  %[nd] max actuator force rate
 mmPm= auxdata.cnstr.mmPm;  %[nd] max actuator power
 mmER= auxdata.cnstr.mmER;  %[nd] max actuator energy cost rate
    

 MaxMeshi= auxdata.MaxMeshi;  % mesh max iterations
  MeshTol= auxdata.MeshTol;   % mesh tolerance
MaxSNOPTi= auxdata.MaxSNOPTi; % SNOPT max iterations
 SNOPTtol= auxdata.SNOPTtol;  % SNOPT optimal solution tolerance
 
% ------------------------------------------------Non-dimensionalize params
gM = sqrt(sum(g.^2));
tau= sqrt(hb/gM); %[s] time non-dimensionalization factor

w= 2*pi*f;   w= w*tau; %step frequency
T= 1/f;      T= T/tau; %hopping time period


%convert abs --> dim
dh_mb= dh_mb/hb;
hm= hm/hb;
ds= ds/hb;
k= k*(hb/(M*gM));
c= c*(hb/(M*gM*tau));

%-------------------------------------------------------------------------%
% Setup for Problem Bounds------------------------------------------------%
%-------------------------------------------------------------------------%

%---------------------------------------------------------------Time Bounds
bounds.phase.initialtime.lower= 0; %time of initial ground contact
bounds.phase.initialtime.upper= 0;
if contains(fnID,'_DF')
DFID= fnID(findstr(fnID,'_DF')+3:findstr(fnID,'_DF')+6); DFID(find(DFID=='p'))='.';
DF= str2num(DFID);
tf= DF*T*[1 1];
else
tf= [(5e-2)/tau inf];
end
bounds.phase.finaltime.lower= tf(1); %time of leg compression ending
bounds.phase.finaltime.upper= tf(2);
%--------------------------------------------------------------State Bounds
% bounds.phase.state.variables  = [xbi   xbd   yb    ybd   Lm    Lmd   Lmdd]; %for reference
bounds.phase.initialstate.lower = [  0  -inf    0   -inf    0   -inf   -inf]; 
bounds.phase.initialstate.upper = [  0   inf    1      0    1    inf    inf];
bounds.phase.state.lower        = [  0  -inf    0   -inf    0   -inf   -inf];
bounds.phase.state.upper        = [ ds   inf    1    inf    1    inf    inf];
bounds.phase.finalstate.lower   = [  0  -inf    0      0    0   -inf   -inf];
bounds.phase.finalstate.upper   = [ ds   inf    1    inf    1    inf    inf];
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
% bounds.phase.state.variables  = [                                pLtd  qLtd]; %for reference
bounds.phase.initialstate.lower = [bounds.phase.initialstate.lower    0  -inf];
bounds.phase.initialstate.upper = [bounds.phase.initialstate.upper  inf     0];
bounds.phase.state.lower        = [bounds.phase.state.lower           0  -inf];
bounds.phase.state.upper        = [bounds.phase.state.upper         inf     0];
bounds.phase.finalstate.lower   = [bounds.phase.finalstate.lower      0  -inf];
bounds.phase.finalstate.upper   = [bounds.phase.finalstate.upper    inf     0];
if contains(fnID,'_cFmdd')
% bounds.phase.state.variables  = [                                 pLtdd qLtdd]; %for reference
bounds.phase.initialstate.lower = [bounds.phase.initialstate.lower   -inf  -inf];
bounds.phase.initialstate.upper = [bounds.phase.initialstate.upper    inf   inf];
bounds.phase.state.lower        = [bounds.phase.state.lower          -inf  -inf];
bounds.phase.state.upper        = [bounds.phase.state.upper           inf   inf];
bounds.phase.finalstate.lower   = [bounds.phase.finalstate.lower     -inf  -inf];
bounds.phase.finalstate.upper   = [bounds.phase.finalstate.upper      inf   inf];
end
end
%------------------------------------------------------------Control Bounds
if contains(fnID,'_noFRC')
if contains(fnID,'_noDmp')==0
% MscJrkB= [-1 1]*2e3*tau^3/hb; %muscle jerk bounds
MscJrkB= [-1 1]*2e3*tau^3/hb; %muscle jerk bounds
else
MscJrkB= [-inf inf];    
end
else
MscJrkB= [-inf inf];
end
if isfield(auxdata,'jrkB')
MscJrkB= [-1 1]*auxdata.jrkB*tau^3/hb; %muscle jerk bounds
end

bounds.phase.control.lower= [ MscJrkB(1)    0    0 ]; %[Lmddd pPm qPm]
bounds.phase.control.upper= [ MscJrkB(2)  inf  inf ];
if contains(fnID,'_cFmdd')
bounds.phase.control.lower= [bounds.phase.control.lower    0    0]; %[... pFmdd qFmdd]
bounds.phase.control.upper= [bounds.phase.control.upper  inf  inf];
end
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
bounds.phase.control.lower= [bounds.phase.control.lower -inf -inf]; %[pLtdd qLtdd] or [... pLtddd qLtddd] if '_cFmdd'
bounds.phase.control.upper= [bounds.phase.control.upper  inf  inf];
end
%-----------------------------------------------------------Integral Bounds
bounds.phase.integral.lower= [  0   0   0   0]; %[W+ W- FRS pPm*qPm]
bounds.phase.integral.upper= [inf inf inf inf];
if contains(fnID,'_cFmdd')
bounds.phase.integral.lower= [bounds.phase.integral.lower   0]; %[... pFmdd*qFmdd]
bounds.phase.integral.upper= [bounds.phase.integral.upper inf];
end
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
bounds.phase.integral.lower= [bounds.phase.integral.lower    0  ]; %[...  pLtd*qLtd  pLtdd*qLtdd]
bounds.phase.integral.upper= [bounds.phase.integral.upper  inf  ];
end
%----------------------------------------------------------Parameter Bounds
bounds.parameter.lower= -ds; %xf - foot position
bounds.parameter.upper=  ds;
if k(1)~=k(2) && isnan(k(1))==0
bounds.parameter.lower= [bounds.parameter.lower k(1)]; %add spring constant
bounds.parameter.upper= [bounds.parameter.upper k(2)];
end
if c(1)~=c(2) && isnan(c(1))==0
bounds.parameter.lower= [bounds.parameter.lower c(1)]; %add damping coefficient
bounds.parameter.upper= [bounds.parameter.upper c(2)];
end
%-----------------------------------------------------------EndPoint Bounds
I= 6; %-No. of endpoint constraints
if contains(fnID,'_nonZerLtd')==0
I=I+2; 
end
%e.g. connect phases,cyclical Solutions, etc.
for ii=1:I
bounds.eventgroup(ii).lower= 0;
bounds.eventgroup(ii).upper= 0;
end
%-neg tendon acceleration (compression) @0
ii=ii+1;
bounds.eventgroup(ii).lower= -inf;
bounds.eventgroup(ii).upper=    0;
%-constrain min total energy cost per time
if ~isinf(mmER(1))
ii=ii+1;
bounds.eventgroup(ii).lower=   0;
bounds.eventgroup(ii).upper= inf;
end
%-constrain max total energy cost per time
if ~isinf(mmER(2))
ii=ii+1;
bounds.eventgroup(ii).lower=   0;
bounds.eventgroup(ii).upper= inf;
end
%---------------------------------------------------------------Path Bounds
%bounds.phase.path.vars= [Pm-pPm+qPm    Fs   Ll ]
bounds.phase.path.lower= [         0     0    0 ];
bounds.phase.path.upper= [         0   inf    1 ];
if contains(fnID,'_cFmdd')
bounds.phase.path.lower= [bounds.phase(1).path.lower   0]; %[... Fmdd-pFmdd+qFmdd]
bounds.phase.path.upper= [bounds.phase(1).path.upper   0];
end
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
bounds.phase.path.lower= [bounds.phase.path.lower   0 ]; %[... Ltd-pLtd-qLtd]
bounds.phase.path.upper= [bounds.phase.path.upper   0 ];
end
if contains(fnID,'_noDmp') && contains(fnID,'_noCol') && contains(fnID,'_noFRC')
% If Pm=0 is possible, then this is obviously optimal
bounds.phase.path.lower= [bounds.phase.path.lower 0 ]; %isometric muscle contraction
bounds.phase.path.upper= [bounds.phase.path.upper 0 ]; %isometric muscle contraction
end
if contains(fnID,'_DF')
bounds.phase.path.lower= [bounds.phase.path.lower    0 ]; %[... Ry]
bounds.phase.path.upper= [bounds.phase.path.upper  inf ];
end
%-constrain actuator characteristics
if ~isinf(mmFm(1));   bounds.phase.path.lower= [bounds.phase.path.lower   0];   end %-min force
if ~isinf(mmFm(1));   bounds.phase.path.upper= [bounds.phase.path.upper inf];   end %-min force
if ~isinf(mmFR(1));   bounds.phase.path.lower= [bounds.phase.path.lower   0];   end %-min force rate
if ~isinf(mmFR(1));   bounds.phase.path.upper= [bounds.phase.path.upper inf];   end %-min force rate
if ~isinf(mmPm(1));   bounds.phase.path.lower= [bounds.phase.path.lower   0];   end %-min power
if ~isinf(mmPm(1));   bounds.phase.path.upper= [bounds.phase.path.upper inf];   end %-min power
if ~isinf(mmFm(2));   bounds.phase.path.lower= [bounds.phase.path.lower   0];   end %-max force
if ~isinf(mmFm(2));   bounds.phase.path.upper= [bounds.phase.path.upper inf];   end %-max force
if ~isinf(mmFR(2));   bounds.phase.path.lower= [bounds.phase.path.lower   0];   end %-max force rate
if ~isinf(mmFR(2));   bounds.phase.path.upper= [bounds.phase.path.upper inf];   end %-max force rate
if ~isinf(mmPm(2));   bounds.phase.path.lower= [bounds.phase.path.lower   0];   end %-max power
if ~isinf(mmPm(2));   bounds.phase.path.upper= [bounds.phase.path.upper inf];   end %-max power

%-------------------------------------------------------------------------%
% Provide Guess of Solution-----------------------------------------------%
%-------------------------------------------------------------------------%
guess= auxdata.guess;


%-------------------------------------------------------------------------%
% Provide Mesh Refinement Method and Initial Mesh-------------------------%
%-------------------------------------------------------------------------%
% mesh.method        = 'hp-LiuRao-Legendre';
mesh.method        = 'hp-DarbyRao';
mesh.tolerance     =  MeshTol;
mesh.maxiterations = MaxMeshi;
mesh.colpointsmin  =  6;
mesh.colpointsmax  = 20;


%-------------------------------------------------------------------------%
% Assemble Info into Problem Structure------------------------------------%
%-------------------------------------------------------------------------%
setup.name                 = 'PMR';
setup.functions.continuous = @PMR_cont;
setup.functions.endpoint   = @PMR_endp;
setup.displaylevel         = 0;
setup.bounds               = bounds;
setup.mesh                 = mesh;
setup.nlp.solver           = 'snopt';
setup.auxdata              = auxdata;
setup.guess                = guess;

setup.nlp.snoptoptions.tolerance     =  SNOPTtol;
setup.nlp.snoptoptions.maxiterations = MaxSNOPTi;
% setup.nlp.SNOPToptions.linear_solver = 'ma57';

% setup.derivatives.supplier           = 'adigator';
setup.derivatives.derivativelevel    = 'first';
setup.method                         = 'RPM-Differentiation';

%-------------------------------------------------------------------------%
% Solve Problem Using GPOPS2----------------------------------------------%
%-------------------------------------------------------------------------%
tic
output= gpops2(setup);
toc
end

%-------------------------------------------------------------------------%
% BEGIN: cont.m-----------------------------------------------------------%
%-------------------------------------------------------------------------%
function phaseout= PMR_cont(input)
%--------------------------------------------------------------Load Auxdata
dh_mb= input.auxdata.dhmb;  %[    m] hb - hm
 fnID= input.auxdata.fnID;  %        File ID for saving and Opt desired
   hb= input.auxdata.h__b;  %[    m] max CoM height during grnd contact
   hm= input.auxdata.h__m;  %[    m] max muscle length during grnd contact
   ds= input.auxdata.stpL;  %[    m] step length
    M= input.auxdata.mcom;  %[   kg] body mass
    m= input.auxdata.mcol;  %[   kg] collision mass (m/M represents momentum loss due to collision)
    g= input.auxdata.grav;  %[m/s^2] gravitational acceleration
    f= input.auxdata.mfrq;  %[   Hz] hopping frequency
    k= input.auxdata.sprC;  %[  N/m] leg spring constant
    c= input.auxdata.dmpC;  %[ Ns/m] leg damping coefficient
 mmFm= input.auxdata.cnstr.mmFm;  %[nd] max actuator force
 mmFR= input.auxdata.cnstr.mmFR;  %[nd] max actuator force rate
 mmPm= input.auxdata.cnstr.mmPm;  %[nd] max actuator power
 mmER= input.auxdata.cnstr.mmER;  %[nd] max actuator energy cost rate

 epsC= input.auxdata.epsC;  %cost coefficiencts
 eps1= epsC(1);  %Complimentarity cost coefficient
 eps2= epsC(2);  %Force rate cost coefficient
 eps3= epsC(3);  %Complimentarity cost coefficient for damping force

% ------------------------------------------------Non-dimensionalize params
gM = sqrt(sum(g.^2));
tau= sqrt(hb/gM); %[s] time non-dimensionalization factor

w= 2*pi*f;   w= w*tau; %step frequency
T= 1/f;      T= T/tau; %hopping time period

dh_mb= dh_mb/hb;
hm   = hm/hb;
ds   = ds/hb;



iP=1;
P= input.phase(1).parameter;
xf= P(1,iP);
if k(1)~=k(2) && isnan(k(1))==0
iP=iP+1;
k= P(1,iP);
else
k= k(1)*(hb/(M*gM));
end
if c(1)~=c(2) && isnan(c(1))==0
iP=iP+1;
c= P(1,iP);
else
c= c(1)*(hb/(M*gM*tau));
end


%--------------------------------------------------------Define Opt Vectors
t= input.phase.time;    %time
q= input.phase.state;   %states
u= input.phase.control; %controls

%-states
  ii=1;
  xb= q(:,ii);ii=ii+1; %hor body pos
 xbd= q(:,ii);ii=ii+1; %hor body vel
  yb= q(:,ii);ii=ii+1; %vrt body pos
 ybd= q(:,ii);ii=ii+1; %vrt body vel
  Lm= q(:,ii);ii=ii+1; %muscle pos
 Lmd= q(:,ii);ii=ii+1; %muscle vel
Lmdd= q(:,ii);ii=ii+1; %muscle acc
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
pLtd = q(:,ii);ii=ii+1; %pos damper velocity
qLtd = q(:,ii);ii=ii+1; %neg damper velocity
if contains(fnID,'_cFmdd')
pLtdd= q(:,ii);ii=ii+1; %pos damper acceleration
qLtdd= q(:,ii);ii=ii+1; %neg damper acceleration
end
end

%-controls
   ii= 1;
Lmddd= u(:,ii);ii=ii+1; %muscle acceleration
  pPm= u(:,ii);ii=ii+1; %pos muscle power
  qPm= u(:,ii);ii=ii+1; %neg muscle power
if contains(fnID,'_cFmdd')
pFmdd= u(:,ii);ii=ii+1; %pos muscle force rate (2nd)
qFmdd= u(:,ii);ii=ii+1; %neg muscle force rate (2nd)
end
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
if contains(fnID,'_cFmdd')
pLtddd= u(:,ii);ii=ii+1; %pos damper jerk
qLtddd= u(:,ii);ii=ii+1; %neg damper jerk
else
pLtdd=  u(:,ii);ii=ii+1; %pos damper acceleration
qLtdd=  u(:,ii);ii=ii+1; %neg damper acceleration
end
end


%-leg length/velocity
 Ll= sqrt( (yb).^2 + (xb-xf).^2 ); %leg    length
 Lt= Ll -  Lm;                     %tendon length
Lld= (yb.*ybd + (xb-xf).*xbd)./Ll; %leg    velocity
Ltd= Lld - Lmd;                    %tendon velocity


%-Spring/Damper/Muscle Forces & Muscle Power
if isnan(k(1))==0
Fs= k*(dh_mb-Lt); %spring force
else
Fs= zeros(size(t)); %no spring
end
if isnan(c(1))==0
    if contains(fnID,'_slackFd')==1 && contains(fnID,'_noDmp')==0
        Fd= -c*qLtd; %damping force (only damping compression)
    else
        Fd= -c*Ltd;  %damping force
    end
else
Fd= zeros(size(t)); %no damper
end
Fm= Fs + Fd; %muscle force (muscle in series with spring/damper)
Pm= Fm.*Lmd; %muscle power


%-Dynamics
CqV= [1 (gM*tau^2/hb) 1 (gM*tau^2/hb) 1 1 1]; %coeff vector
cTh= (xb-xf)./Ll; %cos(\theta), where \theta is the leg angle defined CCW from (+)-->
sTh= (yb   )./Ll; %sin(\theta), where \theta is ^^^
ii = 1;
qdot(:,ii)= CqV(1)*(xbd);                     ii=ii+1;%xbd
qdot(:,ii)= CqV(2)*(cTh.*Fs+cTh.*Fd-g(1)/gM); ii=ii+1;%xbdd
qdot(:,ii)= CqV(3)*(ybd);                     ii=ii+1;%ybd
qdot(:,ii)= CqV(4)*(sTh.*Fs+sTh.*Fd-g(2)/gM); ii=ii+1;%ybdd
qdot(:,ii)= CqV(5)*(Lmd);                     ii=ii+1;%Lmd
qdot(:,ii)= CqV(6)*(Lmdd);                    ii=ii+1;%Lmdd
qdot(:,ii)= CqV(7)*(Lmddd);                   ii=ii+1;%Lmddd
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
qdot(:,ii)= pLtdd;  ii=ii+1;%positive damper acceleration
qdot(:,ii)= qLtdd;  ii=ii+1;%negative damper acceleration
if contains(fnID,'_cFmdd')
qdot(:,ii)= pLtddd; ii=ii+1;%positive damper jerk
qdot(:,ii)= qLtddd; ii=ii+1;%negative damper jerk
end
end
phaseout.dynamics= qdot;


%-Leg acceleration
xbdd= qdot(:,2);
ybdd= qdot(:,4);
%implement quotient rule for d(Lld)/dt
  N1= Lld.*Ll; %numerator(1)
 N1d= ybd.^2 + yb.*ybdd + xbd.^2 + (xb-xf).*xbdd; %numerator(1) derivative
  D1= Ll;         %denominator(1)
 D1d= Lld;        %denominator(1) derivative
D1sq= Ll.^2;      %denominator(1) squared
Lldd= (D1.*N1d - N1.*D1d)./D1sq; %leg    acceleration
Ltdd= Lldd - Lmdd;               %tendon acceleration


%-1st Force Rate (Spring/Damper/Muscle)
if isnan(k(1))==0
Fsd= -k*Ltd;  %rate of spring force
else
Fsd= zeros(size(t)); %no spring
end
if isnan(c(1))==0
    if contains(fnID,'_slackFd')
        Fdd= -c*qLtdd;  %rate of damping force (only compression)
    else
        Fdd= -c*Ltdd;   %rate of damping force
    end
else
Fdd= zeros(size(t)); %no damper
end
Fmd= Fsd + Fdd; %muscle force rate (1st order)


if contains(fnID,'_cFmdd')
%-Leg jerk
%-implement quotient rule again, for d(Lldd)/dt
xbddd= CqV(2)*(cTh.*Fsd + cTh.*Fdd); %hor body jerk
ybddd= CqV(4)*(sTh.*Fsd + sTh.*Fdd); %vrt body jerk
 N1dd= 3*xbd.*xbdd + (xb-xf).*xbddd +...
       3*ybd.*ybdd + (yb   ).*ybddd; %--numerator(1) 2nd derivative
 D1dd= Lldd;                         %denominator(1) 2nd derivative
   N2= D1.*N1d  - N1.*D1d;  %--numerator(2)
  N2d= D1.*N1dd - N1.*D1dd; %--numerator(2) derivative
   D2= D1.^2;               %denominator(2)
  D2d= 2*D1.*D1d;           %denominator(2) derivative
 D2sq= D2.^2;               %denominator(2) squared
Llddd= (D2.*N2d - N2.*D2d)./D2sq; %leg    jerk
Ltddd= Llddd - Lmddd;             %tendon jerk

%-2nd Force Rate (Spring/Damper) + Muscle Force & Rates & Power
if isnan(k(1))==0
Fsdd= -k*Ltdd; %rate of spring force (2nd rate)
else
Fsdd= zeros(size(t)); %no spring
end
if isnan(c(1))==0
    if contains(fnID,'_slackFd')==1 && contains(fnID,'_noDmp')==0
        if contains(fnID,'_cFmdd')
        Fddd= -c*qLtddd; %rate of damping force (2nd rate)
        else
        Fddd= zeros(size(t)); 
        end
    else
        Fddd= -c*Ltddd;  %rate of damping force (2nd rate)
    end
else
Fddd= zeros(size(t)); %no damper
end

Fmdd= Fsdd + Fddd; %muscle force rate (2nd order)
end


phaseout.path= [ Pm-pPm+qPm   Fs   Ll ]; %path constraints
if contains(fnID,'_cFmdd')
phaseout.path= [phaseout.path   Fmdd-pFmdd+qFmdd ]; %add slack constraint for Fmdd
end
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
% phaseout.path= [phaseout.path   Ltd-pLtd-qLtd   Ltdd-pLtdd-qLtdd]; %path constraints
phaseout.path= [phaseout.path   Ltd-pLtd-qLtd]; %path constraints
end
if contains(fnID,'_noDmp') && contains(fnID,'_noCol') && contains(fnID,'_noFRC')
% If Pm=0 is possible, then this is obviously optimal
phaseout.path= [phaseout.path Lmd]; %isometric muscle contraction
end
if contains(fnID,'_DF')
Ry= sTh.*Fm; %vrt GRF
DFID= fnID(findstr(fnID,'_DF')+3:findstr(fnID,'_DF')+6); DFID(find(DFID=='p'))='.';
DF= str2num(DFID);
Tc= DF*T; %time of contact
B= T/Tc;
C= 4*B/(Tc^2);
A= 0.5*Tc;
LowForceBnd= B-C*(t-A).^2; %neg parabola with peak = ave force
phaseout.path= [phaseout.path  Ry-LowForceBnd ]; %add lower force bound to ensure correct DF 
end

%-constrain actuator characteristics
if ~isinf(mmFm(1));   phaseout.path= [phaseout.path  Fm-mmFm(1)];   end %-min force
if ~isinf(mmFR(1));   phaseout.path= [phaseout.path Fmd-mmFR(1)];   end %-min force rate
if ~isinf(mmPm(1));   phaseout.path= [phaseout.path  Pm-mmPm(1)];   end %-min power
if ~isinf(mmFm(2));   phaseout.path= [phaseout.path mmFm(2)-Fm ];   end %-max force
if ~isinf(mmFR(2));   phaseout.path= [phaseout.path mmFR(2)-Fmd];   end %-max force rate
if ~isinf(mmPm(2));   phaseout.path= [phaseout.path mmPm(2)-Pm ];   end %-max power


%-Muscle Power Coefficients (e.g. inverse muscle contraction effciency, etc.)
if length(strfind(fnID,'_DiffWorkEff'))==0
Cp= 1;
Cq= 1;
elseif length(strfind(fnID,'_DiffWorkEff'))==1
Cp= 1/0.25; % 25% efficiency for positive muscle work
Cq= 1/1.20; %120% efficiency for negative muscle work
end
Ca= 0.5*(Cp+Cq);
if length(strfind(fnID,'_ZeroNegWrkCost'))==1
Cq=  0;
Ca= Cp;
end

%-Cost Function
if contains(fnID,'_cFmdd')
   ForceRateCost= pFmdd+qFmdd;
elseif contains(fnID,'_noFRC')
   ForceRateCost= zeros(size(t));
else
   ForceRateCost= Fmd.^2;
end
phaseout.integrand= [Cp*pPm  Cq*qPm  Ca*eps2*ForceRateCost  Ca*eps1*pPm.*qPm]; %Cost Term Integrands
if contains(fnID,'_cFmdd')
phaseout.integrand= [phaseout.integrand Ca*eps1*pFmdd.*qFmdd];
end
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
% phaseout.integrand= [phaseout.integrand  -Ca*eps3*pLtd.*qLtd  Ca*eps3*(pLtdd.^2).*(qLtdd.^2)];
phaseout.integrand= [phaseout.integrand -Ca*eps3*pLtd.*qLtd ];
end


end
%-------------------------------------------------------------------------%
% END: cont.m-------------------------------------------------------------%
%-------------------------------------------------------------------------%


%-------------------------------------------------------------------------%
% BEGIN: endp.m-----------------------------------------------------------%
%-------------------------------------------------------------------------%
function output= PMR_endp(input)
%--------------------------------------------------------------Load Auxdata
%-parameters
fnID = input.auxdata.fnID; % FileID
dh_mb= input.auxdata.dhmb; %[    m] max tendon length
ds   = input.auxdata.stpL; %[    m] step length
hb   = input.auxdata.h__b; %[    m] max leg    length
M    = input.auxdata.mcom; %[   kg] body mass
m    = input.auxdata.mcol; %[   kg] collision mass (m/M represents momentum loss due to collision)
g    = input.auxdata.grav; %[m/s^2] gravitational acceleration
f    = input.auxdata.mfrq; %[   Hz] hopping frequency
mmFm = input.auxdata.cnstr.mmFm; %[nd] max actuator force
mmFR = input.auxdata.cnstr.mmFR; %[nd] max actuator force rate
mmPm = input.auxdata.cnstr.mmPm; %[nd] max actuator power
mmER = input.auxdata.cnstr.mmER; %[nd] max actuator energy cost rate
 
%-non-dimensionalize parameters
gM   = sqrt(sum(g.^2));
dh_mb= dh_mb/hb;
ds   =    ds/hb;
tau= sqrt(hb/gM); %[s] time non-dimensionalization factor


%-------------------initial states
  xbi= input.phase.initialstate(1);
 xbdi= input.phase.initialstate(2);
  ybi= input.phase.initialstate(3);
 ybdi= input.phase.initialstate(4);
  Lmi= input.phase.initialstate(5);
 Lmdi= input.phase.initialstate(6);
Lmddi= input.phase.initialstate(7);
%---------------------final states
  xbf= input.phase.finalstate(1);
 xbdf= input.phase.finalstate(2);
  ybf= input.phase.finalstate(3);
 ybdf= input.phase.finalstate(4);
  Lmf= input.phase.finalstate(5);
 Lmdf= input.phase.finalstate(6);
Lmddf= input.phase.finalstate(7);
%-------------initial/final time
to= input.phase.initialtime;
tf= input.phase.finaltime;



%parameters
iP=1;
P= input.parameter;
xf= P(1,iP);

%leg/tendon length/velocity
 Lli= sqrt( (ybi).^2 + (xbi-xf).^2 );    %initial leg    length
 Lti= Lli -  Lmi;                        %initial tendon length
Lldi= (ybi.*ybdi + (xbi-xf).*xbdi)./Lli; %initial leg    velocity
Ltdi= Lldi - Lmdi;                       %initial tendon velocity
 Llf= sqrt( (ybf).^2 + (xbf-xf).^2 );    %final   leg    length
 Ltf= Llf -  Lmf;                        %final   tendon length
Lldf= (ybf.*ybdf + (xbf-xf).*xbdf)./Llf; %final   leg    velocity
Ltdf= Lldf - Lmdf;                       %final   tendon velocity
%implement quotient rule for d(Lldi)/dt
CqV= [(gM*tau^2/hb) (gM*tau^2/hb)]; %coeff vector
cThi= (xbi-xf)./Lli; %cos(\theta), where \theta is the leg angle defined CCW from (+)-->
sThi= (ybi   )./Lli; %sin(\theta), where \theta is ^^^
%-Initial/final spring/damper force
Fsi= dh_mb-Lti; %initial spring deflection
Fsf= dh_mb-Ltf; %final   spring deflection
Fdi=      Ltdi; %initial damper velocity
Fdf=      Ltdf; %final   damper velocity
xbddi= CqV(1)*(cThi.*Fsi+cThi.*Fdi-g(1)/gM); %xbdd
ybddi= CqV(2)*(sThi.*Fsi+sThi.*Fdi-g(2)/gM); %ybdd
  N1= Lldi.*Lli; %numerator(1)
 N1d= ybdi.^2 + ybi.*ybddi + xbdi.^2 + (xbi-xf).*xbddi; %numerator(1) derivative
  D1= Lli;         %denominator(1)
 D1d= Lldi;        %denominator(1) derivative
D1sq= Lli.^2;      %denominator(1) squared
Llddi= (D1.*N1d - N1.*D1d)./D1sq; %leg    acceleration
Ltddi= Llddi - Lmddi;             %tendon acceleration



%---------initial/final muscle states
%-intial
sThi= (ybi   )./Lli; %sin(theta), where theta is the leg angle, CCW from axis: + -->
cThi= (xbi-xf)./Lli; %cos(theta), where theta is the leg angle, CCW from axis: + -->
Thdi= (ybdi.*(xbi-xf)-ybi.*xbdi) ./ (ybi.^2 + (xbi-xf).^2); %[rad/s] leg ang vel
 xmi=  cThi.*Lmi+xf; %[m] hor pos of muscle end
 ymi=  sThi.*Lmi;    %[m] vrt pos of muscle end
xmdi= -sThi.*Thdi.*Lmi + cThi.*Lmdi; %[m/s] hor vel of muscle end
ymdi=  cThi.*Thdi.*Lmi + sThi.*Lmdi; %[m/s] vrt vel of muscle end
%-final
sThf= (ybf   )./Llf; %sin(theta), where theta is the leg angle, CCW from axis: + -->
cThf= (xbf-xf)./Llf; %cos(theta), where theta is the leg angle, CCW from axis: + -->
Thdf= (ybdf.*(xbf-xf)-ybf.*xbdf) ./ (ybf.^2 + (xbf-xf).^2); %[rad/s] leg ang vel
 xmf=  cThf.*Lmf+xf; %[m] hor pos of muscle end
 ymf=  sThf.*Lmf;    %[m] vrt pos of muscle end
xmdf= -sThf.*Thdf.*Lmf + cThf.*Lmdf; %[m/s] hor vel of muscle end
ymdf=  cThf.*Thdf.*Lmf + sThf.*Lmdf; %[m/s] vrt vel of muscle end



alpha= atan2d(g(1),g(2)); %ground angle
%-use rotation matrix to transform local body coordinate into global
Rmat= [cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)];
%-transform initial positions
pgi= Rmat*[xbi; ybi];
xgi= pgi(1); ygi= pgi(2);
%-transform final positions
pgf= Rmat*[xbf; ybf];
xgf= pgf(1); ygf= pgf(2);
%-transform initial velocities
vgi = Rmat*[xbdi; ybdi];
xgdi= vgi(1); ygdi= vgi(2);
%-transform final velocities
vgf = Rmat*[xbdf; ybdf];
xgdf= vgf(1); ygdf= vgf(2);


%-Time params
T = 1/f;         %[   s] step time period
T = T/tau;       %[nonD] step time period
Tc= tf-to;       %[nonD] time of ground phase
Mc= 1-m/M; %fraction of momentum conserved after collision
Tf= (ygdf-ygdi/Mc)*(hb/tau)/gM; %[s] time of flight phase 
dyf= (ygdf*(hb/tau))*Tf - 0.5*gM*Tf^2; %[m] net change in body height during flight phase
Tf= Tf/tau;      %[nonD] time of flight phase

dxg= xgf-xgi;        %change in horizontal position during ground contact
dsx= ds*cosd(alpha); %horizontal component of step length
dsy= ds*sind(alpha); %vertical   component of step length
% -----------------------------------------------------Endpoint Constraints
ii=1;
%-other endP constraints
output.eventgroup(ii).event= Fsi;                  ii=ii+1; %spring  force starts @0
output.eventgroup(ii).event= Fsf;                  ii=ii+1; %spring  force ends   @0
output.eventgroup(ii).event= T-Tc-Tf;              ii=ii+1; %enforce step time
output.eventgroup(ii).event= dxg+xgdf*Tf-dsx;      ii=ii+1; %enforce step length
output.eventgroup(ii).event= (ygf-ygi)*hb+dyf-dsy; ii=ii+1; %periodic gait
output.eventgroup(ii).event= xgdf-xgdi/Mc;         ii=ii+1; %periodic gait
if contains(fnID,'_nonZerLtd')==0
output.eventgroup(ii).event= Fdi;                  ii=ii+1; %damping force starts @0
output.eventgroup(ii).event= Fdf;                  ii=ii+1; %damping force ends   @0
end
output.eventgroup(ii).event= Ltddi;                ii=ii+1; %neg tendon acceleration (compression) @0


%-constain total energy cost per time
E = sum(input.phase.integral(1:3)); %-total energy cost per step (work + FRS)
ER= E*f; %-total energy cost per time (work + FRS)
if ~isinf(mmER(1));   output.eventgroup(ii).event= ER-mmER(1);   end; ii=ii+1; %-min energy rate
if ~isinf(mmER(2));   output.eventgroup(ii).event= mmER(2)-ER;   end; ii=ii+1; %-max energy rate


q = sum(input.phase.integral);
output.objective= sum(q);
end
%-------------------------------------------------------------------------%
% END: endp.m-------------------------------------------------------------%
%-------------------------------------------------------------------------%













