function [t q u p C] = PMR_NonDim2AbsDim(OptSol)
%====================================================================Inputs
%  OptSol  --> optimal solution
%===================================================================Outputs
%  t --> time    var
%  q --> state   vars
%  u --> control vars
%  p --> output parameters from optimization
%  C --> cost terms
%==========================================================================
%  This function dimensionalizes OptSol vars with combinations of M,L and g
%  for model PMR.


% non-dim variables
fnID= OptSol.result.setup.auxdata.fnID; % File ID for saving and Opt desired
   M= OptSol.result.setup.auxdata.mcom; %[  kg] body mass
   g= OptSol.result.setup.auxdata.grav; %[m/s2] gravitational acceleration
  hb= OptSol.result.setup.auxdata.h__b; %[   m] standing height
  gM= sqrt(sum(g.^2));
 tau= sqrt(hb/gM);                      %[   s] time non-dimensionalization factor


iP=1;
p(iP)= OptSol.result.interpsolution.parameter(iP)*hb; %xf - foot position
% ----------------------------------------------------------Spring Constant
if isfield(OptSol.result.setup.auxdata,'sprC')==1
    k= OptSol.result.setup.auxdata.sprC; %[N/m] spring constant
    if isnan(k(1))==1
    k=nan;
    elseif k(1)==k(2)
    k=k(1);
    else
    iP=iP+1;
    p(iP)= OptSol.result.interpsolution.parameter(iP)*(M*g/hb);
    end
end
% ------------------------------------------------------Damping coefficient
if isfield(OptSol.result.setup.auxdata,'dmpC')==1
    c= OptSol.result.setup.auxdata.dmpC; %[Ns/m] damping coefficient
    if isnan(c(1))==1
    c=nan;
    elseif c(1)==c(2)
    c=c(1);
    else
    iP=iP+1;
    p(iP)= OptSol.result.interpsolution.parameter(iP)*(M*g*tau/hb);
    end
end


% Define opt model outputs
% t= OptSol.result.interpsolution.phase.time;
% q= OptSol.result.interpsolution.phase.state;
% u= OptSol.result.interpsolution.phase.control;
t=[];   q=[];   u=[];   C=[];
for i=1:1 %2phase
    t= [t; OptSol.result.solution.phase(i).time(1:end-1)];
    q= [q; OptSol.result.solution.phase(i).state(1:end-1,:)];
    u= [u; OptSol.result.solution.phase(i).control(1:end-1,:)];
    C= [C; OptSol.result.solution.phase(i).integral];
end
    t= [t; OptSol.result.solution.phase(i).time(end)];
    q= [q; OptSol.result.solution.phase(i).state(end,:)];
    u= [u; OptSol.result.solution.phase(i).control(end,:)];
%     C= sum(C);

% Dimensionalize non-dim model outputs
t= t*tau;

%-states
ii=1;
q(:,ii)= q(:,ii)*hb;       ii=ii+1;
q(:,ii)= q(:,ii)*hb/tau;   ii=ii+1;
q(:,ii)= q(:,ii)*hb;       ii=ii+1;
q(:,ii)= q(:,ii)*hb/tau;   ii=ii+1;
q(:,ii)= q(:,ii)*hb;       ii=ii+1;
q(:,ii)= q(:,ii)*hb/tau;   ii=ii+1;
q(:,ii)= q(:,ii)*hb/tau^2; ii=ii+1;
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
q(:,ii)= q(:,ii)*hb/tau;   ii=ii+1;
q(:,ii)= q(:,ii)*hb/tau;   ii=ii+1;
if contains(fnID,'_cFmdd')
q(:,ii)= q(:,ii)*hb/tau^2; ii=ii+1;
q(:,ii)= q(:,ii)*hb/tau^2; ii=ii+1;
end
end

%-controls
ii=1;
u(:,ii)= u(:,ii)*hb/tau^3;   ii=ii+1;
u(:,ii)= u(:,ii)*M*gM*hb/tau; ii=ii+1;
u(:,ii)= u(:,ii)*M*gM*hb/tau; ii=ii+1;
if contains(fnID,'_cFmdd')
u(:,ii)= u(:,ii)*M*gM/tau^2; ii=ii+1;
u(:,ii)= u(:,ii)*M*gM/tau^2; ii=ii+1;
end
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
if contains(fnID,'_cFmdd')
u(:,ii)= u(:,ii)*hb/tau^3; ii=ii+1;
u(:,ii)= u(:,ii)*hb/tau^3; ii=ii+1;
else
u(:,ii)= u(:,ii)*hb/tau^2; ii=ii+1;
u(:,ii)= u(:,ii)*hb/tau^2; ii=ii+1; 
end
end
% C([1 5  9]) = C([1 5  9])*M*gM*h;
% C([2 6 10]) = C([2 6 10])*(M*gM)^2/tau;
C= C;

if exist('p','var')==0
    p=nan;
end




















end




   
   
   
   
   
   
   