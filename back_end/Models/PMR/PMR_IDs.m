function fnID= PMR_IDs(Model, ProbSet)
%====================================================================Inputs
%  Model --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%  fnID  --> model identifier string
%==========================================================================
%  This function compiles a model identifier string with information about
%  the desired optimization for PMR (point-mass runner) model



iSP= ProbSet.iSP;

Model_cell= struct2cell(Model); %-convert Model struct to cell array
fnames= fieldnames(Model);%-field names in Model struct
iAP= [cellfun(@isnumeric,Model_cell).*cellfun('length',Model_cell)];
iAP(find(iAP>1))= iSP;


fnID='OptSol_';
fnID= [fnID Model.Name];

%-Scale work?
if strcmpi(Model.ScaleWork,'yes')
fnID= [fnID '_DiffWorkEff'];
end

%-Ground slope
i= strcmp(fnames,'GroundSlope'); S= Model_cell{i}(iAP(i));
if round(S,2)~=0
ID= num2str(round(S,2),'%1.2f'); ID(find(ID=='.'))='p';
if S<=0
fnID= [fnID '_Slope'  ID];
elseif S>0
fnID= [fnID '_Slope+' ID];
end
end

%-Collision?
i= strcmp(fnames,'CollisionFraction'); CF= Model_cell{i}(iAP(i));
if round(CF,2)>0
ID= num2str(round(CF,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID '_wCol' ID];
end

%-Force-rate cost?
i= strcmp(fnames,'ForceRateCoef'); cR= Model_cell{i}(iAP(i));
if cR==0
fnID= [fnID '_nonZerLtd_FreeBnds_noFRC'];
elseif cR>0
ID= [num2str(round(cR/(10^(floor(log10(cR)))),2),'%1.2f'),...
     'e' num2str(floor(log10(cR)),'%1.0f')]; ID(find(ID=='.'))='p';
fnID= [fnID '_FreeBnds_wFRC_eps' ID];
end

%-Damping?
i= strcmp(fnames,'DampingRatio'); DR= Model_cell{i}(iAP(i));
if round(DR,2)==0
fnID= [fnID '_noDmp'];
elseif round(DR,2)>0
ID= num2str(round(DR,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID '_slackFd_DmpRat' ID];
end

if round(DR,2)==0 & round(CF,2)==0 & round(S,2)==0
fnID= [fnID '_noCol'];
end

%-Spring constant
i= strcmp(fnames,'SpringConstant'); k= Model_cell{i}(iAP(i));
k_ref= 27370.46501693961;
k_rel= k/k_ref;
if round(DR,2)==0 & round(CF,2)==0 & round(S,2)==0
ID= num2str(round(k_rel,2),'%1.2f'); ID(find(ID=='.'))='p';
else
ID= num2str(round(k_rel,1),'%1.1f'); ID(find(ID=='.'))='p';
end
fnID= [fnID '_krel' ID];

%-Step length
if isnumeric(Model.StepLength)==1
i= strcmp(fnames,'StepLength'); ds= Model_cell{i}(iAP(i));
ID= num2str(round(ds,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID '_ds' ID];
elseif ischar(Model.StepLength)==1 & strcmpi(Model.StepLength,'G06')==1
fnID= [fnID '_Vopt'];
end

%-Step frequency
i= strcmp(fnames,'Speed'); Sp= Model_cell{i}(iAP(i));
if isnumeric(Model.StepLength)
fs= Sp/ds;
elseif ischar(Model.StepLength)==1 & strcmpi(Model.StepLength,'G06')==1
fs= 0.2592*Sp+2.1651; %-relationship from (Gutman et al, 2005)
end
ID= num2str(round(fs,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID '_fref2p25_f' ID];

%-Gravity level
i= strcmp(fnames,'RelativeGravity'); gC= Model_cell{i}(iAP(i));
if gC~=1
ID= num2str(round(gC,2),'%1.2f'); ID(find(ID=='.'))='p';
fnID= [fnID,'_gRel' ID]; 
end

if round(DR,2)==0 & round(CF,2)==0 & cR==0 & round(S,2)==0
fnID(findstr(fnID,'_FreeBnds'):findstr(fnID,'_FreeBnds')+8)='';
end













end