function PMR_PlotOptSol(OPvars, Model, ProbSet, OptSol, View)
%====================================================================Inputs
%  OPvars  --> basic vars from OptSol
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  OptSol  --> optimal solution
%  View    --> structure indicating what plots to show
%==========================================================================
%  This function plots summary of vars in OPvars


if isfield(View,'Preset'); if ~isempty(View.Preset); return; end; end %-bypass if preset


iSP= ProbSet.iSP;
%% ===========================================================Define Params
Model_cell= struct2cell(Model); %-convert Model struct to cell array
fnames= fieldnames(Model);      %-field names in Model struct
iAP= [cellfun(@isnumeric,Model_cell).*cellfun('length',Model_cell)];
iAP(find(iAP>1))= iSP;

i= strcmp(fnames,'Speed');             Sp= Model_cell{i}(iAP(i)); %-speed
i= strcmp(fnames,'BodyMass');          M = Model_cell{i}(iAP(i)); %-body mass
% i= strcmp(fnames,'DutyFactor');      DF= Model_cell{i}(iAP(i)); %-duty factor
DF= nan;
i= strcmp(fnames,'GroundSlope');       S = Model_cell{i}(iAP(i)); %-ground slope
i= strcmp(fnames,'DampingRatio');      DR= Model_cell{i}(iAP(i)); %-damping ratio
i= strcmp(fnames,'MaxLegLength');      hb= Model_cell{i}(iAP(i)); %-max allowable leg length
i= strcmp(fnames,'ForceRateCoef');     cR= Model_cell{i}(iAP(i)); %-force rate coefficient
i= strcmp(fnames,'SpringConstant');    k = Model_cell{i}(iAP(i)); %-leg spring constant
i= strcmp(fnames,'RelativeGravity');   gC= Model_cell{i}(iAP(i)); %-relative gravity
i= strcmp(fnames,'CollisionFraction'); CF= Model_cell{i}(iAP(i)); %-collision fraction


i= strcmp(fnames,'StepLength');
if ischar(Model_cell{i})
if strcmpi(Model_cell{i},'G06')
fs= 0.2592*Sp+2.1651; %[Hz] step frequency: relationship based on Gutman et al., 2006
ds= Sp./fs; %[m] step length
else
error(['Unknown code for Model.StepLength: ' Model_cell{i}]);
end
elseif isnumeric(Model_cell{i})
ds= Model_cell{i}(iAP(i)); %[m] step length
fs= Sp./ds; %[Hz] step frequency
end
gM= 9.81; %[m/s2] Earth's gravitational acceleration

if isfield(OptSol.result.setup.auxdata,'h__m')
hm= OptSol.result.setup.auxdata.h__m;
else
hm= nan;
end
dh_mb= hb-hm;


%% ==================================Define output vars in structure OPvars

%-time
t  = OPvars.time.time; %-time
T  = t(end); %-step  time
to = t(1);   %-start time
tf = t(end); %-end   time
DF = OPvars.time.dutyFactor;  %-duty factor
Tc = OPvars.time.contactTime; %-ground contact time
Tf = OPvars.time.flightTime;  %-flight time
pTO= OPvars.time.events(1,2); %-point of take off

%-kinematics
Th= OPvars.kinematics.pos.legAngle;  %-leg angle, CCW from +--> (global)
Ll= OPvars.kinematics.pos.legLength; %-leg length
Lt= OPvars.kinematics.pos.springLength;
Lm= OPvars.kinematics.pos.actuatorLength;

xb= OPvars.kinematics.pos.body(:,1); %-body hor pos
yb= OPvars.kinematics.pos.body(:,2); %-body vrt pos
xm= OPvars.kinematics.pos.actuatorEnd(:,1); %-actuator end away from ground (hor)
ym= OPvars.kinematics.pos.actuatorEnd(:,2); %-actuator end away from ground (vrt)
xf= OPvars.kinematics.pos.foot(:,1);  %-foot hor pos
yf= OPvars.kinematics.pos.foot(:,2);  %-foot vrt pos

Thd= OPvars.kinematics.vel.legAngle;  %-leg angular velocity, CCW +
Lld= OPvars.kinematics.vel.legLength; %-leg velocity
xbd= OPvars.kinematics.vel.body(:,1); %-body hor pos
ybd= OPvars.kinematics.vel.body(:,2); %-body vrt pos
Ltd= OPvars.kinematics.vel.springLength;   %-spring vel
Lmd= OPvars.kinematics.vel.actuatorLength; %-actuator vel

Lldd= OPvars.kinematics.acc.leglength; %-leg acc
xbdd= OPvars.kinematics.acc.body(:,1); %-hor body acc
ybdd= OPvars.kinematics.acc.body(:,2); %-vrt body acc
Lmdd=  OPvars.kinematics.acc.actuatorLength;
Lmddd= OPvars.kinematics.jrk.actuatorLength;
if isfield(OPvars.kinetics,'jrk'); Llddd= OPvars.kinematics.jrk.legLength;       else; Llddd=nan; end



%-kinetics
Rx=  OPvars.kinetics.groundRxnForce(:,1); %-hor GRF
Ry=  OPvars.kinetics.groundRxnForce(:,2); %-vrt GRF
Fm=  OPvars.kinetics.actuatorForce;  %-Actuator force
Fmd= OPvars.kinetics.actuatorForce_ddt1; %-d/dt actuator force
if isfield(OPvars.kinetics,'actuatorForce_ddt2'); Fmdd= OPvars.kinetics.actuatorForce_ddt2; else; Fmdd=nan; end %-d2/dt2 actuator force
if isfield(OPvars.kinetics,'springForce');        Fs  = OPvars.kinetics.springForce;        else; Fs  =nan; end %-Spring force
if isfield(OPvars.kinetics,'damperForce');        Fd  = OPvars.kinetics.springForce;        else; Fd  =nan; end %-Damper force


%-energetics
negMscWrk= OPvars.energetics.work.actuator(1); %-neg actuator work
posMscWrk= OPvars.energetics.work.actuator(2); %-pos actuator work
absMscWrk= posMscWrk-negMscWrk; %-abs actuator work
netMscWrk= posMscWrk+negMscWrk; %-net actuator work
negSprWrk= OPvars.energetics.work.spring(1); %-neg spring work
posSprWrk= OPvars.energetics.work.spring(2); %-pos spring work
negDmpWrk= OPvars.energetics.work.damper(1); %-neg damper work
posDmpWrk= OPvars.energetics.work.damper(2); %-pos damper work

Pm= OPvars.energetics.power.actuator; %-actuator power
Ps= OPvars.energetics.power.spring; %-spring power
Pd= OPvars.energetics.power.damper; %-damper power
Pn= Pm+Ps+Pd; %net power


%-cost
cTot= OPvars.cost.total;
cWrk= OPvars.cost.work;
cFRS= OPvars.cost.forceRate;




%% ----------------------------------------------------------------Plot PMR
if strcmpi(OPvars.name,'PMR')
nPh= length(OptSol.result.solution.phase); %-number of phases
Ph{1}= 1:length(OptSol.result.solution.phase(1).time); %-points from 1st phase
for iPh=2:nPh
    Ph{iPh}= (1:length(OptSol.result.solution.phase(iPh).time))+length(Ph{iPh-1}); %-points from each successive phase
end
nPh= nPh+1; %-add flight phase
Ph{nPh}= sum(cellfun('length',Ph)):length(t); %-points from extrapolated flight phase
%--------------------------------------------------------------------Energy
E_spr= (0.5/k)*Fs.^2; %spring        potential energy
E_grv= M*gM*yb;       %gravitational potential energy
E_kin= 0.5*M*(xbd.^2+ybd.^2);  %body kinetic energy
%---------------------------------------------------------------Spring Work
Ws_ac= cumtrapz(t,Ps); %accumulating spring work
Ws   = Ws_ac(end);     %net spring work
PsPos=Ps;   PsPos(find(PsPos<0))=0;   WsPos= trapz(t,PsPos); %-pos work
PsNeg=Ps;   PsNeg(find(PsNeg>0))=0;   WsNeg= trapz(t,PsNeg); %-neg work
%---------------------------------------------------------------Damper Work
Wd_ac= cumtrapz(t,Pd); %accumulating damper work
Wd   = Wd_ac(end);     %net damper work
PdPos=Pd;   PdPos(find(PdPos<0))=0;   WdPos= trapz(t,PdPos); %-pos work
PdNeg=Pd;   PdNeg(find(PdNeg>0))=0;   WdNeg= trapz(t,PdNeg); %-neg work
%---------------------------------------------------------------Muscle Work
Wm_ac= cumtrapz(t,Pm); %accumulating muscle work
Wm   = Wm_ac(end);     %net muscle work
Wm_nd= Wm/(M*gM*hb);   %^^^non-dim
PmPos=Pm;   PmPos(find(PmPos<0))=0;   WmPos= trapz(t,PmPos); %-pos work
PmNeg=Pm;   PmNeg(find(PmNeg>0))=0;   WmNeg= trapz(t,PmNeg); %-neg work
%------------------------------------------------------Absolute Muscle Work
absWm_ac= cumtrapz(t,abs(Pm)); %accumulating absolute value of muscle work
absWm   = absWm_ac(end);       %total accumulated absolute value of muscle work
absWm_nd= absWm/(M*gM*hb);     %^^^ non-dim
%------------------------------------------------------------Energy balance
E_sys= E_spr + E_grv + E_kin - Wm_ac - Wd_ac; %system energy (should be constant)


%--------------------------------------------------------------------------
%check outputs to make sure everything looks kosher
%--------------------------------------------------------------------------
Lmd_ch= (Lm(2:end)-Lm(1:end-1)) ./ (t(2:end)-t(1:end-1));
Lmd_ch= [Lmd_ch(1); Lmd_ch];

Pm_ch= Fm.*Lmd_ch;
Accum_ch= cumtrapz(t,Pm_ch);
Wm_ch= Accum_ch(end);

Accum_ch_abs= cumtrapz(t,abs(Pm_ch));
Wm_ch_abs= Accum_ch_abs(end);
Wm_ch_nd_abs= Wm_ch_abs/(M*gM*hb);
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------



%% ============================================================Plot Summary
if View.PlotSummary==1
FigTxt= ['Summary Plot: SPmat(' num2str(iSP) ') = ' num2str(Model.SPmat(iSP),'%g')];
figure('Name',FigTxt,'NumberTitle','Off','Color','w',...
                      'units','normalized','Position',[0.05 0.08 0.5 0.8]);
LSs= [{'-'} {':'}];   LW=2;   FS=12;
%-----------------------------------------------------------------Positions
subplot(3,2,1);ih=1;
% title('Position');
plot([0 xb(end)],[0 S*xb(end)],'k','LineWidth',3);hold on;
for iPh=1:nPh
h(ih)= plot(xb(Ph{iPh}),yb(Ph{iPh}),['b' LSs{iPh}],'LineWidth',LW);ih=ih+1; hold on;
h(ih)= plot(xm(Ph{iPh}),ym(Ph{iPh}),['r' LSs{iPh}],'LineWidth',LW);ih=ih+1;
end
h(ih)= plot([xf xb(  1)],[yf yb(  1)],'k','LineWidth',1);ih=ih+1;
h(ih)= plot([xf xb(pTO)],[yf yb(pTO)],'k','LineWidth',1);ih=ih+1;
h(ih)= plot(xf,yf,'mx');ih=ih+1;
xlim([0 ds]);
ylim([0 1.2*hb]);
axis equal;
xlabel('x (m)','FontSize',FS,'FontWeight','Bold');
ylabel('y (m)','FontSize',FS,'FontWeight','Bold');
legend(h(1:3),'y_b','y_m','leg','Location','SouthEast');
% set(gca,'FontSize',16,'FontWeight','Bold');
grid on;
%----------------------------------------------------------------Velocities
subplot(3,2,3);ih=1;
% title('Velocity');
for iPh=1:nPh
h(ih)= plot(t(  Ph{iPh}),Lld(  Ph{iPh}),['b' LSs{iPh}],'LineWidth',LW);ih=ih+1; hold on;
h(ih)= plot(t(  Ph{iPh}),Lmd(  Ph{iPh}),['r' LSs{iPh}],'LineWidth',LW);ih=ih+1;
end
legend('leg','act');
xlim([to tf]);
xlabel('Time (s)','FontSize',FS,'FontWeight','Bold');
ylabel('Vel (m/s)','FontSize',FS,'FontWeight','Bold');
grid on;
%--------------------------------------------------------------------Forces
subplot(3,2,5);ih=1; grid on;
for iPh=1:nPh
% h(ih)= plot(t(  Ph{iPh}),Fm(  Ph{iPh})/(M*gM),['k' LSs{iPh}],'LineWidth',LW+1);ih=ih+1; hold on;
% h(ih)= plot(t(  Ph{iPh}),Fs(  Ph{iPh})/(M*gM),LSs{iPh},'Color',[255  140    0]/255,'LineWidth',LW);ih=ih+1;
% h(ih)= plot(t(  Ph{iPh}),Fd(  Ph{iPh})/(M*gM),LSs{iPh},'Color',[ 75    0  130]/255,'LineWidth',LW);ih=ih+1;
h(ih)= plot(t(Ph{iPh}),Ry(  Ph{iPh})/(M*gM),['r' LSs{iPh}],'LineWidth',LW+1);ih=ih+1; hold on;
h(ih)= plot(t(Ph{iPh}),Rx(  Ph{iPh})/(M*gM),['b' LSs{iPh}],'LineWidth',LW+1);ih=ih+1; hold on;
end
legend('R_y','R_x');
xlim([to tf]);% ylim([0 5]);
ylabel('Force (BW)','FontSize',FS,'FontWeight','Bold');
xlabel('Time (s)','FontSize',FS,'FontWeight','Bold');
grid on;
%------------------------------------------------------------Energy Balance
subplot(3,2,2);ih=1; grid on;
for iPh=1:nPh
h(ih)= plot(t(Ph{iPh}), E_grv(Ph{iPh}),['b' LSs{iPh}],'LineWidth',LW);ih=ih+1; hold on;
h(ih)= plot(t(Ph{iPh}), E_kin(Ph{iPh}),['r' LSs{iPh}],'LineWidth',LW);ih=ih+1;
h(ih)= plot(t(Ph{iPh}), E_spr(Ph{iPh}),LSs{iPh},'Color',[255  140    0]/255,'LineWidth',LW);ih=ih+1;
h(ih)= plot(t(Ph{iPh}), Wd_ac(Ph{iPh}),LSs{iPh},'Color',[ 75    0  130]/255,'LineWidth',LW);ih=ih+1;
h(ih)= plot(t(Ph{iPh}), Wm_ac(Ph{iPh}),['m' LSs{iPh}],'LineWidth',LW  );ih=ih+1;
h(ih)= plot(t(Ph{iPh}), E_sys(Ph{iPh}),['k' LSs{iPh}],'LineWidth',LW+1);ih=ih+1;
end
legend('PE_g','KE','PE_s','W_d','W_m','PE+KE-W');
xlim([to tf]);% ylim([0 5]);
xlabel('Time (s)','FontSize',FS,'FontWeight','Bold');
ylabel('Energy (J)','FontSize',FS,'FontWeight','Bold');
title(['E_{sys}= ' num2str(E_sys(end),'%1.2f') ' J']);
grid on;
%-----------------------------------------------------------------Work Loop
subplot(3,2,4);ih=1;
for iPh=1:nPh
h(ih)= plot(   hb-Ll(Ph{iPh}),Fm(Ph{iPh})/(M*gM),['k' LSs{iPh}],'LineWidth',LW+1);ih=ih+1; hold on;
h(ih)= plot(   hm-Lm(Ph{iPh}),Fm(Ph{iPh})/(M*gM),LSs{iPh},'Color',[255  140    0]/255,'LineWidth',LW);ih=ih+1;
h(ih)= plot(dh_mb-Lt(Ph{iPh}),Fm(Ph{iPh})/(M*gM),LSs{iPh},'Color',[ 75    0  130]/255,'LineWidth',LW);ih=ih+1;
end
legend('leg','act','spr-dmp');
% xlim([to tf]);% ylim([0 5]);
ylabel('Force (BW)','FontSize',FS,'FontWeight','Bold');
xlabel('Length (m)','FontSize',FS,'FontWeight','Bold');
grid on;
%-----------------------------------------------------------------All Power
subplot(3,2,6);ih=1; grid on;
for iPh=1:nPh
h(ih)= plot(t(Ph{iPh}), Pn(Ph{iPh})/1000,['k' LSs{iPh}],'LineWidth',LW+1);ih=ih+1;hold on;
h(ih)= plot(t(Ph{iPh}), Pm(Ph{iPh})/1000,['b' LSs{iPh}],'LineWidth',LW  );ih=ih+1;
h(ih)= plot(t(Ph{iPh}), Ps(Ph{iPh})/1000,LSs{iPh},'Color',[255  140    0]/255,'LineWidth',LW);ih=ih+1;
h(ih)= plot(t(Ph{iPh}), Pd(Ph{iPh})/1000,LSs{iPh},'Color',[ 75    0  130]/255,'LineWidth',LW);ih=ih+1;
end
legend('leg','act','spr','dmp');
xlim([to tf]);% ylim([0 5]);
xlabel('Time (s)','FontSize',FS,'FontWeight','Bold');
ylabel('Power (W)','FontSize',FS,'FontWeight','Bold');
% title(['E_{sys}= ' num2str(E_sys(1),'%1.2f') ' J']);
grid on;
end












end %-end function


