function auxdata= PMR_InitGuess(ProbSet, auxdata)
%====================================================================Inputs
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> structure with problem parameters
%===================================================================Outputs
%  auxdata --> input auxdata updated with random initial guesses
%==========================================================================
%  This function randomly assigns initial guesses for model PMR



fnID= ProbSet.fnID;
hb  = auxdata.h__b;
g   = auxdata.grav;
k   = auxdata.sprC;
c   = auxdata.dmpC;
gM  = sqrt(sum(g.^2));
tau = sqrt(hb/gM); %[s] time non-dimensionalization factor


Rnd= rand;
auxdata.guess.phase.time     = [ 0; Rnd];
auxdata.guess.phase.state    = rand(2,7);
auxdata.guess.phase.control  = rand(2,3);
auxdata.guess.phase.integral = [rand(1,3) 0];
if contains(fnID,'_cFmdd')
auxdata.guess.phase.control  = [auxdata.guess.phase.control  rand(2,2)];
auxdata.guess.phase.integral = [auxdata.guess.phase.integral        0 ];
end
if contains(fnID,'_slackFd') && contains(fnID,'_noDmp')==0
if contains(fnID,'_cFmdd')
auxdata.guess.phase.state    =  [auxdata.guess.phase.state  rand(2,4)];
else
auxdata.guess.phase.state    =  [auxdata.guess.phase.state  rand(2,2)];
end
auxdata.guess.phase.control  =  [auxdata.guess.phase.control  rand(2,2)];
auxdata.guess.phase.integral =  [auxdata.guess.phase.integral zeros(1,1)];
end







auxdata.guess.parameter= [rand];
if k(1)~=k(2) && isnan(k(1))==0
    auxdata.guess.parameter= [auxdata.guess.parameter (k(1)+rand*diff(k))*(hb/(M*g))];
end
if c(1)~=c(2) && isnan(c(1))==0
    auxdata.guess.parameter= [auxdata.guess.parameter (c(1)+rand*diff(c))*(hb/(M*g*tau))];
end 













end