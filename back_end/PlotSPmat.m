function PlotOptSol(OPvars, Model, View)
%====================================================================Inputs
%  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
%  Model  --> structure with SPmat
%  View    --> structure indicating what plots to show
%==========================================================================
%  This function plots basic outputs from OptSol versus var in SPmat


if isfield(View,'Preset'); if ~isempty(View.Preset); return; end; end %-bypass if preset


SPmat= Model.SPmat;
if length(SPmat)==1;   return;   end

%% ==================================================Plot Cost versus SPmat
if View.Cost==1
cTot= nan(1,length(SPmat)); cWrk= nan(1,length(SPmat)); cFRS= nan(1,length(SPmat)); %-initiate vars
for iSP=1:length(SPmat)
cTot(iSP)= OPvars(iSP).cost.total;
cWrk(iSP)= OPvars(iSP).cost.work;
cFRS(iSP)= OPvars(iSP).cost.forceRate;
end


figure('Color','w');
plot(SPmat, cTot,'ks-');hold on;
plot(SPmat, cWrk,'bd-');
plot(SPmat, cFRS,'mo-');


ylim([0 1.3*max(cTot)]);

xlabel('SPmat');
ylabel('Model Cost');
end


%% ==================================================Plot Cost versus SPmat
if View.DutyFactor==1
DF= nan(1,length(SPmat));
for iSP=1:length(SPmat)
DF(iSP)= OPvars(iSP).time.dutyFactor;
end

figure('Color','w');
plot(SPmat, DF,'ks-');

ylim([0 1]);grid on;

xlabel('SPmat');
ylabel('Duty Factor');
end









end %-end function