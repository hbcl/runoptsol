function [Model, Response]= OverwriteCheck(Model, ProbSet, Response)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%  Model    --> structure with model parameters
%  Response --> response to prompt
%==========================================================================
%  This function checks for a preexisting optimal solution file

iSP=    ProbSet.iSP;
fnID=   ProbSet.fnID;
Home=   ProbSet.Home;
SaveOP= ProbSet.SaveOP;
SaveName= Model.SaveName;

if SaveOP==1
   if ispc; s='\'; else s='/'; end
   if isempty(SaveName)
   fsave= [Home s 'OptSols' s fnID '.mat'];
   else
   if strcmpi(SaveName(end-3:end),'.mat')
   fsave= [Home s 'OptSols' s SaveName];
   else
   fsave= [Home s 'OptSols' s SaveName '.mat'];
   end
   end
   
   if exist(fsave,'file')==2
    if isempty(Response)
     answer= questdlg(['File already exists for optimal solution, named: "',...
                        fsave(findstr(fsave,[s 'OptSols' s])+9:end) '". ',...
                       'Please select an option below. Do not overwrite ',...
                       'for multiple optimizations (i.e. length(SPmat)>1)'],'File name already exists!',...
                       'Overwrite file','Append/Add count to file name','Skip optimization','Skip optimization');
    
    %-handle response
    switch answer
        
    case 'Overwrite file'
        Response= 1;
        delete(fsave);
        
    case 'Append/Add count to file name'
        if isempty(str2num(fsave(end-6:end-4))) && exist(fsave,'file')==0
        Model.SaveName= [fsave(findstr(fsave,[s 'OptSols' s])+9:end-4) '_001.mat'];
        elseif isempty(str2num(fsave(end-6:end-4)))==0 & fsave(end-7)=='_'
        while exist(fsave,'file')==2
        AppendNum= str2num(fsave(end-6:end-4))+1;
        AppendNum= [repelem('0',3-length(num2str(AppendNum))) num2str(AppendNum)];
        Model.SaveName= [fsave(findstr(fsave,[s 'OptSols' s])+9:end-8) '_' AppendNum '.mat']; 
        fsave= [Home s 'OptSols' s Model.SaveName];
        end
        end
        disp(['File name appended to: "' Model.SaveName '"'])
        Response= 2;
        
    case 'Skip optimization'
        Response= 3;

    end %-switch
    if isempty(answer); Response= 4; end %-if user exits window w/o selection
       
    
    
      
    else %-if Response ~empty?
    %-handle response
        
    if Response==1 %-Overwrite file
        delete(fsave);
        
    elseif Response==2 %-Append/Add count to file name
        if isempty(str2num(fsave(end-6:end-4))) && exist(fsave,'file')==0
        Model.SaveName= [fsave(findstr(fsave,[s 'OptSols' s])+9:end-4) '_001.mat'];
        elseif isempty(str2num(fsave(end-6:end-4)))==0 & fsave(end-7)=='_'
        while exist(fsave,'file')==2
        AppendNum= str2num(fsave(end-6:end-4))+1;
        AppendNum= [repelem('0',3-length(num2str(AppendNum))) num2str(AppendNum)];
        Model.SaveName= [fsave(findstr(fsave,[s 'OptSols' s])+9:end-8) '_' AppendNum '.mat']; 
        fsave= [Home s 'OptSols' s Model.SaveName];
        end
        end
        disp(['File name appended to: "' Model.SaveName '"'])
        Response= 2;
        
    elseif Response==3 %-Skip optimization
    end %-Response==?

    end %-Response empty?
    
    
    
   else %-file exist?
       Response= [];
   end  %-file exist?
   
   
    
end %-save?







end %-end function