function [yf,w] = DriftFilter(x,y,DAQr,w)
%% This function attempts to remove noise from a given signal by calculating
%  the mean value for successive chunks of data centered around points
%  (x,y) and bounded by the width (w). Only 2D data currently works. The 
%  number of data points in a given chunk is equal to 2*w+1. DAQr is the
%  data acquisition rate.

%  ***NOTE*** ESSENTIALLY, THIS IS A MOVING AVERAGE FILTER
%  ***NOTE*** If there is no "w" input, the function will allow the user to
%             explore w values.


if length(x)>ceil(40*DAQr)
    e = ceil(40*DAQr);
else
    e = length(x);
end

if nargin==3
f1=figure;
huf = plot(x(1:e),y(1:e),'b'); grid on; hold on; set(huf,'LineWidth',2);
xlabel('X','FontSize',14); ylabel('Signal','FontSize',14);
title('Drift Filter (Assuming a Symetrical Waveform)','FontSize',28);
hstop = uicontrol('Style', 'PushButton', 'String', 'Finished', ...
              'Callback', 'delete(gcbo)');
hslid = uicontrol('style','slider','units','pixel','position',[200 10 300 20],'Min',1,'Max',10*DAQr,'Value',1,'SliderStep',[0.001 0.001]);
drawnow;
elseif nargin==4
hstop=plot(nan); delete(hstop);
end


hf = plot(nan); ht = plot(nan); hD = plot(nan);
while ishandle(hstop)
% Check to make sure there is enough data supported by width (w)
  w=floor(get(hslid,'Value')); wa = 2*w+1; wt = wa/DAQr;

if length(x)<2*w
    fprintf('!!!ERROR!!! \n Your width (w) is less than twice the number of data points in x and y.');
else

for i=1:length(y)
    if i-w<1 && i+w>length(y)
        yD(i) = (mean(y(1:length(y))) + median(y(1:length(y))))/2;
    elseif i-w<1 && i+w<=length(y)
        yD(i) = (mean(y(1:i+w)) + median(y(1:i+w)))/2;
    elseif i-w>=1 && i+w>length(y)
        yD(i) = (mean(y(i-w:length(y))) + median(y(i-w:length(y))))/2;
    elseif i-w>=1 && i+w<=length(y)
        yD(i) = (mean(y(i-w:i+w)) + median(y(i-w:i+w)))/2;
    end
end
end


check = fliplr(size(yD));
if size(y,1) == check(1) && size(y,2) == check(2)
    yD = yD';
end

yf = y-yD;      % Subtract Drift Values from Original Signal

  disp(datestr(now, 0));
  delete(hf); delete(hD); delete(ht);
  hD = plot(x(1:e),yD(1:e),'r'); set(hD,'LineWidth',4);
  hf = plot(x(1:e),yf(1:e),'c'); set(hf,'LineWidth',2);
  txtw = sprintf('w = %1.0f = %1.2fs',wa,wt);
  ht = uicontrol('Style', 'text',...
                 'String', txtw,... %replace something with the text you want
                 'Units','normalized',...
                 'FontSize',14,...
                 'Position', [0.8 0.005 0.05 0.05]); 
  legend('Unfiltered Signal','Drift Average','Filtered Signal');
  pause(0.25); clc;
end
clc;
% close;

if nargin==3
endfilter = sprintf('You have stopped on a width value of w = %1.0f = %1.2fs\n\n\n\n\n\n\n\n\n\n',wa,wt);
fprintf(endfilter);
elseif nargin==4
      % Check to make sure there is enough data supported by width (w)
      wa = 2*w+1;

if length(x)<2*w
    fprintf('!!!ERROR!!! \n Your width (w) is less than twice the number of data points in x and y.');
else
for i=1:length(y)
    if i-w<1 && i+w>length(y)
        yD(i) = (mean(y(1:length(y))) + median(y(1:length(y))))/2;
    elseif i-w<1 && i+w<=length(y)
        yD(i) = (mean(y(1:i+w)) + median(y(1:i+w)))/2;
    elseif i-w>=1 && i+w>length(y)
        yD(i) = (mean(y(i-w:length(y))) + median(y(i-w:length(y))))/2;
    elseif i-w>=1 && i+w<=length(y)
        yD(i) = (mean(y(i-w:i+w)) + median(y(i-w:i+w)))/2;
    end
end
check = fliplr(size(yD));
if size(y,1) == check(1) && size(y,2) == check(2)
    yD = yD';
end

yf = y-yD;      % Subtract Drift Values from Original Signal
end  

f2=figure;
huf = plot(x(1:e),y(1:e),'b'); grid on; hold on; set(huf,'LineWidth',2);
xlabel('X','FontSize',14); ylabel('Signal','FontSize',14);
title('Drift Filter (Assuming a Symmetrical Waveform)','FontSize',28);
hD = plot(x(1:e),yD(1:e),'r'); set(hD,'LineWidth',4);
hf = plot(x(1:e),yf(1:e),'c'); set(hf,'LineWidth',2);
legend('Unfiltered Signal','Drift Average','Filtered Signal');
end

% pause(0.1);
% close;