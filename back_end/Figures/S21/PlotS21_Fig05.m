function PlotS21_Fig05(OPvars, OptSol, SPps)
%====================================================================Inputs
%  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
%  OptSol --> optimal solution
%  SPps   --> structure with model parameters from all OptSols in preset
%==========================================================================
%  This function plots Fig. 5 from presets 'S21', a manuscript on running
%  mechanics and energetics using the 'PMR' models.
%  Schroeder & Kuo, 2021 (planning to submit to PLoS Computational Biology?)



%% ===========================================================Define Params
k_ref= 27370.46501693961;

kr= SPps.SpringConstant/k_ref; %-spring constant (relative)
k = SPps.SpringConstant;       %-spring constant
Sp= SPps.Speed; %-speed
for iSP=1:length(SPps.Name)
d(iSP)= OptSol(iSP).result.setup.auxdata.stpL; %-step length
end
f = Sp/d; %-step frequency
M = SPps.BodyMass; %-body mass
L = SPps.MaxLegLength; %-max leg length
gM= SPps.RelativeGravity*9.81; %-gravitational acceleration
S = SPps.GroundSlope;  %-ground slope
DR= SPps.DampingRatio; %-damping ratio
CF= SPps.CollisionFraction; %-collision fraction
cR= SPps.ForceRateCoef; %-force rate coefficient


%% ===================================================Define Vars for Plots
for iSP=1:length(SPps.Name)
%-time
  t{iSP}= OPvars(iSP).time.time; %-time
  T(iSP)= t{iSP}(end); %-step  time
 to(iSP)= t{iSP}(1);   %-start time
 tf(iSP)= t{iSP}(end); %-end   time
 DF(iSP)= OPvars(iSP).time.dutyFactor;  %-duty factor
 Tc(iSP)= OPvars(iSP).time.contactTime; %-ground contact time
 Tf(iSP)= OPvars(iSP).time.flightTime;  %-flight time
pTO(iSP)= OPvars(iSP).time.events(1,2); %-point of take off

%-kinematics
Th{iSP}= OPvars(iSP).kinematics.pos.legAngle;  %-leg angle, CCW from +--> (global)
Ll{iSP}= OPvars(iSP).kinematics.pos.legLength; %-leg length
if strcmpi(OPvars(iSP).name,'PMR')
Lt{iSP}= OPvars(iSP).kinematics.pos.springLength;
Lm{iSP}= OPvars(iSP).kinematics.pos.actuatorLength;
end
xb{iSP}= OPvars(iSP).kinematics.pos.body(:,1); %-body hor pos
yb{iSP}= OPvars(iSP).kinematics.pos.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
xm{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,1); %-actuator end away from ground (hor)
ym{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,2); %-actuator end away from ground (vrt)
end
 xf(iSP)= OPvars(iSP).kinematics.pos.foot(:,1);  %-foot hor pos
 yf(iSP)= OPvars(iSP).kinematics.pos.foot(:,2);  %-foot vrt pos
Thd{iSP}= OPvars(iSP).kinematics.vel.legAngle;  %-leg angular velocity, CCW +
Lld{iSP}= OPvars(iSP).kinematics.vel.legLength; %-leg velocity
xbd{iSP}= OPvars(iSP).kinematics.vel.body(:,1); %-body hor pos
ybd{iSP}= OPvars(iSP).kinematics.vel.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
Ltd{iSP}= OPvars(iSP).kinematics.vel.springLength;   %-spring vel
Lmd{iSP}= OPvars(iSP).kinematics.vel.actuatorLength; %-actuator vel
end
Lldd{iSP}= OPvars(iSP).kinematics.acc.leglength; %-leg acc
xbdd{iSP}= OPvars(iSP).kinematics.acc.body(:,1); %-hor body acc
ybdd{iSP}= OPvars(iSP).kinematics.acc.body(:,2); %-vrt body acc
if strcmpi(OPvars(iSP).name,'PMR');      Lmdd{iSP}= OPvars(iSP).kinematics.acc.actuatorLength; else;  Lmdd{iSP}=nan; end
if isfield(OPvars(iSP).kinetics,'jrk'); Llddd{iSP}= OPvars(iSP).kinematics.jrk.legLength;      else; Llddd{iSP}=nan; end
if strcmpi(OPvars(iSP).name,'PMR');     Lmddd{iSP}= OPvars(iSP).kinematics.jrk.actuatorLength; else; Lmddd{iSP}=nan; end


%-kinetics
 Rx{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,1); %-hor GRF
 Ry{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,2); %-vrt GRF
 Fm{iSP}=  OPvars(iSP).kinetics.actuatorForce;  %-Actuator force
Fmd{iSP}=  OPvars(iSP).kinetics.actuatorForce_ddt1; %-d/dt actuator force
if isfield(OPvars(iSP).kinetics,'actuatorForce_ddt2'); Fmdd{iSP}= OPvars(iSP).kinetics.actuatorForce_ddt2; else; Fmdd{iSP}=nan; end %-d2/dt2 actuator force
if isfield(OPvars(iSP).kinetics,'springForce');        Fs{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fs{iSP}  =nan; end %-Spring force
if isfield(OPvars(iSP).kinetics,'damperForce');        Fd{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fd{iSP}  =nan; end %-Damper force


%-energetics
negMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(1); %-neg actuator work
posMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(2); %-pos actuator work
absMscWrk(iSP)= posMscWrk(iSP)-negMscWrk(iSP); %-abs actuator work
netMscWrk(iSP)= posMscWrk(iSP)+negMscWrk(iSP); %-net actuator work
if strcmpi(OPvars(iSP).name,'PMR')
negSprWrk(iSP)= OPvars(iSP).energetics.work.spring(1); %-neg spring work
posSprWrk(iSP)= OPvars(iSP).energetics.work.spring(2); %-pos spring work
negDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(1); %-neg damper work
posDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(2); %-pos damper work
end
Pm{iSP}= OPvars(iSP).energetics.power.actuator; %-actuator power
if strcmpi(OPvars(iSP).name,'PMR')
Ps{iSP}= OPvars(iSP).energetics.power.spring; %-spring power
Pd{iSP}= OPvars(iSP).energetics.power.damper; %-damper power
Pn{iSP}= Pm{iSP}+Ps{iSP}+Pd{iSP}; %-net power
HysLoss(iSP)= -trapz(t{iSP},Pd{iSP}); %[J] hysteresis loss magnitude
elseif strcmpi(OPvars(iSP).name,'srinR')
Pn{iSP}= Pm{iSP}; %-net power
end

vbf(iSP)= sqrt(xbd{iSP}(end)^2+ybd{iSP}(end)^2); %-final   body velocity before TD
vbi(iSP)= sqrt(xbd{iSP}( 1 )^2+ybd{iSP}( 1 )^2); %-initial body velocity after  TD
dKE(iSP)= 0.5*M(iSP)*(vbi(iSP)^2-vbf(iSP)^2); %-change in KE from final to initial
ColLoss(iSP)= -dKE(iSP); %[J] collision loss magnitude

%-cost
cTot(iSP)= OPvars(iSP).cost.total;
cWrk(iSP)= OPvars(iSP).cost.work;
cFRS(iSP)= OPvars(iSP).cost.forceRate;

end %-end defining vars



%% ==================================================Choose OptSols to Plot
%-all Actauted Spring-mass OptSols (compliant/stiff)
krDsp= [0.5 1.0 2.0 4.0]*k_ref; %-stiffness for display
SpDsp= [2.5 3.0 3.5]; %-speed for display

for iC=1:length(SpDsp) %-iterate speeds
Dsp{iC}= find(strcmpi(SPps.Name,'PMR') & strcmpi(SPps.StepLength,'G06') &...
              SPps.ForceRateCoef==0   & SPps.BodyMass==70              &...
               SPps.DampingRatio==0.1 & SPps.CollisionFraction==0.03   &...
                SPps.GroundSlope==0   & SPps.RelativeGravity==1        &...
                SPps.Speed==SpDsp(iC) & SPps.MaxLegLength==0.9         &...
                                      ismember(SPps.SpringConstant,krDsp));
[U, I]= sort(kr(Dsp{iC}),'ascend');   Dsp{iC}= Dsp{iC}(I);      %-ascending order
[unqVal unqInd]= unique(kr(Dsp{iC})); Dsp{iC}= Dsp{iC}(unqInd); %-eliminate duplicates
end


%% =============================================================Plot Figure
nR= 3; %-# rows
nC= 3; %-# cols


figure('Name','Fig. 5: Actuated Work/Power vs. Stiffness/Speed',...
           'Color','w','NumberTitle','off','Units','Normalized',...
           'Position',[0.1 0.1 0.5 0.6]);

for iC=1:length(Dsp)
%--------------------------------------------------------------Trajectories
subplot(nR,nC,[iC nC+iC]);
NDF= 100/(M(Dsp{iC}(1))*gM(Dsp{iC}(1))*L(Dsp{iC}(1)));

semilogx(k(Dsp{iC})/1000,absMscWrk(Dsp{iC})*NDF,'ko-','LineWidth',2,'MarkerFaceColor','w');hold on;
semilogx(k(Dsp{iC})/1000,  HysLoss(Dsp{iC})*NDF,'bo-','LineWidth',1,'MarkerFaceColor','w');hold on;
semilogx(k(Dsp{iC})/1000,  ColLoss(Dsp{iC})*NDF,'ro-','LineWidth',1,'MarkerFaceColor','w');hold on;
xlim([7 200]);   ylim([0 10]);
xticks([10 100]);   box off;
if iC==1;   ylabel('Model Work per Step (x10^{-2})');    end
title([num2str(Sp(Dsp{iC}(1)),'%1.1f') ' m s^{-1}']);


%--------------------------------------------------------------Power Insets
subplot(nR,nC,2*nC+iC);   NDF= 1000;

%-create axes
semilogx(nan);
xlim([7 200]);   xticks([10 100]);
yticks([]);

box off;
if iC==2;   xlabel('Spring Stiffness, k (kN m^{-1})');   end
if iC==1;   ylabel('Power, P(t)');                       end

P(iC,:)= get(gca,'Position');
end %-iterate cols


%-plot power curves
set(gcf,'Units','Normalized');   SC=0.15;
for iC=1:length(Dsp)
%-plot power for compliant spring
axes('Position',[P(iC,1)+0.01 P(iC,2)+0.03 0.15 0.2]);
plot([0 t{Dsp{iC}(1)}(pTO(Dsp{iC}(1)))+0.01],[0 0],'k');hold on; %-zero line
plot([0 0],[0 -ColLoss(Dsp{iC}(1))*SC],'r','LineWidth',2);hold on; %-Collision impulse
plot(0,-ColLoss(Dsp{iC}(1))*SC,'rv','MarkerFaceColor','r');hold on;

fill([t{Dsp{iC}(1)}(1); t{Dsp{iC}(1)}(1:pTO(Dsp{iC}(1)))],...
     [0; Pn{Dsp{iC}(1)}(1:pTO(Dsp{iC}(1)))]/NDF,[1 0.62 0],'LineStyle','None');hold on; %-fill power with orange
fill([t{Dsp{iC}(1)}(1); t{Dsp{iC}(1)}(1:pTO(Dsp{iC}(1)))],...
     [0; Pm{Dsp{iC}(1)}(1:pTO(Dsp{iC}(1)))]/NDF,'b','LineStyle','None');hold on; %-actuator power
plot([t{Dsp{iC}(1)}(1); t{Dsp{iC}(1)}(1:pTO(Dsp{iC}(1)))],...
     [0; Pn{Dsp{iC}(1)}(1:pTO(Dsp{iC}(1)))]/NDF,'k','LineWidth',1);hold on; %-net leg power
axis off;
xlim([0 T(Dsp{iC}(1))]); ylim([-5 5]);

%-plot time unit legend
if iC==2
plot([0 0.1],[2 2],'k','LineWidth',1); 
text(0.05,2.2,'0.1 s','VerticalAlignment','Bottom','HorizontalAlignment','Center');
end


%-plot power for stiff spring
axes('Position',[P(iC,1)+0.15 P(iC,2)+0.03 0.15 0.2]);
plot([0 t{Dsp{iC}(4)}(pTO(Dsp{iC}(4)))+0.01],[0 0],'k');hold on; %-zero line
plot([0 0],[0 -ColLoss(Dsp{iC}(4))*SC],'r','LineWidth',2);hold on; %-Collision impulse
plot(0,-ColLoss(Dsp{iC}(4))*SC,'rv','MarkerFaceColor','r');hold on;

fill([0;  t{Dsp{iC}(4)}(1:pTO(Dsp{iC}(4)))],...
     [0; Pn{Dsp{iC}(4)}(1:pTO(Dsp{iC}(4)))]/NDF,[1 0.62 0],'LineStyle','None');hold on; %-fill power with orange
fill([0;  t{Dsp{iC}(4)}(1:pTO(Dsp{iC}(4)))],...
     [0; Pm{Dsp{iC}(4)}(1:pTO(Dsp{iC}(4)))]/NDF,'b','LineStyle','None');hold on; %-actuator power
plot([0;  t{Dsp{iC}(4)}(1:pTO(Dsp{iC}(4)))],...
     [0; Pn{Dsp{iC}(4)}(1:pTO(Dsp{iC}(4)))]/NDF,'k','LineWidth',1);hold on; %-net leg power
axis off;
xlim([0 T(Dsp{iC}(1))]); ylim([-5 5]);


%-plot power unit legend
if iC==3
plot((t{Dsp{iC}(4)}(pTO(Dsp{iC}(4)))+0.1)*[1 1],[-2.5 2.5],'k','LineWidth',1); 
text( t{Dsp{iC}(4)}(pTO(Dsp{iC}(4)))+0.1,2.7,'5 kW','VerticalAlignment','Bottom','HorizontalAlignment','Center');
end

end %-iterate cols









end %-end function