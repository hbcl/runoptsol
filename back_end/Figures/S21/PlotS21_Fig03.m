function PlotS21_Fig03(OPvars, OptSol, SPps)
%====================================================================Inputs
%  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
%  OptSol --> optimal solution
%  SPps   --> structure with model parameters from all OptSols in preset
%==========================================================================
%  This function plots Fig. 3 from presets 'S21', a manuscript on running
%  mechanics and energetics using the 'PMR' and 'srinR' models.
%  Schroeder & Kuo, 2021 (planning to submit to PLoS Computational Biology?)




%% ===========================================================Define Params
k_ref= 27370.46501693961;

kr= SPps.SpringConstant/k_ref; %-spring constant
Sp= SPps.Speed; %-speed
for iSP=1:length(SPps.Name)
d(iSP)= OptSol(iSP).result.setup.auxdata.stpL; %-step length
end
f = Sp/d; %-step frequency
M = SPps.BodyMass; %-body mass
L = SPps.MaxLegLength; %-max leg length
gM= SPps.RelativeGravity*9.81; %-gravitational acceleration
S = SPps.GroundSlope;  %-ground slope
DR= SPps.DampingRatio; %-damping ratio
CF= SPps.CollisionFraction; %-collision fraction
cR= SPps.ForceRateCoef; %-force rate coefficient



%% ===================================================Define Vars for Plots
for iSP=1:length(SPps.Name)
%-time
  t{iSP}= OPvars(iSP).time.time; %-time
  T(iSP)= t{iSP}(end); %-step  time
 to(iSP)= t{iSP}(1);   %-start time
 tf(iSP)= t{iSP}(end); %-end   time
 DF(iSP)= OPvars(iSP).time.dutyFactor;  %-duty factor
 Tc(iSP)= OPvars(iSP).time.contactTime; %-ground contact time
 Tf(iSP)= OPvars(iSP).time.flightTime;  %-flight time
pTO(iSP)= OPvars(iSP).time.events(1,2); %-point of take off

%-kinematics
Th{iSP}= OPvars(iSP).kinematics.pos.legAngle;  %-leg angle, CCW from +--> (global)
Ll{iSP}= OPvars(iSP).kinematics.pos.legLength; %-leg length
if strcmpi(OPvars(iSP).name,'PMR')
Lt{iSP}= OPvars(iSP).kinematics.pos.springLength;
Lm{iSP}= OPvars(iSP).kinematics.pos.actuatorLength;
end
xb{iSP}= OPvars(iSP).kinematics.pos.body(:,1); %-body hor pos
yb{iSP}= OPvars(iSP).kinematics.pos.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
xm{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,1); %-actuator end away from ground (hor)
ym{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,2); %-actuator end away from ground (vrt)
end
 xf(iSP)= OPvars(iSP).kinematics.pos.foot(:,1);  %-foot hor pos
 yf(iSP)= OPvars(iSP).kinematics.pos.foot(:,2);  %-foot vrt pos
Thd{iSP}= OPvars(iSP).kinematics.vel.legAngle;  %-leg angular velocity, CCW +
Lld{iSP}= OPvars(iSP).kinematics.vel.legLength; %-leg velocity
xbd{iSP}= OPvars(iSP).kinematics.vel.body(:,1); %-body hor pos
ybd{iSP}= OPvars(iSP).kinematics.vel.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
Ltd{iSP}= OPvars(iSP).kinematics.vel.springLength;   %-spring vel
Lmd{iSP}= OPvars(iSP).kinematics.vel.actuatorLength; %-actuator vel
end
Lldd{iSP}= OPvars(iSP).kinematics.acc.leglength; %-leg acc
xbdd{iSP}= OPvars(iSP).kinematics.acc.body(:,1); %-hor body acc
ybdd{iSP}= OPvars(iSP).kinematics.acc.body(:,2); %-vrt body acc
if strcmpi(OPvars(iSP).name,'PMR');      Lmdd{iSP}= OPvars(iSP).kinematics.acc.actuatorLength; else;  Lmdd{iSP}=nan; end
if isfield(OPvars(iSP).kinetics,'jrk'); Llddd{iSP}= OPvars(iSP).kinematics.jrk.legLength;      else; Llddd{iSP}=nan; end
if strcmpi(OPvars(iSP).name,'PMR');     Lmddd{iSP}= OPvars(iSP).kinematics.jrk.actuatorLength; else; Lmddd{iSP}=nan; end


%-kinetics
 Rx{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,1); %-hor GRF
 Ry{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,2); %-vrt GRF
 Fm{iSP}=  OPvars(iSP).kinetics.actuatorForce;  %-Actuator force
Fmd{iSP}=  OPvars(iSP).kinetics.actuatorForce_ddt1; %-d/dt actuator force
if isfield(OPvars(iSP).kinetics,'actuatorForce_ddt2'); Fmdd{iSP}= OPvars(iSP).kinetics.actuatorForce_ddt2; else; Fmdd{iSP}=nan; end %-d2/dt2 actuator force
if isfield(OPvars(iSP).kinetics,'springForce');        Fs{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fs{iSP}  =nan; end %-Spring force
if isfield(OPvars(iSP).kinetics,'damperForce');        Fd{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fd{iSP}  =nan; end %-Damper force


%-energetics
negMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(1); %-neg actuator work
posMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(2); %-pos actuator work
absMscWrk(iSP)= posMscWrk(iSP)-negMscWrk(iSP); %-abs actuator work
netMscWrk(iSP)= posMscWrk(iSP)+negMscWrk(iSP); %-net actuator work
if strcmpi(OPvars(iSP).name,'PMR')
negSprWrk(iSP)= OPvars(iSP).energetics.work.spring(1); %-neg spring work
posSprWrk(iSP)= OPvars(iSP).energetics.work.spring(2); %-pos spring work
negDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(1); %-neg damper work
posDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(2); %-pos damper work
end
Pm{iSP}= OPvars(iSP).energetics.power.actuator; %-actuator power
if strcmpi(OPvars(iSP).name,'PMR')
Ps{iSP}= OPvars(iSP).energetics.power.spring; %-spring power
Pd{iSP}= OPvars(iSP).energetics.power.damper; %-damper power
Pn{iSP}= Pm{iSP}+Ps{iSP}+Pd{iSP}; %-net power
elseif strcmpi(OPvars(iSP).name,'srinR')
Pn{iSP}= Pm{iSP}; %-net power
end


%-cost
cTot(iSP)= OPvars(iSP).cost.total;
cWrk(iSP)= OPvars(iSP).cost.work;
cFRS(iSP)= OPvars(iSP).cost.forceRate;



% =================================Kinematics of Ballistics (inf stiff leg)
 xB{iSP}= Sp(iSP).*t{iSP}; %-hor pos
vTO(iSP)= gM(iSP)*T(iSP)/2; %-vrt takeoff vel
 yB{iSP}= L(iSP) + vTO(iSP).*t{iSP} - 0.5*gM(iSP)*t{iSP}.^2; %-vrt pos


end %-end defining vars



%% ==================================================Choose OptSols to Plot
%-all Spring-mass for plots
krDsp= [0.18 0.45 1.33 4.45]*k_ref; %-stiffness for display
Dsp{1}= find(strcmpi(SPps.Name,'PMR') & strcmpi(SPps.StepLength,'G06') &...
              SPps.ForceRateCoef==0   & SPps.BodyMass==70              &...
               SPps.DampingRatio==0   & SPps.CollisionFraction==0      &...
                SPps.GroundSlope==0   & SPps.RelativeGravity==1        &...
                      SPps.Speed==3.5 & SPps.MaxLegLength==0.9         &...
                                      ismember(SPps.SpringConstant,krDsp));
[unqVal unqInd]= unique(kr(Dsp{1})); Dsp{1}= Dsp{1}(unqInd); %-eliminate duplicates
[U, I]= sort(kr(Dsp{1}),'ascend');   Dsp{1}= Dsp{1}(I);      %-ascending order


%-all Actuator-mass for plots
cRDsp= 10.^-[2:5]; %-force rate coef for display
Dsp{2}= find(strcmpi(SPps.Name,'srinR') & strcmpi(SPps.StepLength,'G06') &...
               SPps.SpringConstant==inf & SPps.BodyMass==70              &...
                 SPps.DampingRatio==0   & SPps.CollisionFraction==0      &...
                  SPps.GroundSlope==0   & SPps.RelativeGravity==1        &...
                        SPps.Speed==3.5 & SPps.MaxLegLength==0.9         &...
                                       ismember(SPps.ForceRateCoef,cRDsp));
[unqVal unqInd]= unique(cR(Dsp{2}));  Dsp{2}= Dsp{2}(unqInd); %-eliminate duplicates
[U, I]= sort(cR(Dsp{2}),'descend');   Dsp{2}= Dsp{2}(I);      %-descending order




%% =============================================================Plot Figure
LW= [3 2 1 2 3];
CL= [linspace(0,1,5)' zeros(length(Dsp{1})+1,1) linspace(1,0,5)'];


figure('Name','Fig. 3: Spring-mass vs. Actuator-only','Color','w',...
       'NumberTitle','off','Units','Normalized','Position',[0.1 0.1 0.5 0.6]);

for iR=1:length(Dsp)
%--------------------------------------------------------------Trajectories
subplot(2,4,4*(iR-1)+1);

%-plot ground
plot(0.5*d(Dsp{iR}(1)).*[-1 1],[0 0],'k','LineWidth',3);hold on;

%-plot compliant/stiff leg angles
iDsp=Dsp{iR}(1);   i=find(iDsp==Dsp{iR});
plot([0 xb{iDsp}( 1 )-xf(iDsp)],[0 yb{iDsp}( 1 )],'Color',CL(i,:),'LineWidth',1); %-initial
plot([0 xb{iDsp}(end)-xf(iDsp)],[0 yb{iDsp}(end)],'Color',CL(i,:),'LineWidth',1); %-final
iDsp=length(Dsp{iR})+1;   i=find(iDsp==Dsp{iR});
plot([0 0],[0 L(Dsp{iR}(1))],'Color',CL(iDsp,:),'LineWidth',1);hold on;
plot(xB{Dsp{iR}(1)}(1:find(max(yB{Dsp{iR}(1)})==yB{Dsp{iR}(1)})),...
     yB{Dsp{iR}(1)}(1:find(max(yB{Dsp{iR}(1)})==yB{Dsp{iR}(1)})),...
                      ':','Color',CL(iDsp,:),'LineWidth',LW(iDsp));hold on;
plot(xB{Dsp{iR}(1)}(find(max(yB{Dsp{iR}(1)})==yB{Dsp{iR}(1)}):end)-...
     xB{Dsp{iR}(1)}(find(max(yB{Dsp{iR}(1)})==yB{Dsp{iR}(1)}))*2  ,...
     yB{Dsp{iR}(1)}(find(max(yB{Dsp{iR}(1)})==yB{Dsp{iR}(1)}):end),...
                      ':','Color',CL(iDsp,:),'LineWidth',LW(iDsp));hold on;


%-plot trajectories
for iDsp=Dsp{iR}
i=find(iDsp==Dsp{iR});
plot(xb{iDsp}(find(yb{iDsp}==max(yb{iDsp})):end)-xb{iDsp}(end)-xf(iDsp), yb{iDsp}(find(yb{iDsp}==max(yb{iDsp})):end),':','Color',CL(i,:),'LineWidth',LW(i));hold on;
plot(xb{iDsp}(  1:pTO(iDsp))-xf(iDsp), yb{iDsp}(  1:pTO(iDsp)),'-','Color',CL(i,:),'LineWidth',LW(i));hold on;
plot(xb{iDsp}(pTO(iDsp):find(yb{iDsp}==max(yb{iDsp})))-xf(iDsp), yb{iDsp}(pTO(iDsp):find(yb{iDsp}==max(yb{iDsp}))),':','Color',CL(i,:),'LineWidth',LW(i));hold on;
end
axis equal;   axis off;
xticks('');yticks('');box off;
if iR==1
txt=text(xb{iDsp}( 1 )-xf(iDsp)-0.6,L(iDsp)/2-0.4,'Spring-mass');
elseif iR==2
txt=text(xb{iDsp}( 1 )-xf(iDsp)-0.6,L(iDsp)/2-0.4,'Actuator-only');
end
set(txt,'Rotation',90);
%----------------------------------------------------------------------GRFs
subplot(2,4,4*(iR-1)+2);   NDF= M(Dsp{iR}(1))*gM(Dsp{iR}(1));
plot([-0.03 T(1)],[0 0],'k');hold on; %-zero line
plot([0 0],[0 7],'r','LineWidth',2);hold on; %-impulse
plot(0,7,'r^','MarkerFaceColor','r');hold on;
for iDsp=Dsp{iR}
i=find(iDsp==Dsp{iR});
plot(t{iDsp}(  1:pTO(iDsp)), Ry{iDsp}(  1:pTO(iDsp))/NDF,'-','Color',CL(i,:),'LineWidth',LW(i));hold on;
plot(t{iDsp}(pTO(iDsp):end), Ry{iDsp}(pTO(iDsp):end)/NDF,':','Color',CL(i,:),'LineWidth',LW(i));hold on;
end
xlabel('Time (s)');   ylabel('v. GRF (Mg)');   box off;   axis square;
xlim([-0.03 T(1)]);   ylim([0 8]);
%---------------------------------------------------------------------Power
subplot(2,4,4*(iR-1)+3);   NDF= 1000;
plot([-0.03 T(1)],[0 0],'k');hold on; %-zero line
plot(-0.015*[1 1],[0 -4],'r','LineWidth',2);hold on; %-impulse (-)
plot(-0.015,-4,'rv','MarkerFaceColor','r');hold on;
plot( 0.015*[1 1],[0  4],'r','LineWidth',2);hold on; %-impulse (+)
plot( 0.015,4,'r^','MarkerFaceColor','r');hold on;
for iDsp=Dsp{iR}
i=find(iDsp==Dsp{iR});
plot(t{iDsp}(  1:pTO(iDsp)), Pn{iDsp}(  1:pTO(iDsp))/NDF,'-','Color',CL(i,:),'LineWidth',LW(i));hold on;
plot(t{iDsp}(pTO(iDsp):end), Pn{iDsp}(pTO(iDsp):end)/NDF,':','Color',CL(i,:),'LineWidth',LW(i));hold on;
end
xlabel('Time (s)');   ylabel('Power (kW)');   box off;   axis square;
xlim([-0.03 T(1)]);   ylim([-5 5]);
%------------------------------------------------------Quasi-stiffness Plot
subplot(2,4,4*(iR-1)+4);
plot([-6 6],[0 0],'k');hold on; %-zero line
plot([0 0],[-1 6],'r','LineWidth',2);hold on; %-impulse
plot(0,6,'r^','MarkerFaceColor','r');hold on;
for iDsp=Dsp{iR}
i=find(iDsp==Dsp{iR});
plot((yb{iDsp}(  1:pTO(iDsp))-yb{iDsp}(1))*100, ybdd{iDsp}(  1:pTO(iDsp))/gM(Dsp{iR}(1)),'-','Color',CL(i,:),'LineWidth',LW(i));hold on;
plot((yb{iDsp}(pTO(iDsp):end)-yb{iDsp}(1))*100, ybdd{iDsp}(pTO(iDsp):end)/gM(Dsp{iR}(1)),':','Color',CL(i,:),'LineWidth',LW(i));hold on;
end
xlabel('v. Disp. (cm)');   ylabel('v. Acc. (g)');   box off;   axis square;
xlim([-6 6]);   ylim([-2 8]);
end %-iterate rows












end %-end function