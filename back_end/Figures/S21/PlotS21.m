function PlotS21(OPvars, OptSol, Model, View, SPps)
%====================================================================Inputs
%  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
%  OptSol --> optimal solution
%  Model  --> structure with SPmat
%  View   --> structure indicating what plots to show
%  SPps   --> structure with model parameters from all OptSols in preset
%==========================================================================
%  This function plots figures from presets 'S21', a manuscript on running
%  mechanics and energetics using the 'PMR' and 'srinR' models.
%  Schroeder & Kuo, 2021 (planning to submit to PLoS Computational Biology?)




%% ============================================================Plot Figures
PlotS21_Fig01(OPvars, OptSol, SPps);pause(0.1) %-plot Fig. 1
%-Note: Fig. 2 does not contain any optimzation results
PlotS21_Fig03(OPvars, OptSol, SPps);pause(0.1) %-plot Fig. 3
PlotS21_Fig04(OPvars, OptSol, SPps);pause(0.1) %-plot Fig. 4
PlotS21_Fig05(OPvars, OptSol, SPps);pause(0.1) %-plot Fig. 5
PlotS21_Fig06(OPvars, OptSol, SPps);pause(0.1) %-plot Fig. 6
PlotS21_Fig07(OPvars, OptSol, SPps);pause(0.1) %-plot Fig. 7
PlotS21_Fig08(OPvars, OptSol, SPps);pause(0.1) %-plot Fig. 8
PlotS21_Fig09(OPvars, OptSol, SPps);pause(0.1) %-plot Fig. 9




end %-end function