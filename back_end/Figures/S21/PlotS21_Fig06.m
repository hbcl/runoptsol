function PlotS21_Fig06(OPvars, OptSol, SPps)
%====================================================================Inputs
%  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
%  OptSol --> optimal solution
%  SPps   --> structure with model parameters from all OptSols in preset
%==========================================================================
%  This function plots Fig. 6 from presets 'S21', a manuscript on running
%  mechanics and energetics using the 'PMR' models.
%  Schroeder & Kuo, 2021 (planning to submit to PLoS Computational Biology?)



%% ===========================================================Define Params
k_ref= 27370.46501693961;

kr= SPps.SpringConstant/k_ref; %-spring constant (relative)
k = SPps.SpringConstant;       %-spring constant
Sp= SPps.Speed; %-speed
for iSP=1:length(SPps.Name)
d(iSP)= OptSol(iSP).result.setup.auxdata.stpL; %-step length
end
f = Sp/d; %-step frequency
M = SPps.BodyMass; %-body mass
L = SPps.MaxLegLength; %-max leg length
gM= SPps.RelativeGravity*9.81; %-gravitational acceleration
S = SPps.GroundSlope;  %-ground slope
DR= SPps.DampingRatio; %-damping ratio
CF= SPps.CollisionFraction; %-collision fraction
cR= SPps.ForceRateCoef; %-force rate coefficient


%% ===================================================Define Vars for Plots
for iSP=1:length(SPps.Name)
%-time
  t{iSP}= OPvars(iSP).time.time; %-time
  T(iSP)= t{iSP}(end); %-step  time
 to(iSP)= t{iSP}(1);   %-start time
 tf(iSP)= t{iSP}(end); %-end   time
 DF(iSP)= OPvars(iSP).time.dutyFactor;  %-duty factor
 Tc(iSP)= OPvars(iSP).time.contactTime; %-ground contact time
 Tf(iSP)= OPvars(iSP).time.flightTime;  %-flight time
pTO(iSP)= OPvars(iSP).time.events(1,2); %-point of take off

%-kinematics
Th{iSP}= OPvars(iSP).kinematics.pos.legAngle;  %-leg angle, CCW from +--> (global)
Ll{iSP}= OPvars(iSP).kinematics.pos.legLength; %-leg length
if strcmpi(OPvars(iSP).name,'PMR')
Lt{iSP}= OPvars(iSP).kinematics.pos.springLength;
Lm{iSP}= OPvars(iSP).kinematics.pos.actuatorLength;
end
xb{iSP}= OPvars(iSP).kinematics.pos.body(:,1); %-body hor pos
yb{iSP}= OPvars(iSP).kinematics.pos.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
xm{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,1); %-actuator end away from ground (hor)
ym{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,2); %-actuator end away from ground (vrt)
end
 xf(iSP)= OPvars(iSP).kinematics.pos.foot(:,1);  %-foot hor pos
 yf(iSP)= OPvars(iSP).kinematics.pos.foot(:,2);  %-foot vrt pos
Thd{iSP}= OPvars(iSP).kinematics.vel.legAngle;  %-leg angular velocity, CCW +
Lld{iSP}= OPvars(iSP).kinematics.vel.legLength; %-leg velocity
xbd{iSP}= OPvars(iSP).kinematics.vel.body(:,1); %-body hor pos
ybd{iSP}= OPvars(iSP).kinematics.vel.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
Ltd{iSP}= OPvars(iSP).kinematics.vel.springLength;   %-spring vel
Lmd{iSP}= OPvars(iSP).kinematics.vel.actuatorLength; %-actuator vel
end
Lldd{iSP}= OPvars(iSP).kinematics.acc.leglength; %-leg acc
xbdd{iSP}= OPvars(iSP).kinematics.acc.body(:,1); %-hor body acc
ybdd{iSP}= OPvars(iSP).kinematics.acc.body(:,2); %-vrt body acc
if strcmpi(OPvars(iSP).name,'PMR');      Lmdd{iSP}= OPvars(iSP).kinematics.acc.actuatorLength; else;  Lmdd{iSP}=nan; end
if isfield(OPvars(iSP).kinetics,'jrk'); Llddd{iSP}= OPvars(iSP).kinematics.jrk.legLength;      else; Llddd{iSP}=nan; end
if strcmpi(OPvars(iSP).name,'PMR');     Lmddd{iSP}= OPvars(iSP).kinematics.jrk.actuatorLength; else; Lmddd{iSP}=nan; end


%-kinetics
 Rx{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,1); %-hor GRF
 Ry{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,2); %-vrt GRF
 Fm{iSP}=  OPvars(iSP).kinetics.actuatorForce;  %-Actuator force
Fmd{iSP}=  OPvars(iSP).kinetics.actuatorForce_ddt1; %-d/dt actuator force
if isfield(OPvars(iSP).kinetics,'actuatorForce_ddt2'); Fmdd{iSP}= OPvars(iSP).kinetics.actuatorForce_ddt2; else; Fmdd{iSP}=nan; end %-d2/dt2 actuator force
if isfield(OPvars(iSP).kinetics,'springForce');        Fs{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fs{iSP}  =nan; end %-Spring force
if isfield(OPvars(iSP).kinetics,'damperForce');        Fd{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fd{iSP}  =nan; end %-Damper force


%-energetics
negMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(1); %-neg actuator work
posMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(2); %-pos actuator work
absMscWrk(iSP)= posMscWrk(iSP)-negMscWrk(iSP); %-abs actuator work
netMscWrk(iSP)= posMscWrk(iSP)+negMscWrk(iSP); %-net actuator work
if strcmpi(OPvars(iSP).name,'PMR')
negSprWrk(iSP)= OPvars(iSP).energetics.work.spring(1); %-neg spring work
posSprWrk(iSP)= OPvars(iSP).energetics.work.spring(2); %-pos spring work
negDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(1); %-neg damper work
posDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(2); %-pos damper work
end
Pm{iSP}= OPvars(iSP).energetics.power.actuator; %-actuator power
if strcmpi(OPvars(iSP).name,'PMR')
Ps{iSP}= OPvars(iSP).energetics.power.spring; %-spring power
Pd{iSP}= OPvars(iSP).energetics.power.damper; %-damper power
Pn{iSP}= Pm{iSP}+Ps{iSP}+Pd{iSP}; %-net power
HysLoss(iSP)= -trapz(t{iSP},Pd{iSP}); %[J] hysteresis loss magnitude
elseif strcmpi(OPvars(iSP).name,'srinR')
Pn{iSP}= Pm{iSP}; %-net power
end

vbf(iSP)= sqrt(xbd{iSP}(end)^2+ybd{iSP}(end)^2); %-final   body velocity before TD
vbi(iSP)= sqrt(xbd{iSP}( 1 )^2+ybd{iSP}( 1 )^2); %-initial body velocity after  TD
dKE(iSP)= 0.5*M(iSP)*(vbi(iSP)^2-vbf(iSP)^2); %-change in KE from final to initial
ColLoss(iSP)= -dKE(iSP); %[J] collision loss magnitude

%-cost
cTot(iSP)= OPvars(iSP).cost.total;
cWrk(iSP)= OPvars(iSP).cost.work;
cFRS(iSP)= OPvars(iSP).cost.forceRate;

end %-end defining vars



%% ==================================================Choose OptSols to Plot
%-all Actuated Spring-mass OptSols (force-rate coef)
cRDsp1= [1 2 5 8 1 2 3 5 1 2].*10.^(-[4 4 4 4 3 3 3 3 2 2]); %-force-rate coefficient for display
iC=1;
Dsp{iC}= find(strcmpi(SPps.Name,'PMR') & strcmpi(SPps.StepLength,'G06') &...
       SPps.SpringConstant==1.3*k_ref  & SPps.BodyMass==70              &...
               SPps.DampingRatio==0.1  & SPps.CollisionFraction==0.03   &...
                SPps.GroundSlope==0    & SPps.RelativeGravity==1        &...
                      SPps.Speed==3    & SPps.MaxLegLength==0.9         &...
                                       ismember(SPps.ForceRateCoef,cRDsp1));
[U, I]= sort(cR(Dsp{iC}),'ascend');   Dsp{iC}= Dsp{iC}(I);      %-ascending order
[unqVal unqInd]= unique(cR(Dsp{iC})); Dsp{iC}= Dsp{iC}(unqInd); %-eliminate duplicates


%-highlight OptSols (force-rate coef)
cRDsp2= [0.0002 0.0008 0.003 0.01]; %-force-rate coefficient for display

iC=2;
Dsp{iC}= find(strcmpi(SPps.Name,'PMR') & strcmpi(SPps.StepLength,'G06') &...
       SPps.SpringConstant==1.3*k_ref  & SPps.BodyMass==70              &...
               SPps.DampingRatio==0.1  & SPps.CollisionFraction==0.03   &...
                SPps.GroundSlope==0    & SPps.RelativeGravity==1        &...
                      SPps.Speed==3    & SPps.MaxLegLength==0.9         &...
                                       ismember(SPps.ForceRateCoef,cRDsp2));
[U, I]= sort(cR(Dsp{iC}),'ascend');   Dsp{iC}= Dsp{iC}(I);      %-ascending order
[unqVal unqInd]= unique(cR(Dsp{iC})); Dsp{iC}= Dsp{iC}(unqInd); %-eliminate duplicates



%% =============================================================Plot Figure
nR= 6; %-# rows
nC= 4; %-# cols


figure('Name','Fig. 6: Effect of force-rate coefficient on mechanics/cost',...
           'Color','w','NumberTitle','off','Units','Normalized',...
           'Position',[0.1 0.1 0.5 0.6]);

%-----------------------------------------------------------------Work/Cost
subplot(nR,nC,nC*round(nR/2)+1:nC*nR);   NDF= gM(Dsp{1}(1))*L(Dsp{1}(1));

%-plot vertical lines
for iDsp=1:length(cRDsp2)
semilogx(cRDsp2(iDsp)*[1 1],[0 14],'--','Color',0.8*ones(1,3));hold on;
end

%-plot interpolated work/cost
Npts=50;
Xp= linspace(min(cRDsp1),max(cRDsp1),Npts);
Yp_cWrk= spline([min(cRDsp1)-eps cRDsp1 max(cRDsp1)+eps],[cWrk(Dsp{1}(1)) cWrk(Dsp{1}) cWrk(Dsp{1}(end))  ],Xp);
Yp_cTot= spline([min(cRDsp1)-eps cRDsp1 max(cRDsp1)+eps],[cTot(Dsp{1}(1)) cTot(Dsp{1}) cTot(Dsp{1}(end))+5e-15],Xp);
semilogx(Xp,Yp_cWrk*NDF,'b-','LineWidth',1.5);hold on;
semilogx(Xp,Yp_cTot*NDF,'k-','LineWidth',2.5);hold on;
box off;   xticks([1e-4 1e-3 1e-2]);   yticks([]);
xlim([1e-4 2e-2]);   ylim([0 14]);
xlabel('Force-rate Cost Coefficient, \epsilon (M^{-1}g^{-1.5}L^{1.5})');
ylabel('Energetic Cost per Step');

%-plot cost interpolations (at highlight OptSols)
for iDsp=1:length(cRDsp2)
semilogx(cRDsp2(iDsp),interp1(cRDsp1,cWrk(Dsp{1}),cRDsp2(iDsp))*NDF,'bo','MarkerFaceColor','w','LineWidth',2);hold on;
semilogx(cRDsp2(iDsp),interp1(cRDsp1,cTot(Dsp{1}),cRDsp2(iDsp))*NDF,'ko','MarkerFaceColor','w','LineWidth',2);hold on;
end

%-cost units bar
plot(4.6e-3*[1 1],[1.6 3.6],'k','LineWidth',1);
text(5.1e-3,2.8,'2 J kg^{-1}','HorizontalAlignment','Left','VerticalAlignment','Middle');


Rt=0.4;
%-----------------------------------------------------------Plot Highlights
for iC=1:length(Dsp{2})
%--------------------------------------------------------------Trajectories
subplot(nR,nC,iC);   LW=1.5;
plot(t{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC))),yb{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC)))-median(yb{Dsp{2}(iC)}),'k-','LineWidth',LW);hold on;
plot(t{Dsp{2}(iC)}(pTO(Dsp{2}(iC)):end),yb{Dsp{2}(iC)}(pTO(Dsp{2}(iC)):end)-median(yb{Dsp{2}(iC)}),'k:','LineWidth',LW);hold on;
ylim([-0.1 0.1]);   xlim([0 Rt]);
box off; axis off;
if iC==1;   txt=text(-0.2,-0.1,'v. Disp.');   end;   set(txt,'Rotation',60);

%-plot height units bar
if iC==length(Dsp{2})
plot(Rt*[1 1],[-0.05 0.05],'k','LineWidth',1);hold on;
text(Rt+0.03,0,'10 cm','HorizontalAlignment','Left','VerticalAlignment','Middle');hold on;
end
%----------------------------------------------------Ground Reaction Forces
subplot(nR,nC,iC+nC);   LW=1.5;   NDF= M(Dsp{2}(1))*gM(Dsp{2}(1));
plot(t{Dsp{2}(iC)},zeros(size(t{Dsp{2}(iC)})),'k');hold on; %-plot zero line
plot([0 0],[0 2.5],'r','LineWidth',2);hold on;  %-plot impulse
plot(0,2.5,'r^','MarkerFaceColor','r');hold on; %-plot impulse
plot(t{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC))),Ry{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC)))/NDF,'k-','LineWidth',LW);hold on;
plot(t{Dsp{2}(iC)}(pTO(Dsp{2}(iC)):end),Ry{Dsp{2}(iC)}(pTO(Dsp{2}(iC)):end)/NDF,'k:','LineWidth',LW);hold on;
ylim([0 4]);   xlim([0 Rt]);
box off; axis off;
if iC==1;   txt=text(-0.2,-0.1,'v. GRF.');   end;   set(txt,'Rotation',60);

%-plot height units bar
if iC==length(Dsp{2})
plot(Rt*[1 1],[0 2],'k','LineWidth',1);hold on;
text(Rt+0.03,1,'2 Mg','HorizontalAlignment','Left','VerticalAlignment','Middle');hold on;
end

%-plot time units bar
if iC==1
plot((t{Dsp{iC}(1)}(pTO(Dsp{iC}(1)))+0.03)+[0 0.1],[1 1],'k','LineWidth',1);hold on;
text((t{Dsp{iC}(1)}(pTO(Dsp{iC}(1)))+0.03)+0.05,1.3,'0.1 s','HorizontalAlignment','Center','VerticalAlignment','Bottom');hold on;
end
%---------------------------------------------------------------------Power
subplot(nR,nC,iC+2*nC);   LW=1.5;   NDF= 1000;
plot(t{Dsp{2}(iC)},zeros(size(t{Dsp{2}(iC)})),'k');hold on; %-plot zero line
plot([0 0],[0 -2.5],'r','LineWidth',2);hold on;  %-plot impulse
plot(0,-2.5,'rv','MarkerFaceColor','r');hold on; %-plot impulse
fill(t{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC))),Pn{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC)))/NDF,[255 165 0]/255,'LineStyle','None');hold on;
fill(t{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC))),Pm{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC)))/NDF,'b' ,'LineStyle','None');hold on;
plot(t{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC))),Pn{Dsp{2}(iC)}(  1:pTO(Dsp{2}(iC)))/NDF,'k-','LineWidth',LW);hold on;
plot(t{Dsp{2}(iC)}(pTO(Dsp{2}(iC)):end),Pn{Dsp{2}(iC)}(pTO(Dsp{2}(iC)):end)/NDF,'k:','LineWidth',LW);hold on;
ylim([-2.5 2.5]);   xlim([0 Rt]);
box off; axis off;
if iC==1;   txt=text(-0.18,-1,'Power');   end;   set(txt,'Rotation',60);

%-plot height units bar
if iC==length(Dsp{2})
plot(Rt*[1 1],[-1 1],'k','LineWidth',1);hold on;
text(Rt+0.03,0,'2 kW','HorizontalAlignment','Left','VerticalAlignment','Middle');hold on;
end

end %-iterate cols




end %-end function