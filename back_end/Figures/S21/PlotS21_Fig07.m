function PlotS21_Fig07(OPvars, OptSol, SPps)
%====================================================================Inputs
%  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
%  OptSol --> optimal solution
%  SPps   --> structure with model parameters from all OptSols in preset
%==========================================================================
%  This function plots Fig. 7 from presets 'S21', a manuscript on running
%  mechanics and energetics using the 'PMR' model and human data.
%  Schroeder & Kuo, 2021 (planning to submit to PLoS Computational Biology?)



%% ===========================================================Define Params
k_ref= 27370.46501693961;

kr= SPps.SpringConstant/k_ref; %-spring constant (relative)
k = SPps.SpringConstant;       %-spring constant
Sp= SPps.Speed; %-speed
for iSP=1:length(SPps.Name)
d(iSP)= OptSol(iSP).result.setup.auxdata.stpL; %-step length
end
f = Sp/d; %-step frequency
M = SPps.BodyMass; %-body mass
L = SPps.MaxLegLength; %-max leg length
gM= SPps.RelativeGravity*9.81; %-gravitational acceleration
S = SPps.GroundSlope;  %-ground slope
DR= SPps.DampingRatio; %-damping ratio
CF= SPps.CollisionFraction; %-collision fraction
cR= SPps.ForceRateCoef; %-force rate coefficient


%% ===================================================Define Vars for Plots
for iSP=1:length(SPps.Name)
%-time
  t{iSP}= OPvars(iSP).time.time; %-time
  T(iSP)= t{iSP}(end); %-step  time
 to(iSP)= t{iSP}(1);   %-start time
 tf(iSP)= t{iSP}(end); %-end   time
 DF(iSP)= OPvars(iSP).time.dutyFactor;  %-duty factor
 Tc(iSP)= OPvars(iSP).time.contactTime; %-ground contact time
 Tf(iSP)= OPvars(iSP).time.flightTime;  %-flight time
pTO(iSP)= OPvars(iSP).time.events(1,2); %-point of take off

%-kinematics
Th{iSP}= OPvars(iSP).kinematics.pos.legAngle;  %-leg angle, CCW from +--> (global)
Ll{iSP}= OPvars(iSP).kinematics.pos.legLength; %-leg length
if strcmpi(OPvars(iSP).name,'PMR')
Lt{iSP}= OPvars(iSP).kinematics.pos.springLength;
Lm{iSP}= OPvars(iSP).kinematics.pos.actuatorLength;
end
xb{iSP}= OPvars(iSP).kinematics.pos.body(:,1); %-body hor pos
yb{iSP}= OPvars(iSP).kinematics.pos.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
xm{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,1); %-actuator end away from ground (hor)
ym{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,2); %-actuator end away from ground (vrt)
end
 xf(iSP)= OPvars(iSP).kinematics.pos.foot(:,1);  %-foot hor pos
 yf(iSP)= OPvars(iSP).kinematics.pos.foot(:,2);  %-foot vrt pos
Thd{iSP}= OPvars(iSP).kinematics.vel.legAngle;  %-leg angular velocity, CCW +
Lld{iSP}= OPvars(iSP).kinematics.vel.legLength; %-leg velocity
xbd{iSP}= OPvars(iSP).kinematics.vel.body(:,1); %-body hor pos
ybd{iSP}= OPvars(iSP).kinematics.vel.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
Ltd{iSP}= OPvars(iSP).kinematics.vel.springLength;   %-spring vel
Lmd{iSP}= OPvars(iSP).kinematics.vel.actuatorLength; %-actuator vel
end
Lldd{iSP}= OPvars(iSP).kinematics.acc.leglength; %-leg acc
xbdd{iSP}= OPvars(iSP).kinematics.acc.body(:,1); %-hor body acc
ybdd{iSP}= OPvars(iSP).kinematics.acc.body(:,2); %-vrt body acc
if strcmpi(OPvars(iSP).name,'PMR');      Lmdd{iSP}= OPvars(iSP).kinematics.acc.actuatorLength; else;  Lmdd{iSP}=nan; end
if isfield(OPvars(iSP).kinetics,'jrk'); Llddd{iSP}= OPvars(iSP).kinematics.jrk.legLength;      else; Llddd{iSP}=nan; end
if strcmpi(OPvars(iSP).name,'PMR');     Lmddd{iSP}= OPvars(iSP).kinematics.jrk.actuatorLength; else; Lmddd{iSP}=nan; end


%-kinetics
 Rx{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,1); %-hor GRF
 Ry{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,2); %-vrt GRF
 Fm{iSP}=  OPvars(iSP).kinetics.actuatorForce;  %-Actuator force
Fmd{iSP}=  OPvars(iSP).kinetics.actuatorForce_ddt1; %-d/dt actuator force
if isfield(OPvars(iSP).kinetics,'actuatorForce_ddt2'); Fmdd{iSP}= OPvars(iSP).kinetics.actuatorForce_ddt2; else; Fmdd{iSP}=nan; end %-d2/dt2 actuator force
if isfield(OPvars(iSP).kinetics,'springForce');        Fs{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fs{iSP}  =nan; end %-Spring force
if isfield(OPvars(iSP).kinetics,'damperForce');        Fd{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fd{iSP}  =nan; end %-Damper force


%-energetics
negMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(1); %-neg actuator work
posMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(2); %-pos actuator work
absMscWrk(iSP)= posMscWrk(iSP)-negMscWrk(iSP); %-abs actuator work
netMscWrk(iSP)= posMscWrk(iSP)+negMscWrk(iSP); %-net actuator work
if strcmpi(OPvars(iSP).name,'PMR')
negSprWrk(iSP)= OPvars(iSP).energetics.work.spring(1); %-neg spring work
posSprWrk(iSP)= OPvars(iSP).energetics.work.spring(2); %-pos spring work
negDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(1); %-neg damper work
posDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(2); %-pos damper work
end
Pm{iSP}= OPvars(iSP).energetics.power.actuator; %-actuator power
if strcmpi(OPvars(iSP).name,'PMR')
Ps{iSP}= OPvars(iSP).energetics.power.spring; %-spring power
Pd{iSP}= OPvars(iSP).energetics.power.damper; %-damper power
Pn{iSP}= Pm{iSP}+Ps{iSP}+Pd{iSP}; %-net power
HysLoss(iSP)= -trapz(t{iSP},Pd{iSP}); %[J] hysteresis loss magnitude
elseif strcmpi(OPvars(iSP).name,'srinR')
Pn{iSP}= Pm{iSP}; %-net power
end

vbf(iSP)= sqrt(xbd{iSP}(end)^2+ybd{iSP}(end)^2); %-final   body velocity before TD
vbi(iSP)= sqrt(xbd{iSP}( 1 )^2+ybd{iSP}( 1 )^2); %-initial body velocity after  TD
dKE(iSP)= 0.5*M(iSP)*(vbi(iSP)^2-vbf(iSP)^2); %-change in KE from final to initial
ColLoss(iSP)= -dKE(iSP); %[J] collision loss magnitude

%-cost
cTot(iSP)= OPvars(iSP).cost.total;
cWrk(iSP)= OPvars(iSP).cost.work;
cFRS(iSP)= OPvars(iSP).cost.forceRate;

end %-end defining vars


%% =========================================================Load Human Data
[data]= RepresentativeHumanRunningSubject();
xbH= data.traj(:,1); %-hor body pos
ybH= data.traj(:,2); %-vrt body pos
tH = data.vGRF(:,1); %-time
RyH= data.vGRF(:,2); %-vrt GRF
PnH= data.comP(:,2); %-CoM power
ybddH= data.loop(:,2); %-vrt body acc
xfH  = data.fpos; %-hor foot pos
p1H  = data.evnt(1):data.evnt(2); %-points during stance
p2H  = data.evnt(2):data.evnt(3); %-points during flight
pCH  = 1:76; %-points during initial transient impact force


%% ==================================================Choose OptSols to Plot
%-Unified Model
whereNum= find(cellfun('isclass',SPps.StepLength,'double')==1); %-where is Step Length a #?
StepLengthPH= zeros(size(SPps.Speed));   StepLengthPH(whereNum)= SPps.StepLength{whereNum};
iR=1;
Dsp{iR}= find(strcmpi(SPps.Name,'PMR') & StepLengthPH==1.3             &...
       SPps.SpringConstant==1.3*k_ref  & SPps.BodyMass==75.3           &...
               SPps.DampingRatio==0.1  & SPps.CollisionFraction==0.03  &...
                SPps.GroundSlope==0    & SPps.RelativeGravity==1       &...
                      SPps.Speed==3.9  & SPps.MaxLegLength==0.79       &...
                                         SPps.ForceRateCoef==5e-4);
[unqVal unqInd]= unique(Sp(Dsp{iR})); Dsp{iR}= Dsp{iR}(unqInd); %-eliminate duplicates



%% =============================================================Plot Figure
nR= 2; %-# rows
nC= 4; %-# cols


figure('Name','Fig. 7: Unified Model vs. Human Data',...
           'Color','w','NumberTitle','off','Units','Normalized',...
           'Position',[0.1 0.1 0.5 0.6]);



%% =========================================================Plot Model Data
LW=2;
%--------------------------------------------------------------Trajectories
subplot(nR,nC,1);

%-plot ground
plot(d(Dsp{iR}(1)).*[0 1],[0 0],'k','LineWidth',3);hold on;

%-plot compliant/stiff leg angles
iDsp=Dsp{iR}(1); %-compliant leg
plot([xf(iDsp) xb{iDsp}( 1 )],[0 yb{iDsp}( 1 )],'k','LineWidth',1); %-initial leg angle
% plot([0 xb{iDsp}(end)],[0 yb{iDsp}(end)],'k','LineWidth',1); %-final   leg angle



%-plot trajectories
for iDsp=Dsp{iR}
i=find(iDsp==Dsp{iR});
plot(xb{iDsp}(  1:pTO(iDsp)), yb{iDsp}(  1:pTO(iDsp)),'b-','LineWidth',LW);hold on;
plot(xb{iDsp}(pTO(iDsp):end), yb{iDsp}(pTO(iDsp):end),'b:','LineWidth',LW);hold on;
end
axis equal;
xticks('');yticks('');box off;axis off;
title('Unified Model');
%----------------------------------------------------------------------GRFs
subplot(nR,nC,2);   NDF= M(Dsp{iR}(1))*gM(Dsp{iR}(1));
plot([-0.03 T(1)],[0 0],'k');hold on; %-zero line
plot([0 0],[0 3],'r','LineWidth',2);hold on; %-Collision impulse
plot(0,3,'r^','MarkerFaceColor','r');hold on;
for iDsp=Dsp{iR}
i=find(iDsp==Dsp{iR});
plot(t{iDsp}(  1:pTO(iDsp)), Ry{iDsp}(  1:pTO(iDsp))/NDF,'k-','LineWidth',LW);hold on;
plot(t{iDsp}(pTO(iDsp):end), Ry{iDsp}(pTO(iDsp):end)/NDF,'k:','LineWidth',LW);hold on;
end
xlabel('Time (s)');   ylabel('v. GRF (Mg)');   box off;   axis square;
xlim([-0.03 T(1)]);   ylim([-1 5]);   xticks([0:0.1:0.3]);   yticks([0:2:4]);
%---------------------------------------------------------------------Power
subplot(nR,nC,3);   NDF= 1000;
plot([-0.03 T(1)],[0 0],'k');hold on; %-zero line
plot([0 0],[0 -2],'r','LineWidth',2);hold on; %-Collision impulse
plot(0,-2,'rv','MarkerFaceColor','r');hold on;
for iDsp=Dsp{iR}
i=find(iDsp==Dsp{iR});
 fill(t{iDsp}(  1:pTO(iDsp)), Pn{iDsp}(  1:pTO(iDsp))/NDF,[255 160 0]/255,'LineWidth',LW);hold on; %-fill leg power with orange
 fill(t{iDsp}(  1:pTO(iDsp)), Pm{iDsp}(  1:pTO(iDsp))/NDF,'b ','LineStyle','None');hold on; %-actuator power
 plot(t{iDsp}(  1:pTO(iDsp)), Pn{iDsp}(  1:pTO(iDsp))/NDF,'k-','LineWidth',LW);hold on; %-net leg power
 plot(t{iDsp}(pTO(iDsp):end), Pn{iDsp}(pTO(iDsp):end)/NDF,'k:','LineWidth',LW);hold on;
end
xlabel('Time (s)');   ylabel('Power (kW)');   box off;   axis square;
xlim([-0.03 T(1)]);   ylim([-3 3]);   xticks([0:0.1:0.3]);   yticks([-2:2:2]);
%------------------------------------------------------Quasi-stiffness Plot
subplot(nR,nC,4);
plot([-6 6],[0 0],'k');hold on; %-zero line
plot([0 0],[-1 2],'r','LineWidth',2);hold on; %-Collision impulse
plot(0,2,'r^','MarkerFaceColor','r');hold on;
for iDsp=Dsp{iR}
i=find(iDsp==Dsp{iR});
plot((yb{iDsp}(  1:pTO(iDsp))-yb{iDsp}(1))*100, ybdd{iDsp}(  1:pTO(iDsp))/gM(Dsp{iR}(1)),'k-','LineWidth',LW);hold on;
plot((yb{iDsp}(pTO(iDsp):end)-yb{iDsp}(1))*100, ybdd{iDsp}(pTO(iDsp):end)/gM(Dsp{iR}(1)),'k:','LineWidth',LW);hold on;
end
xlabel('v. Disp. (cm)');   ylabel('v. Acc. (g)');   box off;   axis square;
xlim([-6 6]);   ylim([-1.5 3]);   xticks([-6:3:3]);   yticks([-1:3]);



%% =========================================================Plot Human Data
%--------------------------------------------------------------Trajectories
subplot(nR,nC,1+nC);

%-plot ground
plot(d(Dsp{iR}(1)).*[0 1],[0 0],'k','LineWidth',3);hold on;

%-plot leg angles
plot([xfH xbH( 1 )],[0 ybH( 1 )],'k','LineWidth',1); %-initial leg angle
% plot([0 xbH(end)],[0 ybH(end)],'k','LineWidth',1); %-final   leg angle


%-plot trajectories
plot(xbH(p1H),ybH(p1H),'b-','LineWidth',LW);hold on;
plot(xbH(p2H),ybH(p2H),'b:','LineWidth',LW);hold on;
axis equal;
xticks('');yticks('');box off;axis off;
title('Human');
%----------------------------------------------------------------------GRFs
subplot(nR,nC,2+nC);   NDF= M(Dsp{iR}(1))*gM(Dsp{iR}(1));
plot([-0.03 T(1)],[0 0],'k');hold on; %-zero line
plot(tH(p1H), RyH(p1H)/NDF,'k-','LineWidth',LW);hold on;
plot(tH(p2H), RyH(p2H)/NDF,'k:','LineWidth',LW);hold on;
plot(tH(pCH), RyH(pCH)/NDF,'r-','LineWidth',LW);hold on; %-impact trace
xlabel('Time (s)');   ylabel('v. GRF (Mg)');   box off;   axis square;
xlim([-0.03 T(1)]);   ylim([-1 5]);   xticks([0:0.1:0.3]);   yticks([0:2:4]);
%---------------------------------------------------------------------Power
subplot(nR,nC,3+nC);   NDF= 1000;
plot([-0.03 T(1)],[0 0],'k');hold on; %-zero line
plot(tH(p1H), PnH(p1H)/NDF,'k-','LineWidth',LW);hold on; %-CoM power
plot(tH(p2H), PnH(p2H)/NDF,'k:','LineWidth',LW);hold on;
plot(tH(pCH), PnH(pCH)/NDF,'r-','LineWidth',LW);hold on; %-impact trace
xlabel('Time (s)');   ylabel('Power (kW)');   box off;   axis square;
xlim([-0.03 T(1)]);   ylim([-3 3]);   xticks([0:0.1:0.3]);   yticks([-2:2:2]);
%------------------------------------------------------Quasi-stiffness Plot
subplot(nR,nC,4+nC);
plot([-6 6],[0 0],'k');hold on; %-zero line
plot((ybH(p1H)-ybH(1))*100, ybddH(p1H)/gM(Dsp{iR}(1)),'k-','LineWidth',LW);hold on;
plot((ybH(p2H)-ybH(1))*100, ybddH(p2H)/gM(Dsp{iR}(1)),'k:','LineWidth',LW);hold on;
plot((ybH(pCH)-ybH(1))*100, ybddH(pCH)/gM(Dsp{iR}(1)),'r-','LineWidth',LW);hold on; %-impact trace
xlabel('v. Disp. (cm)');   ylabel('v. Acc. (g)');   box off;   axis square;
xlim([-6 6]);   ylim([-1.5 3]);   xticks([-6:3:3]);   yticks([-1:3]);





end %-end function