function PlotS21_Fig08(OPvars, OptSol, SPps)
%====================================================================Inputs
%  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
%  OptSol --> optimal solution
%  SPps   --> structure with model parameters from all OptSols in preset
%==========================================================================
%  This function plots Fig. 8 from presets 'S21', a manuscript on running
%  mechanics and energetics using the 'PMR' model and human data.
%  Schroeder & Kuo, 2021 (planning to submit to PLoS Computational Biology?)



%% ===========================================================Define Params
k_ref= 27370.46501693961;

kr= SPps.SpringConstant/k_ref; %-spring constant (relative)
k = SPps.SpringConstant;       %-spring constant
Sp= SPps.Speed; %-speed
for iSP=1:length(SPps.Name)
d(iSP)= OptSol(iSP).result.setup.auxdata.stpL; %-step length
end
f = Sp./d; %-step frequency
M = SPps.BodyMass; %-body mass
L = SPps.MaxLegLength; %-max leg length
gM= SPps.RelativeGravity*9.81; %-gravitational acceleration
S = SPps.GroundSlope;  %-ground slope
DR= SPps.DampingRatio; %-damping ratio
CF= SPps.CollisionFraction; %-collision fraction
cR= SPps.ForceRateCoef; %-force rate coefficient


%% ===================================================Define Vars for Plots
for iSP=1:length(SPps.Name)
%-time
  t{iSP}= OPvars(iSP).time.time; %-time
  T(iSP)= t{iSP}(end); %-step  time
 to(iSP)= t{iSP}(1);   %-start time
 tf(iSP)= t{iSP}(end); %-end   time
 DF(iSP)= OPvars(iSP).time.dutyFactor;  %-duty factor
 Tc(iSP)= OPvars(iSP).time.contactTime; %-ground contact time
 Tf(iSP)= OPvars(iSP).time.flightTime;  %-flight time
pTO(iSP)= OPvars(iSP).time.events(1,2); %-point of take off

%-kinematics
Th{iSP}= OPvars(iSP).kinematics.pos.legAngle;  %-leg angle, CCW from +--> (global)
Ll{iSP}= OPvars(iSP).kinematics.pos.legLength; %-leg length
if strcmpi(OPvars(iSP).name,'PMR')
Lt{iSP}= OPvars(iSP).kinematics.pos.springLength;
Lm{iSP}= OPvars(iSP).kinematics.pos.actuatorLength;
end
xb{iSP}= OPvars(iSP).kinematics.pos.body(:,1); %-body hor pos
yb{iSP}= OPvars(iSP).kinematics.pos.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
xm{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,1); %-actuator end away from ground (hor)
ym{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,2); %-actuator end away from ground (vrt)
end
 xf(iSP)= OPvars(iSP).kinematics.pos.foot(:,1);  %-foot hor pos
 yf(iSP)= OPvars(iSP).kinematics.pos.foot(:,2);  %-foot vrt pos
Thd{iSP}= OPvars(iSP).kinematics.vel.legAngle;  %-leg angular velocity, CCW +
Lld{iSP}= OPvars(iSP).kinematics.vel.legLength; %-leg velocity
xbd{iSP}= OPvars(iSP).kinematics.vel.body(:,1); %-body hor pos
ybd{iSP}= OPvars(iSP).kinematics.vel.body(:,2); %-body vrt pos
if strcmpi(OPvars(iSP).name,'PMR')
Ltd{iSP}= OPvars(iSP).kinematics.vel.springLength;   %-spring vel
Lmd{iSP}= OPvars(iSP).kinematics.vel.actuatorLength; %-actuator vel
end
Lldd{iSP}= OPvars(iSP).kinematics.acc.leglength; %-leg acc
xbdd{iSP}= OPvars(iSP).kinematics.acc.body(:,1); %-hor body acc
ybdd{iSP}= OPvars(iSP).kinematics.acc.body(:,2); %-vrt body acc
if strcmpi(OPvars(iSP).name,'PMR');      Lmdd{iSP}= OPvars(iSP).kinematics.acc.actuatorLength; else;  Lmdd{iSP}=nan; end
if isfield(OPvars(iSP).kinetics,'jrk'); Llddd{iSP}= OPvars(iSP).kinematics.jrk.legLength;      else; Llddd{iSP}=nan; end
if strcmpi(OPvars(iSP).name,'PMR');     Lmddd{iSP}= OPvars(iSP).kinematics.jrk.actuatorLength; else; Lmddd{iSP}=nan; end


%-kinetics
 Rx{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,1); %-hor GRF
 Ry{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,2); %-vrt GRF
 Fm{iSP}=  OPvars(iSP).kinetics.actuatorForce;  %-Actuator force
Fmd{iSP}=  OPvars(iSP).kinetics.actuatorForce_ddt1; %-d/dt actuator force
if isfield(OPvars(iSP).kinetics,'actuatorForce_ddt2'); Fmdd{iSP}= OPvars(iSP).kinetics.actuatorForce_ddt2; else; Fmdd{iSP}=nan; end %-d2/dt2 actuator force
if isfield(OPvars(iSP).kinetics,'springForce');        Fs{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fs{iSP}  =nan; end %-Spring force
if isfield(OPvars(iSP).kinetics,'damperForce');        Fd{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fd{iSP}  =nan; end %-Damper force


%-energetics
negMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(1); %-neg actuator work
posMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(2); %-pos actuator work
absMscWrk(iSP)= posMscWrk(iSP)-negMscWrk(iSP); %-abs actuator work
netMscWrk(iSP)= posMscWrk(iSP)+negMscWrk(iSP); %-net actuator work
if strcmpi(OPvars(iSP).name,'PMR')
negSprWrk(iSP)= OPvars(iSP).energetics.work.spring(1); %-neg spring work
posSprWrk(iSP)= OPvars(iSP).energetics.work.spring(2); %-pos spring work
negDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(1); %-neg damper work
posDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(2); %-pos damper work
end
Pm{iSP}= OPvars(iSP).energetics.power.actuator; %-actuator power
if strcmpi(OPvars(iSP).name,'PMR')
Ps{iSP}= OPvars(iSP).energetics.power.spring; %-spring power
Pd{iSP}= OPvars(iSP).energetics.power.damper; %-damper power
Pn{iSP}= Pm{iSP}+Ps{iSP}+Pd{iSP}; %-net power
HysLoss(iSP)= -trapz(t{iSP},Pd{iSP}); %[J] hysteresis loss magnitude
elseif strcmpi(OPvars(iSP).name,'srinR')
Pn{iSP}= Pm{iSP}; %-net power
end

vbf(iSP)= sqrt(xbd{iSP}(end)^2+ybd{iSP}(end)^2); %-final   body velocity before TD
vbi(iSP)= sqrt(xbd{iSP}( 1 )^2+ybd{iSP}( 1 )^2); %-initial body velocity after  TD
dKE(iSP)= 0.5*M(iSP)*(vbi(iSP)^2-vbf(iSP)^2); %-change in KE from final to initial
ColLoss(iSP)= -dKE(iSP); %[J] collision loss magnitude

%-cost
% cTot(iSP)= OPvars(iSP).cost.total; %-work + force-rate cost
% cWrk(iSP)= OPvars(iSP).cost.work;  %-work cost
e= [0.32 -1.05]; %-muscle efficiency for positive/negative work
cWrk(iSP)= (posMscWrk(iSP)/e(1) + negMscWrk(iSP)/e(2))./(M(iSP)*gM(iSP)*L(iSP));  %-work cost
cFRS(iSP)= OPvars(iSP).cost.forceRate; %-force-rate cost
cOFS(iSP)= 0.08*2.94./f(iSP); %-offset cost
cTot(iSP)= cOFS(iSP)+cWrk(iSP)+cFRS(iSP); %-total cost

pWrk(iSP)= cWrk(iSP)*f(iSP); %-cost of       work per time
pFRS(iSP)= cFRS(iSP)*f(iSP); %-cost of force-rate per time
pOFS(iSP)= cOFS(iSP)*f(iSP); %-cost of     offset per time
pTot(iSP)= cTot(iSP)*f(iSP); %-total cost         per time

end %-end defining vars


%% ====================================================Human Metabolic Data
%-Plot Kram experimental data (Kipp, Grabowski and Kram, 2018, JEB)
SpH= [0 8:2:18]/3.6; %-[m/s] Kram exp. speeds
MPH_ave= [ 1.70  9.28 10.90 12.80 15.20 18.30 21.40]; %-[W/kg] met power (ave)
MPH_std= [ 0.18  0.51  0.58  0.81  0.67  1.06  0.99]; %-[W/kg] met power (std)
% MPH_ave= MPH_ave-MPH_ave(1); %-convert gross to net
fsH= [nan 1.35 1.36 1.39 1.42 1.47 1.52]*2; %-[Hz] step frequency (ave)
% MPH_ave= MPH_ave./fsH; MPH_std= MPH_std./fsH; %-convert from W/kg to J/kg/step
DspH= 2:5;



%% ==================================================Choose OptSols to Plot
%-Unified Model per speed/stiffness
SpDsp1= [2.00 2.22 2.50 3.00 3.50 3.89 4.00]; %-speeds for display
krDsp1= [1.30]*k_ref; %-stiffness for display
iC=1;
Dsp{iC}= find(strcmpi(SPps.Name,'PMR') & strcmpi(SPps.StepLength,'G06') &...
              SPps.ForceRateCoef==5e-4 & SPps.BodyMass==70              &...
               SPps.DampingRatio==0.1  & SPps.CollisionFraction==0.03   &...
                SPps.GroundSlope==0    & SPps.RelativeGravity==1        &...
                                         SPps.MaxLegLength==0.90        &...
           ismember(SPps.Speed,SpDsp1) & ismember(SPps.SpringConstant,krDsp1));
[unqVal unqInd]= unique(Sp(Dsp{iC})); Dsp{iC}= Dsp{iC}(unqInd); %-eliminate duplicates
[U I]= sort(Sp(Dsp{iC}),'ascend');    Dsp{iC}= Dsp{iC}(I);      %-ascending order

SpDsp2= [2.50 3.00 3.50]; %-speeds for display
krDsp2= [0.50 0.70 1.00 1.30 1.70 2.00 2.50 3.00 3.50 4.00]*k_ref; %-stiffness for display
for iC=2:length(SpDsp2)+1
Dsp{iC}= find(strcmpi(SPps.Name,'PMR') & strcmpi(SPps.StepLength,'G06') &...
              SPps.ForceRateCoef==5e-4 & SPps.BodyMass==70              &...
               SPps.DampingRatio==0.1  & SPps.CollisionFraction==0.03   &...
                SPps.GroundSlope==0    & SPps.RelativeGravity==1        &...
                                         SPps.MaxLegLength==0.90        &...
     ismember(SPps.Speed,SpDsp2(iC-1)) & ismember(SPps.SpringConstant,krDsp2));
[unqVal unqInd]= unique(kr(Dsp{iC})); Dsp{iC}= Dsp{iC}(unqInd); %-eliminate duplicates
[U I]= sort(kr(Dsp{iC}),'ascend');    Dsp{iC}= Dsp{iC}(I);      %-ascending order
end



%% =============================================================Plot Figure
nR= 1; %-# rows
nC= 6; %-# cols


figure('Name','Fig. 8: Unified Model Cost vs. Speed/Stiffness',...
           'Color','w','NumberTitle','off','Units','Normalized',...
           'Position',[0.1 0.4 0.5 0.3]);



%% =========================================================Plot Model Data
LW=2;

%------------------------------------------------------Model Cost per Speed
subplot(nR,nC,1:3);   NDF= gM(Dsp{1}(1))*L(Dsp{1}(1));
% plot(SpDsp1, cTot(Dsp{1})*NDF, 'r','LineWidth',LW);hold on; %-[J/kg]
plot(SpDsp1, pTot(Dsp{1})*NDF, 'r','LineWidth',LW);hold on; %-[W/kg]
%------------------------------------------------------Human Cost per Speed
nStd=1; BarW= 0.08;
for iSp=DspH
plot(SpH(iSp)*[1 1],MPH_ave(iSp)+nStd*MPH_std(iSp)*[-1 1],'k','LineWidth',1.5); hold on; %-plot Kram std data
plot(SpH(iSp)+BarW*[-1 1],(MPH_ave(iSp)-nStd*MPH_std(iSp))*[1 1],'k','LineWidth',1.5); hold on; %-plot Kram std data
plot(SpH(iSp)+BarW*[-1 1],(MPH_ave(iSp)+nStd*MPH_std(iSp))*[1 1],'k','LineWidth',1.5); hold on; %-plot Kram std data
end
plot(SpH(DspH),MPH_ave(DspH),'kd','MarkerFaceColor','w','MarkerSize',8,'LineWidth',1.5); hold on; %-plot Kram ave data
xlim([2 4]); xticks([2:0.5:4]);
box off;
xlabel('Runing Speed (m s^{-1})');
% ylabel('Energy Cost per Step (J kg^{-1})');   ylim([0 7]);   yticks([0:7]);  %[J/kg]
ylabel('Energy Cost per Time (W kg^{-1})');   ylim([0 24]);   yticks([0:4:24]); %[W/kg]
%--------------------------------------------------------Cost per Stiffness
for iC=2:length(SpDsp2)+1
subplot(nR,nC,iC+2);

ik= find(krDsp2==1.3*k_ref);
% %-plot J/kg
% semilogx(krDsp2/1000, NDF*(cOFS(Dsp{iC})),'-','Color',0.8*ones(1,3),       'LineWidth',1.5);hold on;
% semilogx(krDsp2/1000, NDF*(cOFS(Dsp{iC})+cFRS(Dsp{iC})),               'm','LineWidth',1.5);hold on;
% semilogx(krDsp2/1000, NDF*(cOFS(Dsp{iC})+cFRS(Dsp{iC})+cWrk(Dsp{iC})), 'k','LineWidth',2.5);hold on;
% semilogx(krDsp2(ik)/1000, NDF*(cOFS(Dsp{iC}(ik))+cFRS(Dsp{iC}(ik))+cWrk(Dsp{iC}(ik))),'ko','MarkerFaceColor','r');hold on;
%-plot W/kg
semilogx(krDsp2/1000, NDF*(pOFS(Dsp{iC})),'-','Color',0.8*ones(1,3),       'LineWidth',1.5);hold on;
semilogx(krDsp2/1000, NDF*(pOFS(Dsp{iC})+pFRS(Dsp{iC})),               'm','LineWidth',1.5);hold on;
semilogx(krDsp2/1000, NDF*(pOFS(Dsp{iC})+pFRS(Dsp{iC})+pWrk(Dsp{iC})), 'k','LineWidth',2.5);hold on;
semilogx(krDsp2(ik)/1000, NDF*(pOFS(Dsp{iC}(ik))+pFRS(Dsp{iC}(ik))+pWrk(Dsp{iC}(ik))),'ko','MarkerFaceColor','r');hold on;


xlim([7 200]); xticks([10 100]);
ylim([0   7]); yticks([0:7]);
% ylim([0 7]);   yticks([0:7]); %[J/kg]
ylim([0 24]);   yticks([0:4:24]);  %[W/kg]
box off;
title([num2str(Sp(Dsp{iC}(1)),'%1.1f') ' m s^{-1}']);
if iC==3; xlabel('Spring Stiffness (kN m^{-1})'); end
if iC==2
% ylabel('Energy Cost per Step (J kg^{-1})');
ylabel('Energy Cost per Time (W kg^{-1})');
end
end







end %end function