function PlotS21_Fig09(OPvars, OptSol, SPps)
%====================================================================Inputs
%  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
%  OptSol --> optimal solution
%  SPps   --> structure with model parameters from all OptSols in preset
%==========================================================================
%  This function plots Fig. 9 from presets 'S21', a manuscript on running
%  mechanics and energetics using the 'PMR' model and human data.
%  Schroeder & Kuo, 2021 (planning to submit to PLoS Computational Biology?)



%% ===========================================================Define Params
k_ref= 27370.46501693961;

kr= SPps.SpringConstant/k_ref; %-spring constant (relative)
k = SPps.SpringConstant;       %-spring constant
Sp= SPps.Speed; %-speed
for iSP=1:length(SPps.Name)
d(iSP)= OptSol(iSP).result.setup.auxdata.stpL; %-step length
end
f = Sp./d; %-step frequency
M = SPps.BodyMass; %-body mass
L = SPps.MaxLegLength; %-max leg length
gM= SPps.RelativeGravity*9.81; %-gravitational acceleration
S = SPps.GroundSlope;  %-ground slope
DR= SPps.DampingRatio; %-damping ratio
CF= SPps.CollisionFraction; %-collision fraction
cR= SPps.ForceRateCoef; %-force rate coefficient


%% ===================================================Define Vars for Plots
for iSP=1:length(SPps.Name)
%-time
  t{iSP}= OPvars(iSP).time.time; %-time
  T(iSP)= t{iSP}(end); %-step  time
 to(iSP)= t{iSP}(1);   %-start time
 tf(iSP)= t{iSP}(end); %-end   time
 DF(iSP)= OPvars(iSP).time.dutyFactor;  %-duty factor
 Tc(iSP)= OPvars(iSP).time.contactTime; %-ground contact time
 Tf(iSP)= OPvars(iSP).time.flightTime;  %-flight time
pTO(iSP)= OPvars(iSP).time.events(1,2); %-point of take off

%-kinematics
Th{iSP}= OPvars(iSP).kinematics.pos.legAngle;  %-leg angle, CCW from +--> (global)
Ll{iSP}= OPvars(iSP).kinematics.pos.legLength; %-leg length
if strcmpi(OPvars(iSP).name,'PMR')
Lt{iSP}= OPvars(iSP).kinematics.pos.springLength;
Lm{iSP}= OPvars(iSP).kinematics.pos.actuatorLength;
end
xb{iSP}= OPvars(iSP).kinematics.pos.body(:,1); %-body hor pos
yb{iSP}= OPvars(iSP).kinematics.pos.body(:,2); %-body vrt pos
dy_dx(iSP)= diff(yb{iSP}([1 end]))/diff(xb{iSP}([1 end])); %-"measured" slope

if strcmpi(OPvars(iSP).name,'PMR')
xm{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,1); %-actuator end away from ground (hor)
ym{iSP}= OPvars(iSP).kinematics.pos.actuatorEnd(:,2); %-actuator end away from ground (vrt)
end
 xf(iSP)= OPvars(iSP).kinematics.pos.foot(:,1); %-foot hor pos
 yf(iSP)= OPvars(iSP).kinematics.pos.foot(:,2); %-foot vrt pos
Thd{iSP}= OPvars(iSP).kinematics.vel.legAngle;  %-leg angular velocity, CCW +
Lld{iSP}= OPvars(iSP).kinematics.vel.legLength; %-leg velocity
xbd{iSP}= OPvars(iSP).kinematics.vel.body(:,1); %-body hor pos
ybd{iSP}= OPvars(iSP).kinematics.vel.body(:,2); %-body vrt pos



if strcmpi(OPvars(iSP).name,'PMR')
Ltd{iSP}= OPvars(iSP).kinematics.vel.springLength;   %-spring vel
Lmd{iSP}= OPvars(iSP).kinematics.vel.actuatorLength; %-actuator vel
end
Lldd{iSP}= OPvars(iSP).kinematics.acc.leglength; %-leg acc
xbdd{iSP}= OPvars(iSP).kinematics.acc.body(:,1); %-hor body acc
ybdd{iSP}= OPvars(iSP).kinematics.acc.body(:,2); %-vrt body acc
if strcmpi(OPvars(iSP).name,'PMR');      Lmdd{iSP}= OPvars(iSP).kinematics.acc.actuatorLength; else;  Lmdd{iSP}=nan; end
if isfield(OPvars(iSP).kinetics,'jrk'); Llddd{iSP}= OPvars(iSP).kinematics.jrk.legLength;      else; Llddd{iSP}=nan; end
if strcmpi(OPvars(iSP).name,'PMR');     Lmddd{iSP}= OPvars(iSP).kinematics.jrk.actuatorLength; else; Lmddd{iSP}=nan; end


%-kinetics
 Rx{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,1); %-hor GRF
 Ry{iSP}=  OPvars(iSP).kinetics.groundRxnForce(:,2); %-vrt GRF
 Fm{iSP}=  OPvars(iSP).kinetics.actuatorForce;       %-Actuator force
Fmd{iSP}=  OPvars(iSP).kinetics.actuatorForce_ddt1;  %-d/dt actuator force
if isfield(OPvars(iSP).kinetics,'actuatorForce_ddt2'); Fmdd{iSP}= OPvars(iSP).kinetics.actuatorForce_ddt2; else; Fmdd{iSP}=nan; end %-d2/dt2 actuator force
if isfield(OPvars(iSP).kinetics,'springForce');        Fs{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fs{iSP}  =nan; end %-Spring force
if isfield(OPvars(iSP).kinetics,'damperForce');        Fd{iSP}  = OPvars(iSP).kinetics.springForce;        else; Fd{iSP}  =nan; end %-Damper force


%-energetics
negMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(1); %-neg actuator work
posMscWrk(iSP)= OPvars(iSP).energetics.work.actuator(2); %-pos actuator work
absMscWrk(iSP)= posMscWrk(iSP)-negMscWrk(iSP); %-abs actuator work
netMscWrk(iSP)= posMscWrk(iSP)+negMscWrk(iSP); %-net actuator work
if strcmpi(OPvars(iSP).name,'PMR')
negSprWrk(iSP)= OPvars(iSP).energetics.work.spring(1); %-neg spring work
posSprWrk(iSP)= OPvars(iSP).energetics.work.spring(2); %-pos spring work
negDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(1); %-neg damper work
posDmpWrk(iSP)= OPvars(iSP).energetics.work.damper(2); %-pos damper work
end
Pm{iSP}= OPvars(iSP).energetics.power.actuator; %-actuator power
if strcmpi(OPvars(iSP).name,'PMR')
Ps{iSP}= OPvars(iSP).energetics.power.spring; %-spring power
Pd{iSP}= OPvars(iSP).energetics.power.damper; %-damper power
Pn{iSP}= Pm{iSP}+Ps{iSP}+Pd{iSP}; %-net power
HysLoss(iSP)= -trapz(t{iSP},Pd{iSP}); %[J] hysteresis loss magnitude
elseif strcmpi(OPvars(iSP).name,'srinR')
Pn{iSP}= Pm{iSP}; %-net power
end

vbf(iSP)= sqrt(xbd{iSP}(end)^2+ybd{iSP}(end)^2); %-final   body velocity before TD
vbi(iSP)= sqrt(xbd{iSP}( 1 )^2+ybd{iSP}( 1 )^2); %-initial body velocity after  TD
dKE(iSP)= 0.5*M(iSP)*(vbi(iSP)^2-vbf(iSP)^2); %-change in KE from final to initial
ColLoss(iSP)= -dKE(iSP); %[J] collision loss magnitude

%-cost
cTot_ch(iSP)= OPvars(iSP).cost.total; %-total cost
cWrk_ch(iSP)= OPvars(iSP).cost.work;  %-work cost
e= [0.32 -1.05]; %-muscle efficiency for positive/negative work
cNMW(iSP)= (negMscWrk(iSP)/e(2))./(M(iSP)*gM(iSP)*L(iSP)); %-neg msc work cost
cPMW(iSP)= (posMscWrk(iSP)/e(1))./(M(iSP)*gM(iSP)*L(iSP)); %-pos msc work cost
cWrk(iSP)= cNMW(iSP)+cPMW(iSP);           %-work       cost
cFRS(iSP)= OPvars(iSP).cost.forceRate;    %-force rate cost
cOFS(iSP)= 0.08*2.94./f(iSP);             %-offset     cost
cTot(iSP)= cOFS(iSP)+cWrk(iSP)+cFRS(iSP); %-total      cost

end %-end defining vars


%% ====================================================Human Metabolic Data
%-Plot Margaria experimental data (Margaria, 1968, Int J Appl Physiol Incl Occup Physiol)
%-Data digitized from Fig. 3 with web app: https://automeris.io/WebPlotDigitizer/
%-----------Slope (dy/dx)-------Cost (J/kg)---
MrgDat= [-0.201805416248746, 2.861067340567859
         -0.152858575727182, 2.576737666971054
         -0.102306920762287, 2.256866784174647
         -0.049348044132397, 2.985461572766466
          0.001203610832497, 3.802909384357283
          0.052557673019057, 4.833604451145705
          0.102306920762287, 6.130858586931134
          0.152858575727182, 7.570277559514965];


%% ==================================================Choose OptSols to Plot
%-Unified Model per speed/stiffness
GSnom= -0.25:0.05:0.25;
GSadd= [{[-0.08 -0.07]},...
        {[]}  {[]} {[]},...
        {[-0.08 -0.07]} {[]} {[]},...
        {[-0.03 -0.01]} {[-0.08 -0.07]} {[-0.13 -0.08]},...
        {[-0.03      ]} {[-0.08 -0.07]} {[-0.08 -0.07]}];
for iS=1:length(GSadd)
GSDsp{iS}= sort([GSnom GSadd{iS}],'ascend'); %-slopes for display
end
krDsp= [1.30   0.50 2.00  inf   1.30 1.30 1.30   1.30 1.30 1.30   1.30 1.30 1.30]*k_ref; %-stiffness for display
cRDsp= [5e-4   5e-4 5e-4 5e-4   0.00 5e-4 2e-3   5e-4 5e-4 5e-4   5e-4 5e-4 5e-4]; %-force-rate coef for display
DRDsp= [0.10   0.10 0.10 0.00   0.10 0.10 0.10   0.00 0.10 0.20   0.10 0.10 0.10]; %-damping ratio   for display
CFDsp= [0.03   0.03 0.03 0.00   0.03 0.03 0.03   0.03 0.03 0.03   0.00 0.03 0.06]; %-collision fract for display
MNDsp= [repelem({'PMR'},1,3) {'srinR'} repelem({'PMR'},1,9)];                      %-model name      for display


for iS=1:length(GSDsp) %-iterate display series
Dsp{iS}= find(strcmpi(SPps.Name,MNDsp{iS}) & strcmpi(SPps.StepLength,'G06')    &...
            SPps.ForceRateCoef==cRDsp(iS)  & SPps.BodyMass==70                 &...
             SPps.DampingRatio==DRDsp(iS)  & SPps.CollisionFraction==CFDsp(iS) &...
  SPps.RelativeGravity==1 & SPps.Speed==3  & SPps.MaxLegLength==0.90           &...
           SPps.SpringConstant==krDsp(iS)  & ismember(SPps.GroundSlope,GSDsp{iS}));
[unqVal unqInd]= unique(S(Dsp{iS})); Dsp{iS}= Dsp{iS}(unqInd); %-eliminate duplicates
[U I]= sort(S(Dsp{iS}),'ascend');    Dsp{iS}= Dsp{iS}(I);      %-ascending order
end


%% =============================================================Plot Figure
nR= 6; %-# rows
nC= 4; %-# cols


figure('Name','Fig. 9: Unified Model Cost vs. Ground Slope',...
           'Color','w','NumberTitle','off','Units','Normalized',...
           'Position',[0.1 0.1 0.5 0.6]);



%% =========================================================Plot Model Data
LW=1.5;

%-----------------------------------------Compare Cost with Margaria's Data
%----------------------------------------------------------------Model Data
subplot(nR,nC,[1 2 5 6 9 10]);   NDF= L(Dsp{1}(1))/d(Dsp{1}(1));
plot([-0.4 0 0.4],sind(atan2d(0.4,1))*[5/6 0 4],'k','LineWidth',0.5);hold on; %-muscle efficiency assymptotes (25%,-120%)
plot(dy_dx(Dsp{1}), cTot(Dsp{1})*NDF, 'k','LineWidth', 3);hold on; %-model
%----------------------------------------------------------------Human Data
NDF=(gM(Dsp{1}(1))*d(Dsp{1}(1))); %-convert [J/kg] to [J/(Mgd)] i.e. nonDim CoT
plot(MrgDat(:,1),MrgDat(:,2)/NDF,'ko','MarkerFaceColor','w','LineWidth',1);hold on;
xlim([-0.4 0.4]); xticks([-0.4:0.2:0.4]);
ylim([0 1.2]);    yticks([]);   box off;
xlabel('Ground Slope');   ylabel('Cost of Transport');

%-cost units bar
plot(0.2*[1 1],[0.1 0.3],'k','LineWidth',1);hold on;
text(0.22,0.2,'0.2','VerticalAlignment','Middle','HorizontalAlignment','Left');
%---------------------------------------------------Model Cost Constiutents
subplot(nR,nC,[1 2 5 6 9 10]+2);   NDF= L(Dsp{1}(1))/d(Dsp{1}(1));
plot(dy_dx(Dsp{1}), cOFS(Dsp{1})*NDF, ':  ','LineWidth',LW,'Color',0.8*ones(1,3));hold on;
plot(dy_dx(Dsp{1}), cNMW(Dsp{1})*NDF, 'b-.','LineWidth',LW);hold on;
plot(dy_dx(Dsp{1}), cPMW(Dsp{1})*NDF, 'r--','LineWidth',LW);hold on;
plot(dy_dx(Dsp{1}), cFRS(Dsp{1})*NDF, 'm: ','LineWidth',LW);hold on;
plot(dy_dx(Dsp{1}), cTot(Dsp{1})*NDF, 'k- ','LineWidth', 3);hold on;
xlim([-0.4 0.4]); xticks([-0.4:0.2:0.4]);
ylim([0 1.2]);    yticks([]);   box off;
xlabel('Ground Slope');   ylabel('Cost of Transport');

%-cost units bar
plot(-0.35*[1 1],[0.9 1.1],'k','LineWidth',1);hold on;
text(-0.33,1.0,'0.2','VerticalAlignment','Middle','HorizontalAlignment','Left');
%--------------------------------------Model Cost Sensititivty to Stiffness
SNC= [-1 repelem([0 1 2 3],1,3)]; %-series number columns
LS = [{'-'} repmat([{':'} {'-.'} {'--'}],1,4)]; %-line styles
LW = [1 repmat([1.5 1.5 2.0],1,4)]; %-line widths
tts= [1 repelem({'Stiffness, k'},1,3)   repelem({'Force-rate, \epsilon'},1,3),...
        repelem({'Damping, \zeta'},1,3) repelem({'Collision, CF'},1,3)]; %-plot titles
for iS=2:length(Dsp)
subplot(nR,nC,17+[0 nC]+SNC(iS));   NDF= L(Dsp{1}(1))/d(Dsp{1}(1));
if SNC(iS)>SNC(iS-1)
plot([-0.4 0 0.4],sind(atan2d(0.4,1))*[5/6 0 4],'k','LineWidth',0.5);hold on; %-muscle efficiency assymptotes (25%,-120%)
end
plot(dy_dx(Dsp{iS}), cTot(Dsp{iS})*NDF, ['k' LS{iS}],'LineWidth',LW(iS));hold on;
xlim([-0.4 0.4]); xticks([]);
ylim([0 1.2]);    yticks([]);   box off;   axis square;
xlabel('Ground Slope');
if iS==2;   ylabel('Cost of Transport');   end
if SNC(iS)>SNC(iS-1);   title(tts{iS});    end
end










end %-end function