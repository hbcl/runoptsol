function [data]= RepresentativeHumanRunningSubject()
%====================================================================Inputs
% %  OPvars --> structure with vars to plot over SP (e.g. cost versus speed)
% %  OptSol --> optimal solution
% %  SPps   --> structure with model parameters from all OptSols in preset
%==========================================================================
%  This function loads and outputs human running data from a single subject
%  used in Fig. 7 from presets 'S21', based on a manuscript on running
%  mechanics and energetics.
%  Schroeder & Kuo, 2021 (planning to submit to PLoS Computational Biology?)



%% ========================================================Load/Define Data
iS=3; %-sub#
fn= 'S03D1H1.txt'; Sp=3.9;%-vel=3.9 m/s

fID= fopen(fn);
Headers= textscan(fID,'%s',14,'Delimiter',',');
DAQr= str2num(Headers{1}{2}); %[Hz] data acquisition rate

data_all= textscan(fID,'%f %f %f %f','Delimiter',',');
t = (0:length(data_all{1})-1)/DAQr; %-[s] time
Fx= -data_all{3}; %-[N] hor GRF, (Fy in text doc)
Fy= -data_all{4}; %-[N] vrt GRF, (Fz in text doc)

g= 9.81; %[m/s2] gravitational acceleration
M= mean(Fy)/g; %[kg] measured body mass


%% ==========================================================CoM Kinematics
xbdd= Fx/M;                  %[m/s2] CoM hor acc
ybdd= Fy/M - g;              %[m/s2] CoM vrt acc
xbd = cumtrapz(t,xbdd) + Sp; %[m/s ] CoM hor vel
ybd = cumtrapz(t,ybdd);      %[m/s ] CoM vrt vel
xb  = cumtrapz(t,xbd );      %[m   ] CoM hor pos
yb  = cumtrapz(t,ybd );      %[m   ] CoM vrt pos


%% ==============================================================Energetics
Wd= Fy.*ybd + Fx.*xbd; %[W] CoM power


%% ==================================================Measure Step Frequency
if Sp==2.8 && iS==3
pat= [567 0 747 0];
elseif Sp==3.9 && iS==3
pat= [184 0 659 0];
end
cycSP= [  184   844  1516  2181  2852  3533  4205  4868  5532  6199  6863,...
         7501  8176  8854  9532 10199 10869 11544 12228 12905 13589 14243,...
        14923 15591 16248 16912 17584 18245 18907 19576 20251 20912 21583,...
        22243 22903 23562 24242 24902 25577 26229 26897 27555 28210 28866,...
        29536 30187 30843 31491 32167 32834 33509 34185 34859 35516 36198,...
        36876 37547 38225 38883 39566]; %-step cycle start points
if exist('cycSP','var')==0
[cycSP cycPTSint HS PO]= BasicGaitAnalysis(Fy+(rand(size(Fy))-0.5)/(10^6),'run','CoM','max','manual',DAQr,pat,0);clear HS PO;
end

fs= DAQr./diff(cycSP); %[Hz] all step frequency
fs_std=    std(fs);    %[Hz] std step frequency
fs_ave=   mean(fs);    %[Hz] ave step frequency
fs_med= median(fs);    %[Hz] med step frequency

Ts    =     1./fs;     %[s] step time
Ts_std=    std(Ts);    %[s] std step time
Ts_ave=   mean(Ts);    %[s] ave step time
Ts_med= median(Ts);    %[s] med step time

ds    =    Sp./fs;  %[m] all step length
ds_std=    std(ds); %[m] std step length
ds_ave=   mean(ds); %[m] ave step length
ds_med= median(ds); %[m] med step length


%% =============================Determine TD/TO pts and stance/flight times
nC=length(cycSP);
for iC=1:nC
TD(iC)= max(find(Fy(1:cycSP(iC))<0.02*M*g)); %-touchdown time points
end
for iC=2:nC
TO(iC-1)= min(find(Fy(TD(iC-1)+20:TD(iC))<0.02*M*g))+TD(iC-1)+19; %-takeoff time points
end

Tg= (TO-TD(1:length(TO)))/DAQr; %[s] stance time (on ground)
Tg_std=    std(Tg);    %[s] std stance time
Tg_ave=   mean(Tg);    %[s] ave stance time
Tg_med= median(Tg);    %[s] med stance time

Tf= Ts-Tg; %[s] flight time (in air)
Tf_std=    std(Tf);    %[s] std flight time
Tf_ave=   mean(Tf);    %[s] ave flight time
Tf_med= median(Tf);    %[s] med flight time

DF= Tg./Ts; %[#] duty factor
DF_std=    std(DF);    %[s] std duty factor
DF_ave=   mean(DF);    %[s] ave duty factor
DF_med= median(DF);    %[s] med duty factor


%% =================================================Segment data into steps
t_ntp= linspace(0,Ts_ave,round(Ts_ave*DAQr));
for iC=1:nC-1
Fx_ntp(:,iC)= interp1(TD(iC):TD(iC+1),Fx(TD(iC):TD(iC+1)),linspace(TD(iC),TD(iC+1),round(Ts_ave*DAQr))); %[N] interpolated hor GRF
Fy_ntp(:,iC)= interp1(TD(iC):TD(iC+1),Fy(TD(iC):TD(iC+1)),linspace(TD(iC),TD(iC+1),round(Ts_ave*DAQr))); %[N] interpolated vrt GRF

xbdd_ntp(:,iC)= interp1(TD(iC):TD(iC+1),xbdd(TD(iC):TD(iC+1)),linspace(TD(iC),TD(iC+1),round(Ts_ave*DAQr))); %[m/s2] interpolated hor acc
ybdd_ntp(:,iC)= interp1(TD(iC):TD(iC+1),ybdd(TD(iC):TD(iC+1)),linspace(TD(iC),TD(iC+1),round(Ts_ave*DAQr))); %[m/s2] interpolated vrt acc
 xbd_ntp(:,iC)= interp1(TD(iC):TD(iC+1), xbd(TD(iC):TD(iC+1)),linspace(TD(iC),TD(iC+1),round(Ts_ave*DAQr))); %[m/s ] interpolated hor vel
 ybd_ntp(:,iC)= interp1(TD(iC):TD(iC+1), ybd(TD(iC):TD(iC+1)),linspace(TD(iC),TD(iC+1),round(Ts_ave*DAQr))); %[m/s ] interpolated vrt vel
  xb_ntp(:,iC)= interp1(TD(iC):TD(iC+1),  xb(TD(iC):TD(iC+1)),linspace(TD(iC),TD(iC+1),round(Ts_ave*DAQr))); %[m   ] interpolated hor pos
  yb_ntp(:,iC)= interp1(TD(iC):TD(iC+1),  yb(TD(iC):TD(iC+1)),linspace(TD(iC),TD(iC+1),round(Ts_ave*DAQr))); %[m   ] interpolated vrt pos
  
Wd_ntp(:,iC)= interp1(TD(iC):TD(iC+1),Wd(TD(iC):TD(iC+1)),linspace(TD(iC),TD(iC+1),round(Ts_ave*DAQr))); %[N] interpolated hor GRF
end
xb_ntp= xb_ntp-xb_ntp(1,:);

Fx_std=    std(Fx_ntp,0,2); %[N] std hor GRF
Fx_ave=   mean(Fx_ntp,  2); %[N] ave hor GRF
Fx_med= median(Fx_ntp,  2); %[N] med hor GRF
Fy_std=    std(Fy_ntp,0,2); %[N] std vrt GRF
Fy_ave=   mean(Fy_ntp,  2); %[N] ave vrt GRF
Fy_med= median(Fy_ntp,  2); %[N] med vrt GRF

TD_ave= 1;
TO_ave= max(find(Fy_ave>0.01*M*g))+1;

xbdd_std=    std(xbdd_ntp,0,2); %[m/s2] std hor acc
xbdd_ave=   mean(xbdd_ntp,  2); %[m/s2] ave hor acc
xbdd_med= median(xbdd_ntp,  2); %[m/s2] med hor acc
ybdd_std=    std(ybdd_ntp,0,2); %[m/s2] std vrt acc
ybdd_ave=   mean(ybdd_ntp,  2); %[m/s2] ave vrt acc
ybdd_med= median(ybdd_ntp,  2); %[m/s2] med vrt acc

xbd_std=    std(xbd_ntp,0,2); %[m/s] std hor vel
xbd_ave=   mean(xbd_ntp,  2); %[m/s] ave hor vel
xbd_med= median(xbd_ntp,  2); %[m/s] med hor vel
ybd_std=    std(ybd_ntp,0,2); %[m/s] std vrt vel
ybd_ave=   mean(ybd_ntp,  2); %[m/s] ave vrt vel
ybd_med= median(ybd_ntp,  2); %[m/s] med vrt vel

xb_std=    std(xb_ntp,0,2); %[m] std hor pos
xb_ave=   mean(xb_ntp,  2); %[m] ave hor pos
xb_med= median(xb_ntp,  2); %[m] med hor pos
yb_std=    std(yb_ntp,0,2); %[m] std vrt pos
yb_ave=   mean(yb_ntp,  2); %[m] ave vrt pos
yb_med= median(yb_ntp,  2); %[m] med vrt pos

Wd_std=    std(Wd_ntp,0,2); %[m] std CoM power
Wd_ave=   mean(Wd_ntp,  2); %[m] ave CoM power
Wd_med= median(Wd_ntp,  2); %[m] med CoM power

Fy_ave_acc=cumtrapz(t_ntp,Fy_ave);
M_stp= (Fy_ave_acc(end)/Ts_ave)/g; %[kg] body mass given ave Fy_stp = body weight impulse

xf_ave= xb_ave(find(min(abs(Fx_ave(1:TO_ave-1)))==abs(Fx_ave(1:TO_ave-1)))); %ft position, assume body is directly over ft when Fx=0
yb_des= sqrt(0.9^2 - (xb_ave(TO_ave)-xf_ave)^2); %yb @TO assuming a fully extended leg of 0.9 m length
y_shift= yb_des - yb_ave(TO_ave);
yb_ave_adj= yb_ave + y_shift;



%% ===================================================Define data structure
data.traj= [xb_ave   yb_ave_adj]; %-vrt CoM pos vs. hor
data.vGRF= [t_ntp'   Fy_ave]; %-vrt GRF     vs. time
data.comP= [t_ntp'   Wd_ave]; %-CoM power   vs. time
data.loop= [yb_ave ybdd_ave]; %-vrt acc     vs. vrt disp
data.fpos= [         xf_ave]; %-foot position
data.evnt= [1 TO_ave length(t_ntp)]; %-1st point, TO pt and end pt






end %-end function