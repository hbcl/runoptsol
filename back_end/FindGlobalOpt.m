function [output, ProbSet, auxdata, Pseed]= FindGlobalOpt(Model, ProbSet, auxdata, output)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> structure with problem parameters
%  output  --> structure with optimal solutions from every iteration in field "RG"
%===================================================================Outputs
%  output  --> structure with optimal solutions from every iteration in field "RG"
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  auxdata --> input auxdata updated with random initial guesses
%  Pseed   --> lowest cost solution in output.RG ("global" optimum)
%==========================================================================
%  This function provides random initial guesses to test for "global" optimality


hb= auxdata.h__b;
g = auxdata.grav;
k = auxdata.sprC;
c = auxdata.dmpC;
SPmat= ProbSet.SPmat;
iSP  = ProbSet.iSP;
TempFiles=  ProbSet.TempFiles;
TempFolder= ProbSet.TempFolder;

iRG= ProbSet.RG.StartIteration;
nRG= ProbSet.RG.TotalIteration;
if isfield(ProbSet.RG,'CoT'); CoT_RG= ProbSet.RG.CoT; end
if isfield(ProbSet.RG, 'EF');  EF_RG= ProbSet.RG.EF;  end
if isfield(ProbSet.RG,'seeds'); OptSolSeeds.RG= ProbSet.RG.seeds; end
resTHRG=     ProbSet.RG.DownsampleRate;
MaxSNOPTi=   ProbSet.RG.MaxSNOPTIteration;
MaxMeshiMat= ProbSet.RG.MaxMeshIteration;

iPP= ProbSet.PP.StartIteration;
nPP= ProbSet.PP.TotalIteration;
if isfield(ProbSet.PP,'CoT'); CoT_PP= ProbSet.PP.CoT; end
if isfield(ProbSet.PP, 'EF');  EF_PP= ProbSet.PP.EF;  end
if isfield(ProbSet.PP,'seeds'); OptSolSeeds.PP= ProbSet.PP.seeds; end


auxdata.MeshTol   = ProbSet.RG.MeshTolerance;
auxdata.SNOPTtol  = ProbSet.RG.SNOPTTolerance;
auxdata.MaxSNOPTi = ProbSet.RG.MaxSNOPTIteration;


fnID=   ProbSet.fnID;
SaveOP= ProbSet.SaveOP;
Home=   ProbSet.Home;

OptTime= ProbSet.OptTime;
OptWhen= ProbSet.OptWhen;

OptTimeAve=180;
%% ================================================Phase I - Random Guesses
while iRG <= nRG
    iMesh=1;
    while iMesh<=length(MaxMeshiMat)
    rng('shuffle');
%         clc;
% %         fprintf('sweeping parameter number'); %iSP
%         fprintf('\n\n');
%         fprintf('random guesses number'); %iRG
%         fprintf('\n\n');
%         fprintf('max mesh iterations'); MaxMeshiMat(iMesh)
%         fprintf('\n\n\n');
    auxdata.MaxMeshi = MaxMeshiMat(iMesh); % mesh max iterations

% ==============================================================Setup GPOPS
if iMesh==1

%-initial guesses
auxdata= feval([Model.Name '_InitGuess'],ProbSet,auxdata);


elseif iMesh>1
if isfield(output.RG(iRG,iMesh-1).result.solution,'parameter')
auxdata.guess.parameter= output.RG(iRG,iMesh-1).result.solution.parameter;
end

for i=length(output.RG.result.solution.phase)
tf  = output.RG(iRG,iMesh-1).result.solution.phase(i).time(end) - ...
      output.RG(iRG,iMesh-1).result.solution.phase(i).time(  1);
nact= length(output.RG(iRG,iMesh-1).result.solution.phase(i).time);
nthr= round(tf/resTH);
if nact-nthr>2
DSlogic= ones(size(output.RG(iRG,iMesh-1).result.solution.phase(i).time))==1;
ndel= nact-nthr;
nskp= round((nact-2)/(ndel-1));
DSlogic(2:nskp:end-1)= false;
auxdata.guess.phase(i).time     = output.RG(iRG,iMesh-1).result.solution.phase(i).time(DSlogic);
auxdata.guess.phase(i).state    = output.RG(iRG,iMesh-1).result.solution.phase(i).state(DSlogic,:);
auxdata.guess.phase(i).control  = output.RG(iRG,iMesh-1).result.solution.phase(i).control(DSlogic,:);
auxdata.guess.phase(i).integral = output.RG(iRG,iMesh-1).result.solution.phase(i).integral;
else
auxdata.guess.phase(i).time     = output.RG(iRG,iMesh-1).result.solution.phase(i).time;
auxdata.guess.phase(i).state    = output.RG(iRG,iMesh-1).result.solution.phase(i).state;
auxdata.guess.phase(i).control  = output.RG(iRG,iMesh-1).result.solution.phase(i).control;
auxdata.guess.phase(i).integral = output.RG(iRG,iMesh-1).result.solution.phase(i).integral;
end
end
end


%-overwrite initial guesses with seeds field in 'RAOS' mode
if strcmpi(Model.Preset,'RAOS') && isfield(ProbSet.RG,'Seeds')
auxdata.guess= ProbSet.RG.Seeds(iRG,iMesh);
end


% =============================================================Run GPOPS-II
ProbSet.ttic =        tic; % beginning time
ProbSet.tave = OptTimeAve; % Ave. time for Opt Sol

LatestOutput = feval(Model.Name,auxdata); %-call model in GPOPS-II

if isfield(LatestOutput,'warnings')==0
LatestOutput.warnings = cell(1);
end
output.RG(iRG,iMesh)= LatestOutput;   clear LatestOutput;
OptSolSeeds.RG(iRG,iMesh)= output.RG(iRG,iMesh).result.setup.auxdata.guess;
OptTime = [OptTime toc(ProbSet.ttic)];
OptWhen = [OptWhen;datestr(now)];
OptWhenCur = OptWhen(end,:);

if SaveOP==1
   if isempty(Model.SaveName)
   fsave= [Home '/OptSols/working' fnID(findstr(fnID,'OptSol')+6:end) '.mat'];
   else
   if strcmpi(Model.SaveName(end-3:end),'.mat')
   fsave= [Home '/OptSols/working_' Model.SaveName];    
   else
   fsave= [Home '/OptSols/working_' Model.SaveName '.mat'];
   end
   end
   
   if ispc; fsave= strrep(fsave,'/','\'); end
   save(fsave,'output');
end


% ---------------------------------------------------Cost of Opt Sol Output
CoT_RG(iRG,iMesh,iSP) = output.RG(iRG,iMesh).result.objective;
 EF_RG(iRG,iMesh,iSP) = output.RG(iRG,iMesh).result.nlpinfo;
              EF_cRG  = EF_RG(iRG,iMesh,iSP);
 
 
    if EF_RG(iRG,iMesh,iSP)==1 || EF_RG(iRG,iMesh,iSP)==2
          if toc(ProbSet.ttic)<=4*ProbSet.tave
          iMesh=iMesh+1;
          else
          iMesh=1;
          end
    else
          iMesh=1;
    end
        OptTimeAve=   mean(OptTime);
        OptTimeNum= length(OptTime);
    
    AllSol   = output;
    fileN    = TempFiles{1};
    fileNaux = 'MonAux.mat';
    if ispc; s='\'; else s='/'; end
    if exist([Home TempFolder s fileN])==0
    save([Home TempFolder s fileN],'AllSol');
    save([Home TempFolder s fileNaux],'SPmat','iSP','iRG','iPP','iMesh','OptTimeAve','OptTimeNum','OptWhenCur','EF_cRG','fnID');
    else
    save([Home TempFolder s fileN],'AllSol','-append');
    save([Home TempFolder s fileNaux],'SPmat','iSP','iRG','iPP','iMesh','OptTimeAve','OptTimeNum','OptWhenCur','EF_cRG','fnID','-append');
    end
    clear AllSol;
    OptWhen(end,:)='';
    end
iMesh=length(MaxMeshiMat);


    if EF_RG(iRG,iMesh,iSP)==1 || EF_RG(iRG,iMesh,iSP)==2
          iRG=iRG+1;
    end
end
iRG=nRG;

ProbSet.RG.iteration= iRG;
ProbSet.RG.CoT= CoT_RG;
ProbSet.RG.EF =  EF_RG;
ProbSet.RG.seeds= OptSolSeeds.RG;
ProbSet.OptWhen= OptWhen;
ProbSet.OptTime= OptTime;


% Define P-seed as lowest cost output from Random Guesses (RG)
if nRG>0
MinCoT_RG= min( min(CoT_RG(:,:,iSP)));
MinLoc_RG= min(find(CoT_RG(:,:,iSP)==MinCoT_RG));
Pseed = output.RG(MinLoc_RG);
if contains(fnID,'_FreeBnds')
    auxdata.jrkB= inf;
    Pseed.result.setup.auxdata.jrkB= inf;
    Pseed.result.setup.bounds.phase.control.lower(1)= -inf;
    Pseed.result.setup.bounds.phase.control.upper(1)=  inf;
end
else
    fprintf('Please select file containing desired Pseed OptSol.\n');
    [fnLoad pathLoad] = uigetfile('*.*');
    clc;
    load([pathLoad fnLoad]);
    Pseed = OptSolCur;
end









end