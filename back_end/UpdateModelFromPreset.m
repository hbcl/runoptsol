function [Model]= UpdateModelFromPreset(View, Model, ProbSet, SPps)
%====================================================================Inputs
%  Model   --> structure with model parameters
%  ProbSet --> structure with problem setup information (e.g. tolerances)
%  SPps    --> structure with model parameters from all OptSols in preset
%===================================================================Outputs
%  Model   --> structure with model parameters
%==========================================================================
%  This function overwrites model parameters in order to plot preset


iSP= ProbSet.iSP;


%-which preset?
if strcmpi(View.Preset,'S21') %-OptSols from Schroeder & Kuo, 2021

S21= SPps;

% if Model.Name

Model.Name=              S21.Name{iSP}; %-Model name code
Model.ScaleWork=         S21.ScaleWork{iSP}; %-Scale actuator work
Model.ForceRateCoef=     S21.ForceRateCoef(iSP); %-Force-rate cost coefficient
Model.DampingRatio=      S21.DampingRatio(iSP); %-Tendon damping ratio
Model.CollisionFraction= S21.CollisionFraction(iSP); %-Fraction of CoM momentum lost in collision
Model.SpringConstant=    S21.SpringConstant(iSP); %-Spring constant
Model.GroundSlope=       S21.GroundSlope(iSP); %-Ground slope as fraction
Model.RelativeGravity=   S21.RelativeGravity(iSP); %-Gravity realtive to Earth
Model.Speed=             S21.Speed(iSP); %-Running speed
Model.StepLength=        S21.StepLength{iSP}; %-Step length
Model.BodyMass=          S21.BodyMass(iSP); %-Body mass
Model.MaxLegLength=      S21.MaxLegLength(iSP); %-Maximum allowable leg length
% Model.SaveName=          S21.SaveName; %-Name you would like to save optimization output as (input: character array), default= fnID
% Model.Preset=            S21.Preset; %-Define problem parameters from presets (see ReadMe for options)
% Model.SPmat=             S21.SPmat; %-For multiple optimizations, define SPmat with desired field in structure Model

elseif ~isempty(View.Preset) %-unknown preset
error(['Unknown View.Preset: ' View.Preset]);

end




end %-end function