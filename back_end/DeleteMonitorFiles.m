function DeleteMonitorFiles(ProbSet)
%====================================================================Inputs
%  ProbSet    --> structure with problem setup information (e.g. tolerances)
%===================================================================Outputs
%
%==========================================================================
%  This function deletes any existing monitor files

Home=       ProbSet.Home;
TempFiles=  ProbSet.TempFiles;
TempFolder= ProbSet.TempFolder;


iRG= ProbSet.RG.StartIteration;
nRG= ProbSet.RG.TotalIteration;
iPP= ProbSet.PP.StartIteration;
nPP= ProbSet.PP.TotalIteration;

if iRG<nRG && iPP<nPP
% Delete Pre-existing Monitor Files
fileN = TempFiles{1};
fileNaux = 'MonAux.mat';
if exist([Home TempFolder '\' fileN])~=0
  delete([Home TempFolder '\' fileN]);
end
fileN = TempFiles{2};
if exist([Home TempFolder '\' fileN])~=0
  delete([Home TempFolder '\' fileN]);
end
if exist([Home TempFolder '\' fileNaux])~=0
  delete([Home TempFolder '\' fileNaux]);
end
end



end





