clc;   clearvars;   close all;   ProbSet= SetDirectory;
%%=======================================================================%%
% This code plots figures for optimizations of locomotion models          %
% including "PMR" and "srinR", referred to as "Actuated Spring-mass" and  %
% "Actuator-only" in the manuscript: "Elastic energy savings and active   %
% energy cost in a simple model of running", PLoS ONE, Comp Bio, 2021.    %
% Authors: Ryan T. Schroeder & Arthur D. Kuo                              %
% Code by: Ryan T. Schroeder (last updated: May 6, 2021)                  %
% Faculty of Kinesiology, University of Calgary, Calgary, Alberta, Canada %
%%=======================================================================%%
%%=======================================================================%%
%===========================START: USER INPUT=============================%
%%==========================vvvvvvvvvvvvvvvvv============================%%
%%=============================vvvvvvvvvvv===============================%%
%%================================vvvvv==================================%%
%%==================================v====================================%%
%%=======================================================================%%



%% ------------------------------------------Select View Settings, (0 or 1)
View.Preset=  'S21'; %-preset figures (e.g. from published manuscripts, see ReadMe, overrides all settings below, leave empty to use parameters to specify OptSol below)
View.PlotSummary= 0; %-plot basic outputs (trajectory, ground reaction forces, etc.) (# of figures = length(Model.SPmat))
View.Cost=        0; %-plot cost constituents versus SPmat (# of figures = 1)
View.DutyFactor=  0; %-plot duty factor versus SPmat       (# of figures = 1)



%% -------------------------------------------------OptSol Model Parameters
Model.Name=              'PMR'; %-Model name code (input: character array, E.g. 'PMR')
Model.ScaleWork=         'yes'; %-Scale actuator work cost by 25%,-120%? (input: 'yes' or 'no')
Model.ForceRateCoef=      5e-4; %-Force-rate cost coefficient, default= 5e-4 (input: #>=0)
Model.DampingRatio=       0.10; %-Tendon damping ratio, default= 0.1 (input: #>=0)
Model.CollisionFraction=  0.03; %-Fraction of CoM momentum lost in collision, default= 0.03 (input: #>=0)
Model.SpringConstant=    35582; %-Spring constant, default= 35582 N/m (input: #>0)
Model.GroundSlope=        0.00; %-Ground slope as fraction dy/dx, default= 0, flat  (input: #),
Model.RelativeGravity=    1.00; %-Gravity realtive to Earth, default= 1= 9.81 m/s2  (input: #),
Model.Speed=             [3.0]; %-Running speed, default= 3 m/s (input: #>0)
Model.StepLength=        'G06'; %-Step length, default= 'G06' (input: #>0 m or 'G06' for what humans do: Gutmann et al. 2006, JEB)
Model.BodyMass=             70; %-Body mass, default= 70 kg (input: #>0 kg)
Model.MaxLegLength=       0.90; %-Maximum allowable leg length, default= 0.90 m (input: #>0 m)
Model.SaveName=             ''; %-Name of optimization output desired for plot (input: character array), default= fnID
Model.Preset=               ''; %-Specify figure from parameter presets (see ReadMe for options)
Model.SPmat=       Model.Speed; %-For multiple optimizations, define SPmat with desired field in structure Model



%% ======================================================================%%
%%=======================================================================%%
%%==================================^====================================%%
%%================================^^^^^==================================%%
%%=============================^^^^^^^^^^^===============================%%
%%==========================^^^^^^^^^^^^^^^^^============================%%
%============================END: USER INPUT==============================%
%%=======================================================================%%
%%=======================================================================%%











%% ==============================================================Run Figure
ProbSet.SPmat= Model.SPmat;

%-update SPmat for presets
[SPps, View, Model]= SetPresetSPmat(View, Model, ProbSet);

%-fill in default parameter values
if isempty(Model.Name);   Model.Name= 'srinR';   end
[Model, ProbSet]= feval([Model.Name '_DefaultParams'],Model, ProbSet);



%% ============================================Iterate for Multiple OptSols
for iSP=1:length(Model.SPmat)
fprintf('SPmat(%1.0f)= %1.2f\n',iSP,Model.SPmat(iSP));
ProbSet.iSP=iSP;


%-check preset
Model= UpdateModelFromPreset(View, Model, ProbSet, SPps);


%-compile model ID
[Model, ProbSet, auxdata]= CompileModelID(Model, ProbSet);



%% =============================================================Load OptSol
[OptSol(iSP), Model, ProbSet, auxdata]= LoadOptSol(Model, ProbSet, auxdata);


%% =================================================Output OptSol Variables
OPvars(iSP)= feval([Model.Name '_ProcessOptSol'],OptSol(iSP), Model);


%% ==================================================Plot Optimal Solutions
feval([Model.Name '_PlotOptSol'],OPvars(iSP), Model, ProbSet, OptSol(iSP), View);


end %-end SPmat(iSP)


%% ============================================Plot Sweeping Parameter Vars
PlotSPmat(OPvars, Model, View);


%% =====================================================Plot Preset Figures
PlotPresetFigures(OPvars, OptSol, Model, View, SPps);





