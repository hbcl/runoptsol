clc;clear all;close all;
% PathSetup for GPOPS2, SNOPT and Dynamic Optimization package (Schroeder, 2021)
% this script adds the appropriate paths for use of gpops2 to the
% MATLAB path directory


% get current directory
currdir= pwd;
startupFileID= fopen('startupFileGPOPSII.m','w');
writeToStartup= strcat('function currdir= startupFileGPOPSII \n');
fprintf(startupFileID,writeToStartup,'\n');
if ispc
writeToStartup= strcat('currdir= ','''',strrep(currdir,'\','\\'),'''',';','\n');
else
writeToStartup= strcat('currdir= ','''',currdir,'''',';','\n');
end
fprintf(startupFileID,writeToStartup,'\n');
writeToStartup= strcat('cd(currdir);','\n');
fprintf(startupFileID,writeToStartup,'\n');


% add currdir
pathAdded= currdir;
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup,'\n');


% add back_end
pathAdded= [currdir '/back_end'];
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup,'\n');


% add back_end\Figures\
extdir2= '/back_end/Figures';
pathAdded= [currdir extdir2];
dinfo= dir(pathAdded);   dinfo(ismember({dinfo.name},{'.','..'}))= [];
for i=1:length(dinfo)
pathAdded= [currdir extdir2 '/' dinfo(i).name];
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup,'\n');
end


% add back_end\GPOPS2
extdir1= '/back_end/GPOPS2';
pathAdded= [currdir extdir1];
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup,'\n');


% add back_end\Models
extdir2= '/back_end/Models';
pathAdded= [currdir extdir2];
dinfo= dir(pathAdded);   dinfo(ismember({dinfo.name},{'.','..'}))= [];
for i=1:length(dinfo)
pathAdded= [currdir extdir2 '/' dinfo(i).name];
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup,'\n');
end


% add RPMintegration/opRPMIsnopt/
pathAdded = strcat([currdir extdir1,'/lib/gpopsRPMIntegration/gpopsSnoptRPMI/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/lib/gpopsRPMIntegration/gpopsSnoptRPMI/'],'-begin');


% add RPMintegration/opRPMIipopt/
pathAdded = strcat([currdir extdir1,'/lib/gpopsRPMIntegration/gpopsIpoptRPMI/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup,'\n');
% addpath([currdir extdir1,'/lib/gpopsRPMIntegration/gpopsIpoptRPMI/'],'-begin');


% add RPMintegration/
pathAdded = strcat([currdir extdir1,'/lib/gpopsRPMIntegration/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/lib/gpopsRPMIntegration/'],'-begin');


% add RPMdifferentiation/opRPMDsnopt/
pathAdded = strcat([currdir extdir1,'/lib/gpopsRPMDifferentiation/gpopsSnoptRPMD/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/lib/gpopsRPMDifferentiation/gpopsSnoptRPMD/'],'-begin');


% add RPMdifferentiation/opRPMDipopt/
pathAdded = strcat([currdir extdir1,'/lib/gpopsRPMDifferentiation/gpopsIpoptRPMD/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/lib/gpopsRPMDifferentiation/gpopsIpoptRPMD/'],'-begin');


% add RPMdifferentiation/
pathAdded = strcat([currdir extdir1,'/lib/gpopsRPMDifferentiation/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/lib/gpopsRPMDifferentiation/'],'-begin');


% add OCPfinitediff/
pathAdded = strcat([currdir extdir1,'/lib/gpopsFiniteDifference/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/lib/gpopsFiniteDifference/'],'-begin');


% add AdiGator/
pathAdded = strcat([currdir extdir1,'/lib/gpopsADiGator/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
% addpath([currdir extdir1,'/lib/gpopsADiGator/'],'-begin');


% add gpopsAutomaticScaling/
pathAdded = strcat([currdir extdir1,'/lib/gpopsAutomaticScaling/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/lib/gpopsAutomaticScaling/'],'-begin');


% add gpopsMeshRefinement/
pathAdded = strcat([currdir extdir1,'/lib/gpopsMeshRefinement/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/lib/gpopsMeshRefinement/'],'-begin');


% add Common/
pathAdded = strcat([currdir extdir1,'/lib/gpopsCommon/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/lib/gpopsCommon/'],'-begin');


% add gpopsUtilities/
pathAdded = strcat([currdir extdir1,'/gpopsUtilities/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/gpopsUtilities/'],'-begin');


% add license/
pathAdded = strcat([currdir extdir1,'/license/']);
if ispc; pathAdded= strrep(pathAdded,'/','\'); end
addpath(pathAdded,'-begin');
disp(['Adding Directory ',pathAdded,' to Path']);
if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
fprintf(startupFileID,writeToStartup);
% addpath([currdir extdir1,'/license/'],'-begin');


% add NLP solver directory
Chk= [currdir extdir1 '/nlp/snopt'];
if ispc; Chk= strrep(Chk,'/','\'); end
if isdir(Chk)
  pathAdded = strcat([currdir extdir1,'/nlp/snopt/snopt7/matlab']);
  if ispc; pathAdded= strrep(pathAdded,'/','\'); end
  addpath(pathAdded,'-begin');
  disp(['Adding Directory ',pathAdded,' to Path']);
  if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
  writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
  fprintf(startupFileID,writeToStartup);
  % addpath([currdir extdir1,'/nlp/snopt/snopt2017'],'-begin');
end
Chk= [currdir extdir1 '/nlp/ipopt'];
if ispc; Chk= strrep(pathAdded,'/','\'); end
if isdir(Chk)
  pathAdded = strcat([currdir extdir1,'/nlp/ipopt/']);
  if ispc; pathAdded= strrep(pathAdded,'/','\'); end
  addpath(pathAdded,'-begin');
  disp(['Adding Directory ',pathAdded,' to Path']);
  if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
  writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
  fprintf(startupFileID,writeToStartup);
  % addpath([currdir extdir1,'/nlp/ipopt/'],'-begin');
end



% add snopt mex files directory
Chk= [currdir extdir1 '/nlp/snopt'];
if ispc; Chk= strrep(Chk,'/','\'); end
if isdir(Chk)
  pathAdded = strcat([currdir extdir1,'/nlp/snopt/snopt7/matlab/precompiled']);
  if ispc; pathAdded= strrep(pathAdded,'/','\'); end
  addpath(pathAdded,'-begin');
  disp(['Adding Directory ',pathAdded,' to Path']);
  if ispc; pathAdded= strrep(pathAdded,'\','\\'); end
  writeToStartup = strcat('addpath(','''',pathAdded,'''',');','\n');
  fprintf(startupFileID,writeToStartup);
  % addpath([currdir extdir1,'/nlp/snopt/snopt2017'],'-begin');
end


writeToStartup= strcat('end');
fprintf(startupFileID,writeToStartup,'\n');



disp('-------------------------------------------------------------------------');
disp('The GPOPS-II directories have been successfully added to the MATLAB path.');
disp('and have been written to a file named "startupFileGPOPSII.m".');
disp('-------------------------------------------------------------------------');
disp('');
pathNotSaved = savepath;
if pathNotSaved
  warning('% The MATLAB path could not be saved to the master path definition file PATHDEF.M.    %');
  warning('% In order to include the GPOPS-II directories automatically each time MATLAB starts, %');
  warning('% please see the instructions in the GPOPS-II user guide.                             %');
end;

disp('');

if exist('adigator.m','file') < 2
  disp('%--------------------------------------------------------------------------%');
  disp('% In order to install ADiGator, please visit the AdiGator project website: %')
  disp('% <a href="http://sourceforge.net/projects/adigator/">http://sourceforge.net/projects/adigator/</a>.                               %')
  disp('%--------------------------------------------------------------------------%');
end

fclose(startupFileID);
